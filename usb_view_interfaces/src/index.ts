import DevicesView from "./Views/DevicesView";
import DevicesViewClass from "./Views/DevicesViewClass";

export const viewInterfaces = {
   DevicesView: <DevicesView> {},
   DevicesViewClass: <DevicesViewClass> {}
};


export type ViewInterfacesType = typeof viewInterfaces;
export  { deviceType} from "./Views/DevicesView";
export  { deviceTypeClass, devicesByClass} from "./Views/DevicesViewClass";