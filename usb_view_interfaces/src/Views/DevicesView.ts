import { ViewInterface } from "@mcesystems/reflow";

export interface Input {
	devices: deviceType[];

}

export interface Events {
	
}

export interface Output {
}


export interface deviceType {
   
        vendor: number,
        product: number,
        class: number,
        children: deviceType[]


}

export default interface DevicesView extends ViewInterface<Input, Events, Output> { }