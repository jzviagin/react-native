import { ViewInterface } from "@mcesystems/reflow";
export interface Input {
    devices: devicesByClass;
}
export interface Events {
}
export interface Output {
}
export interface deviceTypeClass {
    vendor: number;
    product: number;
}
export interface devicesByClass {
    [typeNum: number]: deviceTypeClass[];
}
export default interface DevicesViewClass extends ViewInterface<Input, Events, Output> {
}
