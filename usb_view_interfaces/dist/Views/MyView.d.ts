import { ViewInterface } from "@mcesystems/reflow";
export interface Input {
    myInProp: string;
    mySecondInProp: string;
}
export interface Events {
    myTriggeredEvent: {
        myEventData: number;
    };
    event2: {
        event2param: string;
    };
}
export interface Output {
    myOutProp: boolean;
}
export default interface MyView extends ViewInterface<Input, Events, Output> {
}
