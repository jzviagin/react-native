const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth')

const db = require('../db/db')

const User = mongoose.model('User');


const Follow = mongoose.model('Follow');

const util = require('../util')

router.use(requireAuth);

router.post('/findUsers', async (req, res) =>{
    console.log(req.body);
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 

        const ret = await db.findUsers(req.body.username);
       res.send(ret);
    }catch (err){
        console.log(err.message);
        res.status(422).send(err.message);
    }

});



router.post('/getUser', async (req, res) =>{
    console.log('get user', req.body);
   
    const {userId} = req.body
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 
        
        let ret  = await db.getUser(userId);

        console.log('get user ', JSON.stringify(ret));
    
        if (ret){

            console.log('get user combined', JSON.stringify(ret));
            res.send(ret);
            return;
        }
        console.log('user not found',  req.body.userId)
        res.status(422).send('not found');
        

    
    
    
     
    }catch (err){
            console.log(err)
        res.status(422).send(err);
    }

    

       

});


router.get('/photo/:file', async (req, res) =>{
	console.log('photo');
    console.log('get photo   12', __dirname);
    console.log(req.params);


    const file = `${__dirname}/../uploads/${req.params.file}`;
	console.log(file);
//	req.send({
//		file:file})
//	return ;
     res.download(file); // Set disposition and send it.
      

    

       

});


router.post('/updateUser', async (req, res) =>{
    console.log('update user', req.body);
    console.log('get user files', req.file);
    const {firstName, lastName} = req.body
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 
        
        let user  = await User.findOne({_id: req.user._id});

        user.firstName = firstName;
        user.lastName = lastName;

        if(req.file){
            user.photo = 'http://ec2-3-17-34-61.us-east-2.compute.amazonaws.com/photo/' + req.file.filename;
        }
        await user.save()
        res.send({user: user});

        
    
     
    }catch (err){
            console.log(err)
        res.status(422).send(err);
    }

    

       

});










router.post('/getFollowers', async (req, res) =>{
    console.log(req.body);
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 


        const ret = await db.getFollowers(req.body.userId);
        res.send(ret);
    }catch (err){
            console.log(err)
        res.status(422).send(err);
    }
});

router.post('/getFollowing', async (req, res) =>{
    console.log(req.body);
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 

        const ret = await db.getFollowing(req.body.userId);
        res.send(ret);
     
    }catch (err){
            console.log(err)
        res.status(422).send(err);
    }

    

       

});






router.post('/follow', async (req, res) =>{
    const {userId} = req.body;
    console.log(req.body);
    
    try{

        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 

        const ret = await db.follow(req.user._id, userId)
        res.send(ret);
       
    
    }catch (err){

        console.log(err.message)
        res.status(422).send(err.message);
    }
  


});


router.post('/unfollow', async (req, res) =>{
    const {userId} = req.body;
    console.log('unfollow',req.body);
    
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 

        const ret = await db.unfollow(req.user._id, userId)
        res.send(ret);
    }catch (err){
        console.log('unfollow err',err.message)
        res.status(422).send(err.message);
    }
  


});







module.exports = router
