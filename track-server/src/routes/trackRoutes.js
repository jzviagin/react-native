const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth')

const db = require ('../db/db')
const util = require('../util')

const Track = mongoose.model('Track');

const Comment = mongoose.model('Comment');

const User = mongoose.model('User');


const Like = mongoose.model('Like');


router.use(requireAuth);

router.post('/getUserTracks', async (req, res) =>{
  
    try{
        const ret = await db.getUserTracks(req.user._id);
        res.send(ret);
    }catch (err){
        console.log(err.message)
        res.status(422).send(err.message);
    }

});



router.post('/createTrack', async (req, res) =>{

    console.log(req.body);
    const {description, locations, insertTime, origId} = req.body;


	
    try{
        const ret = await db.createTrack(req.user._id, description, locations);
        res.send(ret);
    }catch (err){
        console.log(err)
        res.status(422).send(err.message);
    }


});






router.post('/like', async (req, res) =>{
    const {trackId} = req.body;
    console.log(req.body);
    
    try{
       
        const ret = await db.like(trackId, req.user._id);
        res.send(ret);
    }catch (err){
        console.log(err.message);
        res.status(422).send(err.message);
    }
  


});








router.post('/unlike', async (req, res) =>{
    const {trackId} = req.body;
    console.log('unlike',req.body);
    
    try{
        const ret = await db.unlike(trackId, req.user._id);
        res.send(ret);
    }catch (err){
        console.log('unlike err',err.message)
        res.status(422).send(err.message);
    }
  


});


router.post('/getLikes', async (req, res) =>{

    console.log('getLikes')
    const {trackId} = req.body;
    console.log(req.body);
    
    try{
       
        const ret = await db.getLikes(trackId);
        res.send(ret);
    }catch (err){
        res.status(422).send(err.message);
    }
  


});




router.post('/getComments', async (req, res) =>{
    console.log('getComments')
    const {trackId} = req.body;
    console.log(req.body);
    
    try{
       
        const ret = await db.getComments(trackId);
        res.send(ret);
    }catch (err){
        res.status(422).send(err.message);
    }
  


});



router.post('/addComment', async (req, res) =>{
    const {trackId, text} = req.body;
    console.log(req.body);
    
    try{
        const ret = await db.addComment(trackId,req.user._id, text);
        res.send(ret);
    }catch (err){
        console.log('add comment',err.message)
        res.status(422).send(err.message);
    }
  


});




router.post('/getTrack', async (req, res) =>{
    console.log(req.body);
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 

        const ret = db.getTrack(req.body.trackId);
        res.send(ret);
    
     
    }catch (err){
            console.log(err)
        res.status(422).send(err);
    }

    

       

});


router.post('/getComment', async (req, res) =>{
    console.log('getComment');
    console.log(req.body);
    try{
        if(! req.user){
            res.status(422).send(err.message);
            console.log(err.message);
            return; 
        } 

       const ret = await db.getComment(req.body.commentId);
       res.send(ret);

    
    
    
     
    }catch (err){
            console.log(err)
        res.status(422).send(err);
    }

    

       

});




router.post('/feed', async (req, res) =>{
    const {trackId, text} = req.body;
    console.log('feed', req.body.page, new Date());
    const page = parseInt(req.body.page);
    
    try{
        if(! req.user){
            res.status(422).send('user not found');
            console.log('user not found');
            return; 
        } 
     

        const ret = await db.feed(req.user._id, page);
	    console.log('qot feed', new Date());
        res.send(ret);
    }catch (err){
        console.log(err);
        res.status(422).send(err.message);
    }
  


});

router.post('/userTracks', async (req, res) =>{
    const {userId} = req.body;
    console.log('userTracks', req.body);
    const page = parseInt(req.body.page);
    
    try{
        if(! req.user){
            res.status(422).send('user not found');
            console.log('user not found');
            return; 
        } 
     

        const ret = await db.userTracks(userId, page);
        res.send(ret);
    }catch (err){
        console.log(err);
        res.status(422).send(err.message);
    }
  


});












module.exports = router;
