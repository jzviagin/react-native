const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const allowAuth = require('../middlewares/allowAuth')

router.use (allowAuth)

const User = mongoose.model('User');

const  validateEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

router.post('/signup', async (req, res) =>{
    console.log(req.body);

    const {email, password, username, firstName, lastName} = req.body;

    if (!email || email.length === 0 || !validateEmail(email) ){
        res.status(422).send('Please provide a valid email address');
        return;
    }

    if (!password || password.length === 0  ){
        res.status(422).send('Please provide a valid password');
        return;
    }

    let existingUser = await User.findOne( {email: email});
    if(existingUser){
        res.status(422).send("email address is already registered");
        return;
    }

    existingUser = await User.findOne( {username: username});
    if(existingUser){
        res.status(422).send("username is already registered");
        return
    }

   
    try{
        const user = new User({username, email, password, firstName, lastName});
        await user.save();
        const token = jwt.sign({userId: user._id}, 'MY_SECRET_KEY');
        res.send({user:user,
            token:token});
    }catch (err){
        res.status(422).send(err.message);
    }

});

router.post('/login', async (req, res) =>{
    console.log(req.body);
    const {email, password} = req.body;

    if (!email || email.length === 0 ){
        res.status(422).send('Please provide a valid email address');
    }

    try{
        const user = await User.findOne( {email: email});
        if(!user){
            res.status(422).send("email address is not registered");
        }
        user.comparePassword(password).then( (success)=>{
            if (success === true){
                const token = jwt.sign({userId: user._id}, 'MY_SECRET_KEY'); 
                res.send({user: user, token:token});
                return;
            }
            res.status(422).send("wrong password");

        }).catch( (err) => {
            res.status(422).send(err);
        })

        
    }catch (err){
        res.status(422).send(err.message);
    }



    

});



router.get('/login', async (req, res) =>{
    console.log(req.body);


    console.log('login');

    try{
        console.log('login user', req.user);
        const user = await User.findOne( {_id: req.user._id});
    
        if(!user){
            console.log('user not found');
            res.status(422).send("user not registered");
            return;
        }
        const token = jwt.sign({userId: user._id}, 'MY_SECRET_KEY'); 
        console.log('login token', token);
        res.send ({user:user, token:token});        
    }catch (err){
        console.log('login err', err);
        res.status(422).send(err.message);
    }



    

});


module.exports = router;