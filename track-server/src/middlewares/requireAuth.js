const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = (req, res, next) => {
    console.log('require auth',req.url);
    if (req.method == 'OPTIONS'){
        next();
        return;
    }
    const {authorization} = req.headers;
    if (!authorization){
        console.log('You must be logged in. ', req.method)
        return res.status(401).send({error: 'You must be logged in. '});
    }
    const token = authorization.replace('Bearer ', '');
    jwt.verify(token, 'MY_SECRET_KEY', async (err, payload) => {
        if (err) {
            return res.status(401).send({error: 'You must be logged in. '});
        }
        const {userId} = payload;
        const user = await User.findById(userId);
        req.user = user;
        next();
    })
};