const mongoose = require('mongoose');
const User = mongoose.model('User');
const Follow = mongoose.model('Follow');
const Track = mongoose.model('Track');
const Comment = mongoose.model('Comment');
const Like = mongoose.model('Like');
const util = require('../util')

module.exports = {
    
    getUser : async (userId)=>{
        const user = await User.findOne({_id: userId});

        
        if (user){
            let ret = {users: {[user._id]: user}}

            //console.log ('get user', module.exports)
            const followers = await module.exports.getFollowers(userId);
            const following = await module.exports.getFollowing(userId);
            //console.log('followers', JSON.stringify(followers));
            //console.log('following', JSON.stringify(following));

            ret = util.combineResults(ret,following);
            ret = util.combineResults(ret,followers);
            return ret;
        }
        throw 'not found'
    },



    getFollowing : async (userId)=>{
        
        const following = [];
        const users = {}
        const follows = await Follow.find({source: userId})
        for (let i = 0 ; i <  follows.length; i++){
            const user = await User.findOne({_id: follows[i].dest});
            if(user){
                users[user._id] = user;
                following.push(user._id)
            }


        }

        return {users: users, following: {[userId]: following}}
    },

    getFollowers : async (userId)=>{
        const followers = [];
        const users = {}
        //console.log('get followers', userId);
        
        const follows = await Follow.find({dest: userId})
        for (let i = 0 ; i <  follows.length; i++){
            const user = await User.findOne({_id: follows[i].source});
            if(user){
                users[user._id] = user;
                followers.push(user._id)
            }


        }
        //console.log('get followers2', follows);

        return {users: users, followers: {[userId]: followers}}
    },




    follow :  async (sourceId, destId) =>{
        const user = await User.findOne({_id: mongoose.Types.ObjectId(destId)})

        if (!user){
            
            console.log('user not found follow');
            throw 'user not found follow'
        }

        console.log('following from ', sourceId, ' to ' , destId)

    

        const existingFollow = await Follow.findOne({source: mongoose.Types.ObjectId(sourceId), dest:mongoose.Types.ObjectId(destId)})
        if (existingFollow){
            console.log('already followed');
            throw 'already followed';
        }
        console.log('following');
        const follow = new Follow({source: sourceId, dest: destId });
        await follow.save();

        let ret = {}


        const followers = await module.exports.getFollowers(destId);
        const following = await module.exports.getFollowing(sourceId);

        ret = util.combineResults(ret, followers);
        ret = util.combineResults(ret, following);


        console.log('followers ',followers);
        console.log('following ',following);

        console.log('combined ',ret);

        return ret;
            

        
    

    },


    
    unfollow :  async (sourceId, destId) =>{

        console.log('unfollow', sourceId, destId)
        const existingFollow = await Follow.findOne({source: mongoose.Types.ObjectId(sourceId), dest:mongoose.Types.ObjectId(destId)})
        if (!existingFollow){
            
            console.log('not followed');
            throw 'not followed';
        }

        await Follow.deleteOne({source: mongoose.Types.ObjectId(sourceId), dest:mongoose.Types.ObjectId(destId)})




        let ret = {}


        const followers = await module.exports.getFollowers(destId);
        const following = await module.exports.getFollowing(sourceId);

        ret = util.combineResults(ret, followers);
        ret = util.combineResults(ret, following);


        console.log('followers',followers);
        console.log('following',following);

        return ret;
            
  


    },

    
    findUsers: async (username) =>{

            const users = await User.find({username: { 
                "$regex": new RegExp(username.replace(/\s+/g,"\\s+"), "gi")
            }});
            let ret = {users: {}};
            const userIds = []
            for (let i = 0 ; i < users.length; i++){
                const followers = await module.exports.getFollowers(users[i]._id);
                const following = await module.exports.getFollowing(users[i]._id);
        
                ret = util.combineResults(ret, followers);
                ret = util.combineResults(ret, following);
                ret['users'][users[i]._id] = users[i]
                userIds.push(users[i]._id);
               
            }
            ret['search'] = {
                users: userIds
            }
            return ret;
       

    },

     
    userTracks: async (userId, page) =>{

        const userIds = [mongoose.Types.ObjectId(userId)]

        console.log('user ids', userIds)


        let QUERY = [

            { "$match": { 
                "userId": { "$in": userIds }
            } },
            { "$sort" : { "date" : -1 } },

            { "$skip" : (page *10) },
            { "$limit" : 10 },
            
          

           ];

           console.log('query', QUERY)
    
    
    
        const tracks = await Track.aggregate(QUERY);
        if (!tracks){
            console.log('errrrrror')
            throw 'error'
        }



        const trackIds = tracks.map((track)=> track._id)

        QUERY = [

            { "$match": { 
                "trackId": { "$in": trackIds }
            } }

     
            
          

           ]

        const comments = await Comment.aggregate(QUERY)
        console.log('comments', comments)

        const likes = await Like.aggregate(QUERY)
        console.log('likes', likes)

      

        tracks.forEach((track)=> {
            userIds.push(track.userId)
        })

        comments.forEach((comment)=> {
            userIds.push(comment.userId)
        })

        likes.forEach((like)=> {
            userIds.push(like.userId)
        })
        

        QUERY = [

            { "$match": { 
                "_id": { "$in": userIds }
            } }

     
            
          

           ]

        const users = await User.aggregate(QUERY);

        console.log('users', users)


        const retUsers = {}
        users.forEach((user)=>{
            retUsers[user._id] = user
        })

        const retCommentsData = {}
        const retComments = {}
        tracks.forEach((track)=>{
            retComments[track._id] = []
        })
        comments.forEach((comment)=>{
            retCommentsData[comment._id] = comment
            if (!retComments[comment.trackId]){
                retComments[comment.trackId] = []
            }
            retComments[comment.trackId].push(comment._id)
        })


        const retLikes = {}
        tracks.forEach((track)=>{
            retLikes[track._id] = []
        })
        likes.forEach((like)=>{
            if (!retLikes[like.trackId]){
                retLikes[like.trackId] = []
            }
            retLikes[like.trackId].push(like.userId)
        })

        const retTracks = {}
        tracks.forEach((track)=>{
            retTracks[track._id] = track
        })





       // const trackIds = [];


       let ret = {
        userTracks: {
            [page]: []
        }
    }
     

        ret['userTracks'][page] = trackIds;

        ret['commentsData'] = retCommentsData;
        ret['comments'] = retComments;
        ret['likes'] = retLikes;
        ret['tracks'] = retTracks;
        ret['users'] = retUsers

        //let allUserIds = Object.keys(allUsers);
        console.log('usertracks complete' , ret.comments.length);
        return ret;
           
        




    },


feed3: async (userId, page) =>{

        console.log(' feed', new Date());

        const follows = await Follow.find({source: userId});

        console.log('found user', new Date(), follows.length);
        const userIds = []
        for (let i = 0 ; i < follows.length; i++){
            userIds.push(follows[i].dest)
        }

        let QUERY = [

            { "$match": { 
                "userId": { "$in": userIds }
            } },
            { "$sort" : { "date" : -1 } },

            { "$skip" : (page *10) },
            { "$limit" : 10 },
            
          

           ]
    
    
    
        let tracks = await Track.aggregate(QUERY);

        console.log('found tracks', new Date() , tracks);
        if(tracks.length === 0){
            QUERY = [

                { "$sort" : { "date" : -1 } },
    
                { "$skip" : (page *10) },
                { "$limit" : 10 },
                
              
    
               ]
            tracks = await Track.aggregate(QUERY);
        }

        console.log('found tracks', new Date() , tracks);
        if (!tracks){
            throw 'error'
        }
        const trackIds = [];

        let ret = {
            feed: {
                [page]: []
            }
        }

        //console.log('feed', tracks)

        let comments = {};
        let likes = {};
        let allUsers = {};
        let allTracks = {};
        let commentsData = {}



        for (let i = 0 ; i < tracks.length; i++){
            try{

                comments[tracks[i]._id] = []
                likes[tracks[i]._id] = []

                allTracks[tracks[i]._id] = tracks[i]
                const user = await User.findOne({_id:mongoose.Types.ObjectId(tracks[i].userId)});
                    if (user){
                        if (!allUsers[user._id]){
                            allUsers[user._id] = user;
                        }
                    }
                

                const commentsForTrack = await Comment.find({trackId:mongoose.Types.ObjectId(tracks[i]._id)});

                const likesForTrack = await Like.find({trackId:mongoose.Types.ObjectId(tracks[i]._id)});
                for (let j = 0 ; j < commentsForTrack.length ; j++){
                    comments[tracks[i]._id].push(commentsForTrack[j]._id)
                    commentsData[commentsForTrack[j]._id] = commentsForTrack[j];
                    const user = await User.findOne({_id:mongoose.Types.ObjectId(commentsForTrack[j].userId)});
                    if (user){
                        if (!allUsers[user._id]){
                            allUsers[user._id] = user;
                        }
                    }
                }
                for (let j = 0 ; j < likesForTrack.length ; j++){
                    likes[tracks[i]._id].push(likesForTrack[j].userId)
                    const user = await User.findOne({_id:mongoose.Types.ObjectId(likesForTrack[j].userId)});
                    if (user){
                        if (!allUsers[user._id]){
                            allUsers[user._id] = user;
                        }
                    }
                }

                ret['feed'][page].push(tracks[i]._id);

    
            }catch(err){
                console.log(err)
            }
      
        }

        ret['commentsData'] = commentsData;
        ret['comments'] = comments;
        ret['likes'] = likes;
        ret['tracks'] = allTracks;
        ret['users'] = allUsers

        let allUserIds = Object.keys(allUsers);
        console.log('feed complete' , ret.likes);
        return ret;
           
        
        



    },



    feed: async (userId, page) =>{

        console.log(' feed', new Date());

        const follows = await Follow.find({source: userId});

        console.log('found user', new Date(), follows.length);
        let userIds = []
        for (let i = 0 ; i < follows.length; i++){
            userIds.push(follows[i].dest)
        }

        let QUERY = [

            { "$match": { 
                "userId": { "$in": userIds }
            } },
            { "$sort" : { "date" : -1 } },

            { "$skip" : (page *10) },
            { "$limit" : 10 },
            
          

           ]
    
    
    
        let tracks = await Track.aggregate(QUERY);

        console.log('found tracks', new Date() , tracks);
        if(tracks.length === 0){
            QUERY = [

                { "$sort" : { "date" : -1 } },
    
                { "$skip" : (page *10) },
                { "$limit" : 10 },
                
              
    
               ]
            tracks = await Track.aggregate(QUERY);
        }

        console.log('found tracks', new Date() , tracks);
        if (!tracks){
            throw 'error'
        }

        const trackIds = tracks.map((track)=> track._id)

        QUERY = [

            { "$match": { 
                "trackId": { "$in": trackIds }
            } }

     
            
          

           ]

        const comments = await Comment.aggregate(QUERY)
        console.log('comments', comments)

        const likes = await Like.aggregate(QUERY)
        console.log('likes', likes)

        userIds = [];

        tracks.forEach((track)=> {
            userIds.push(track.userId)
        })

        comments.forEach((comment)=> {
            userIds.push(comment.userId)
        })

        likes.forEach((like)=> {
            userIds.push(like.userId)
        })
        

        QUERY = [

            { "$match": { 
                "_id": { "$in": userIds }
            } }

     
            
          

           ]

        const users = await User.aggregate(QUERY);

        console.log('users', users)


        const retUsers = {}
        users.forEach((user)=>{
            retUsers[user._id] = user
        })

        const retCommentsData = {}
        const retComments = {}
        tracks.forEach((track)=>{
            retComments[track._id] = []
        })
        comments.forEach((comment)=>{
            retCommentsData[comment._id] = comment
            if (!retComments[comment.trackId]){
                retComments[comment.trackId] = []
            }
            retComments[comment.trackId].push(comment._id)
        })


        const retLikes = {}
        tracks.forEach((track)=>{
            retLikes[track._id] = []
        })
        likes.forEach((like)=>{
            if (!retLikes[like.trackId]){
                retLikes[like.trackId] = []
            }
            retLikes[like.trackId].push(like.userId)
        })

        const retTracks = {}
        tracks.forEach((track)=>{
            retTracks[track._id] = track
        })





       // const trackIds = [];

        let ret = {
            feed: {
                [page]: []
            }
        }

     

        ret['feed'][page] = trackIds;

        ret['commentsData'] = retCommentsData;
        ret['comments'] = retComments;
        ret['likes'] = retLikes;
        ret['tracks'] = retTracks;
        ret['users'] = retUsers

        //let allUserIds = Object.keys(allUsers);
        console.log('feed complete' , ret.comments);
        return ret;
           
        
        



    },


    
    feed2233: async (userId, page) =>{

        const follows = await Follow.find({source: userId});
        const userIds = []
        for (let i = 0 ; i < follows.length; i++){
            userIds.push(follows[i].dest)
        }

        let QUERY = [

            { "$match": { 
                "userId": { "$in": userIds }
            } },
            { "$sort" : { "date" : -1 } },

            { "$skip" : (page *10) },
            { "$limit" : 10 },
            
          

           ]
    
    
    
        let tracks = await Track.aggregate(QUERY);
        if(tracks.length === 0){
            QUERY = [

                { "$sort" : { "date" : -1 } },
    
                { "$skip" : (page *10) },
                { "$limit" : 10 },
                
              
    
               ]
            tracks = await Track.aggregate(QUERY);
        }
        if (!tracks){
            throw 'error'
        }
        const trackIds = [];

        let ret = {
            feed: {
                [page]: []
            }
        }

        //console.log('feed', tracks)

        for (let i = 0 ; i < tracks.length; i++){
            const track = await module.exports.getTrack(tracks[i]._id);
            ret['feed'][page].push(tracks[i]._id);

            trackIds.push(tracks[i]._id);
            ret = util.combineResults(ret, track);
        }



        console.log('feed' , ret);

        
        return ret;
           
        
        



    },

    
getTrack:  async (trackId) =>{


        const track = await Track.findOne({_id: trackId});
        //const user = await User.findOne({_id: track.userId});

        if (track){

            let result =  {
            tracks: {[track._id]: track}
            }

            const user = await module.exports.getUser(track.userId);
            const likes = await module.exports.getLikes(trackId);
            const comments = await module.exports.getComments(trackId);
            result = util.combineResults(result, user);
            result = util.combineResults(result, likes);
            result = util.combineResults(result, comments);
            //console.log('get track user after combine', result);
            return result;
        }

        throw 'track not found'

    },
    getComment:  async (commentId) =>{

        
   
        const comment = await Comment.findOne({_id: commentId})
        console.log('getComment', commentId)
        console.log('getComment found ', comment)
        const user = await module.exports.getUser(comment.userId);
        let ret = {
            commentsData: {[comment._id]: comment}
        }
        ret = util.combineResults(ret, user)

        return ret;

      
        
    
        


    },
    getComments: async (trackId) =>{

            const track = await Track.findOne({_id: mongoose.Types.ObjectId(trackId)})
    
            if (!track){
                throw 'track not found';
                
            }
    
    
    
    
    
    
    
            let ret = {comments:{
                [trackId]:[]
            }}


            const comments = await Comment.find({trackId:mongoose.Types.ObjectId(trackId)});
        
        
            for (let i = 0 ; i <  comments.length; i++){

                const comment = await module.exports.getComment(comments[i]._id);
                ret = util.combineResults(ret, comment);
                ret['comments'][trackId].push(comments[i]._id)
            
        
        
            }
            
            return ret;
  
      
    
    
    },
    getLikes: async (trackId) =>{
            const track = await Track.findOne({_id: mongoose.Types.ObjectId(trackId)})

            if (!track){
                throw 'track not found';
              
            }
            const users = {}
            const finalLikes = []

            

            const likes = await Like.find({trackId:mongoose.Types.ObjectId(trackId)});

            let ret = {
                likes: {
                    [trackId]:[]
                }
            }

            for (let i = 0 ; i <  likes.length; i++){
                const user = await module.exports.getUser(likes[i].userId);
                ret['likes'][trackId].push(likes[i].userId);
                ret = util.combineResults(ret, user);

            }

            
            return ret;
      
    


    },
    unlike: async (trackId, userId) =>{
        
        console.log('unlike 1')
        const track = await Track.findOne({_id: mongoose.Types.ObjectId(trackId)})

        if (!track){
            console.log('track not found');
            throw 'track not found'
        }


            const existingLike = await Like.findOne({userId: mongoose.Types.ObjectId(userId), trackId:mongoose.Types.ObjectId(trackId)})
            if (!existingLike){
                throw 'not liked';
                
            }

            await Like.deleteOne({userId: mongoose.Types.ObjectId(userId), trackId:mongoose.Types.ObjectId(trackId)})
            

            

        QUERY = [

            { "$match": { 
                "trackId": { "$in": [track._id] }
            } }

     
            
          

           ]

        const likes = await Like.aggregate(QUERY)
        console.log('likes', likes)

        console.log('unlike 2')

    

        userIds = [track.userId];


        likes.forEach((like)=> {
            userIds.push(like.userId)
        })

      

        QUERY = [

            { "$match": { 
                "_id": { "$in": userIds }
            } }

     
            
          

           ]

        const users = await User.aggregate(QUERY);

        console.log('unlike 3')

        console.log('users', users)


        const retUsers = {}
        users.forEach((user)=>{
            retUsers[user._id] = user
        })

        const retLikes = {}

        retLikes[track._id] = []
        likes.forEach((like)=>{
            if (!retLikes[like.trackId]){
                retLikes[like.trackId] = []
            }
            retLikes[like.trackId].push(like.userId)
        })

        let ret = {
            
        }

        console.log('unlike 4')

     


        ret['likes'] = retLikes;
        ret['users'] = retUsers

        return ret;






    },

    

    like: async (trackId, userId) =>{

        console.log('in like async')
  
       
        const track = await Track.findOne({_id: mongoose.Types.ObjectId(trackId)})

        if (!track){
            console.log('track not found');
            throw 'track not found'
        }

        const user = await User.findOne({_id: mongoose.Types.ObjectId(userId)})

        if (!user){
            console.log('user not found');
            throw 'user not found';
        }

        const existingLike = await Like.findOne({userId: mongoose.Types.ObjectId(userId), trackId:mongoose.Types.ObjectId(trackId)})
        if (existingLike){
            console.log('already liked');
            throw 'already liked';
        }
        console.log('liking');
        const like = new Like({userId: userId, trackId:trackId, date: new Date()});
        await like.save();



      





        QUERY = [

            { "$match": { 
                "trackId": { "$in": [track._id] }
            } }

     
            
          

           ]

        const likes = await Like.aggregate(QUERY)
        console.log('likes', likes)

    

        userIds = [track.userId];


        likes.forEach((like)=> {
            userIds.push(like.userId)
        })

      

        QUERY = [

            { "$match": { 
                "_id": { "$in": userIds }
            } }

     
            
          

           ]

        const users = await User.aggregate(QUERY);

        console.log('users', users)


        const retUsers = {}
        users.forEach((user)=>{
            retUsers[user._id] = user
        })

        const retLikes = {}

        retLikes[track._id] = []
        likes.forEach((like)=>{
            if (!retLikes[like.trackId]){
                retLikes[like.trackId] = []
            }
            retLikes[like.trackId].push(like.userId)
        })

        let ret = {
            
        }

     


        ret['likes'] = retLikes;
        ret['users'] = retUsers

        return ret;





    },

    addComment: async (trackId, userId, text) =>{

        
        const track = await Track.findOne({_id: mongoose.Types.ObjectId(trackId)})

        if (!track){
            throw 'track not found';
        }

        const user = await User.findOne({_id: mongoose.Types.ObjectId(userId)})

        if (!user){
            throw 'user not found';
        }

        
        console.log('add comment');
        const comment = new Comment({userId: userId, trackId:trackId, date: new Date(), text:text});
        await comment.save();




        QUERY = [

            { "$match": { 
                "trackId": { "$in": [track._id] }
            } }

     
            
          

           ]

        const comments = await Comment.aggregate(QUERY)
        console.log('comments', comments)

    

        userIds = [track.userId];


        comments.forEach((comment)=> {
            userIds.push(comment.userId)
        })

      

        QUERY = [

            { "$match": { 
                "_id": { "$in": userIds }
            } }

     
            
          

           ]

        const users = await User.aggregate(QUERY);

        console.log('users', users)


        const retUsers = {}
        users.forEach((user)=>{
            retUsers[user._id] = user
        })

        const retCommentsData = {}
        const retComments = {}

        retComments[track._id] = []
        comments.forEach((comment)=>{
            retCommentsData[comment._id] = comment
            if (!retComments[comment.trackId]){
                retComments[comment.trackId] = []
            }
            retComments[comment.trackId].push(comment._id)
        })

        let ret = {
            
        }

     


        ret['commentsData'] = retCommentsData;
        ret['comments'] = retComments;
        ret['users'] = retUsers

        return ret;
           
    
    },
    getUserTracks : async (userId) =>{
  
       console.log('getUserTracks', userId)
        const tracks = await Track.find({userId: userId});
        let ret = {
            userTracks: {
                [userId]:[]
            }
        }


        

        for (let i = 0; i < tracks.length; i++){
            const trackId = tracks[i]._id;
            const trackDetails = await module.exports.getTrack(trackId);
            ret['userTracks'][userId].push(trackId);
            ret = util.combineResults(ret, trackDetails);
        }
        console.log('user tracks', ret);
        return ret;

    },
    
    createTrack: async(userId, description, locations) =>{
        if ( ! locations){
            throw 'you must provide description and locations';
        }

        const sleep = (ms) =>{
            return new Promise((resolve) => {
              setTimeout(resolve, ms);
            });
          } 
      //  for (let i = 0; i < 30 ; i++){
            //console.log(origId)
            const track = new Track({userId: userId, description: description, locations: locations, date: new Date()});
            console.log('created track ', track)
        
            await track.save();
            await sleep(1000);
       // }

        return await module.exports.getUserTracks(userId);
       


    }
    
    

}
