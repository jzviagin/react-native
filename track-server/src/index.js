require('./models/User')
require('./models/Track')
require('./models/Comment')
require('./models/Like')
require('./models/Follow')
const express = require('express');
const app = express();
const requireAuth = require('./middlewares/requireAuth')
const mongoose = require('mongoose')
const authRoutes = require('./routes/authRoutes');
const trackRoutes = require('./routes/trackRoutes');
const userRoutes = require('./routes/userRoutes');

const bodyParser = require('body-parser');

const cors = require('cors')


var multer  = require('multer')
var upload = multer({ dest: __dirname+ '/uploads/' })

app.use(cors());
app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true, parameterLimit: 100000 }));

app.use(upload.single('image'));

app.use(authRoutes);
app.use(trackRoutes);
app.use(userRoutes);

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers','*')
    next();
  });

const mongoUri = 'mongodb+srv://admin:manunited99@cluster0-7vcid.mongodb.net/test?retryWrites=true&w=majority'

mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useCreateIndex: true
});

mongoose.connection.on('connected', ()=> {
    console.log('connected to mongo2222');
});

mongoose.connection.on('error', (err)=> {
    console.log('connection to mongo failed ' + err);
});

app.get('/', (req, res)=> {
    res.send('Hi there');
})

app.get('/login', requireAuth, (req, res)=> {
    console.log(req.user);
    res.send('Hi there');
})



app.listen(3002, ()=>{
    console.log('listening3');
})
