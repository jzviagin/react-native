const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    trackId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Track'
    },
    text: {
        type: String,
        default: ''
    },
    date: {
        type: Date,
        default: new Date()
    }
 
 });




mongoose.model('Comment', commentSchema);
