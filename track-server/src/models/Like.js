const mongoose = require('mongoose');

const likeSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    trackId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Track'
    },

    date: {
        type: Date,
        default: new Date()
    }
 
 });




mongoose.model('Like', likeSchema);
