const mongoose = require('mongoose');


const pointSchema = new mongoose.Schema({
   timestamp :Number,
   coords: {
       latitude:Number,
       longitude: Number,
       altitude: Number,
       accuracy: Number,
       heading: Number,
       speed: Number
   },
	      origId: Number,
        insertTime: Date

});


const trackSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    description: {
        type: String,
        default: ''
    },
    locations: [pointSchema],
    date: {
        type: Date,
        default: new Date()
    }
});



mongoose.model('Track', trackSchema);
