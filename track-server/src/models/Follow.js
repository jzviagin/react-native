const mongoose = require('mongoose');

const followSchema = new mongoose.Schema({
    source: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    dest: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
 
 });




mongoose.model('Follow', followSchema);
