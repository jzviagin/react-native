module.exports = {
    combineResults : (currentResult, nextResult)=>{
        const ret = {...currentResult};
        const keys = Object.keys(nextResult);
        for (let i = 0 ; i < keys.length; i++){
            const entityName = keys[i]
    
            const entity = {...currentResult[entityName]};
            const ids = Object.keys(nextResult[entityName]);
            for (let j = 0 ; j < ids.length; j++){
                const id = ids[j];
                entity[id] = nextResult[entityName][id]
            }
            ret[entityName] = {...entity}
        }
        return ret
    }
    
}
