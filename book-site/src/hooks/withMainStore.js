
import React from 'react';
import {connect} from 'react-redux'
import * as mainActionCreators from '../store/actions/mainActions'


const withMainStore = (Child)=>{
    const MainComp = (props)=>{

        console.log('maincomp', props)
        const funcs = {

            getBook :  (bookId)=>{
                if (props['books'][bookId]){
                    return props['books'][bookId]
                }
                props.dispatch(mainActionCreators.getBook(bookId));
                return null;
            
    
            
                    
            },
    
            fetchBook : (bookId)=>{
    
                props.dispatch(mainActionCreators.getBook(bookId));
                return null;     
            },

            getAuthor: (id)=>{
                if (props['authors'][id]){
                    return props['authors'][id]
                }
                
                return null;
            },
            getPublisher: (id)=>{
                if (props['publishers'][id]){
                    return props['publishers'][id]
                }
                
                return null;
            },
            fetchPublishers : ()=>{
    
                props.dispatch(mainActionCreators.getPublishers());
                return null;     
            },
            getPublishers :  ()=>{
                if (props['publishers']){
                    return props['publishers']
                }
                //props.dispatch(mainActionCreators.getPublishers());
                return null;
            
    
            
                    
            },
            fetchAuthors : ()=>{
    
                props.dispatch(mainActionCreators.getAuthors());
                return null;     
            },
            getAuthors :  ()=>{
                if (props['authors']){
                    return props['authors']
                }
                //props.dispatch(mainActionCreators.getAuthors());
                return null;
            
    
            
                    
            },
            updateBook :  (book)=>{
            
                props.dispatch(mainActionCreators.updateBook(book));
                return null;
            
    
            
                    
            },
            addBook :  (book)=>{
            
                props.dispatch(mainActionCreators.addBook(book));
                return null;
            
    
            
                    
            },
    
    
          
    
    
           
    
          
    
    
           
    
    
            getBooks : ()=>{
    
                
                if (props['search']['books']){
                    return props['search']['books']
                }
                return [];
            },
    
    
    
    
            searchBooks : (book)=>{
                props.dispatch(mainActionCreators.searchBooks(book))
                return [];
            },

            allBooks : ()=>{
                props.dispatch(mainActionCreators.allBooks())
                return [];
            },

            purchaseBook : (bookId)=>{
                props.dispatch(mainActionCreators.purchaseBook(bookId))
                return [];
            },
            
            deleteBook : (bookId)=>{
                props.dispatch(mainActionCreators.deleteBook(bookId))
                return [];
            },
            isPurchased : (bookId)=>{
                if (props['purchases'][bookId]){
                    return true
                }
                return false;
            }
    
    
         
    
          

        }


        const newProps = {...props, ...funcs}

      

        return <Child {...newProps}></Child>
    }

    const mapStateToProps = (state)=>{
        return { 
            search: state.main.search, 
            searchTerm: state.main.searchTerm, 
            books:state.main.books,
            authors:state.main.authors,
            publishers:state.main.publishers,
            purchases: state.main.purchases
           
        }
    }

    const mapDispatchToProps = (dispatch)=>{
        return {
            dispatch: dispatch
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(MainComp)
}

export default withMainStore;