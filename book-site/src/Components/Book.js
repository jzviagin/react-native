import React from 'react'
import {NavLink} from 'react-router-dom'
import classes from './Book.css'
import logo from '../assets/images/logo.png'
import { faEdit, faCartPlus, faMinusCircle } from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class Book extends React.Component{
    render(){

        return (<div className={classes.BookDetails}>
                
                <div className={classes.Vertical}>
                    <h1 className= {classes.Title}   >{this.props.book.name}</h1>
                    <h1 className={classes.Details}>by: {this.props.author.name}</h1>
                    <h1 className={classes.Details}>publisher: {this.props.publisher.name}</h1>
                    <div className={classes.Horizontal}>
                        {this.props.admin?<button className = {classes.EditButton} onClick = {()=>{
                            
                            this.props.onEdit(this.props.book._id)
                        }}><FontAwesomeIcon width = {50} className = {classes.EditIcon} icon={faEdit} />
                        
                        </button>:null}
                        {this.props.admin?<button className = {classes.EditButton} onClick = {()=>{
                            console.log('delete1', this.props.book._id)
                            this.props.onDelete(this.props.book._id)
                        }}><FontAwesomeIcon width = {50} className = {classes.EditIcon} icon={faMinusCircle} />
                        
                        </button>:null}
                       { !this.props.purchased?<button className = {classes.EditButton} onClick = {()=>{
                            this.props.onPurchase(this.props.book._id)
                            
                        }}><FontAwesomeIcon width = {50} className = {classes.EditIcon} icon={faCartPlus} />
                        
                        </button>:null}
                    </div>
                    
                </div>

            </div>)
    }
}

export default Book;