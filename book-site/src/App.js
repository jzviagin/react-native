import React, { Component } from 'react';
import logo from './logo.svg';
import classes from './App.css';
import {BrowserRouter, Switch, Router, Route, withRouter} from 'react-router-dom'
import NavBar from './Components/NavBar'
import BooksScreen from './screens/BooksScreen'
import LoginScreen from './screens/LoginScreen'
import SignupScreen from './screens/SignupScreen'
import * as authActionCreators from './store/actions/authActions'
import * as mainActionCreators from './store/actions/mainActions'
import {connect} from 'react-redux'
import Modal from './Components/Modal'
import EditBookScreen from './screens/EditBookScreen';
import withMainStore from './hooks/withMainStore';
import SearchScreen from './screens/SearchScreen';




class App extends Component {

  constructor(){
    super()

   /* fetch('http://127.0.0.1:3002', {credentials: 'include'}).then(res=>{
      console.log('res',res)
    })*/
    this.state = {
      modalComponent: null,

    }
  }

  componentDidMount(){
    this.props.onSignIn()
  }
  render() {

    if (this.props.inProgress){
        
    }

    console.log('app props', this.props);

    if (!this.props.user){
      console.log('logintest not logged in')
      return (
        <BrowserRouter>
          <Switch>
              <Route path ="/signup" exact component = {SignupScreen}/>
              <Route path ="/"  component = {LoginScreen}/>
          </Switch>
        </BrowserRouter>

      )
    }

    const signOutComponent =  <div className = {classes.Modal}>
                                <h1>Sign out?</h1>
                                <div>
                                  <button onClick = {
                                    ()=>{
                                      this.props.onSignOut()
                                      this.setState({... this.state, modalComponent: null})
                                      }
                                      }>Yes</button>
                                  <button onClick = {()=>{
                              this.setState({... this.state, modalComponent: null})
                            }}>No</button>
                                </div>
                              </div>

    const showModal = (comp)=>{
      document.body.style.overflow = 'hidden';
      this.setState({... this.state, modalComponent: comp});
    }

    const closeModal = ()=>{
      document.body.style.overflow = 'scroll';
      this.setState({... this.state, modalComponent: null});
    }
    console.log('logintest logged in')

    return (

          <div className={classes.App}>

            <BrowserRouter>
            <Modal show = {this.state.modalComponent}
              backdropClickedHandler = {()=>{
                document.body.style.overflow = 'scroll';
                this.setState({... this.state, modalComponent: null})
              }}>
                {
                  this.state.modalComponent
                }
              
            </Modal>
            <NavBar user = {this.props.user} onSignOut = {()=>{this.setState({... this.state, modalComponent: signOutComponent})}}></NavBar>
              <Switch>

  
                
                <Route path ="/search" component = {SearchScreen}/>
                <Route path ="/purchases" render = {(props)=>{
                              
                                
                              return <BooksScreen purchased= {true}/>
                            }} />
                
  
                <Route path ="/edit/:id" render = {(props)=>{
                 
                  
                  return <EditBookScreen bookId = {props.match.params.id} onSubmit = {(book)=>{
                    this.props.updateBook(book);
                  
                  }}/>
                }} />

              <Route path ="/add" render = {(props)=>{
                              
                                
                              return <EditBookScreen onSubmit = {(book)=>{
                                this.props.addBook(book);
                              
                              }}/>
                            }} />
                <Route path ="/" component = {BooksScreen}/>
  
               
       
              </Switch>
            </BrowserRouter>
          </div>
      
    );
  }
}

//export default App;

const mapStateToProps = (state)=>{
  return {
      user: state.auth.user,
      inProgress: state.auth.inProgress

  }
}

const mapDispatchToProps = (dispatch)=>{
  return {
      onSignIn: (data) => {dispatch(authActionCreators.tryLocalSignIn())},
      onSignOut: ()=> {dispatch(authActionCreators.signout())} ,
      onInit: ()=>{dispatch(mainActionCreators.init())}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)( withMainStore(App ) );
