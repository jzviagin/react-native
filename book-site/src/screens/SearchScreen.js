import React  from 'react'
import classes from './SearchScreen.css'
import {connect} from 'react-redux'
import withMainStore from '../hooks/withMainStore'
import Book from '../Components/Book'
import { withRouter } from 'react-router'

class SearchScreen extends React.Component{

    constructor(){
        super()
    }

    componentDidMount(){
        this.props.searchBooks()
    }
    render(){

        const bookIds = this.props.getBooks().filter((id)=>{
            const purchased = this.props.isPurchased(id)
            return (!this.props.purchased || purchased)
        })
        console.log('bookIds', bookIds)

        return <div className = {classes.Main}>
            {!this.props.purchased? <input value = {this.props.searchTerm} type="text" onChange = {

                        (event)=>{

                            this.props.searchBooks(event.target.value)
                            /*if (this.timer){
                                clearTimeout(this.timer)
                            }
                            let obj = this
                            let value = event.target.value 
                            this.timer = setTimeout(()=>{
                                obj.props.searchBooks(value)
                            },1000)*/

                        }
                    
                    } id="term" name="fname" placeholder = "Please enter a book name"/>:null}
                <ul>
                {bookIds.map( (id)=>{
                    const book = this.props.getBook(id)
                    console.log('bookIds', book)
                    const author = this.props.getAuthor(book.authorId)
                    const publisher = this.props.getPublisher(book.publisherId)

                    const purchased = this.props.isPurchased(book._id)
                    return(
                    <li key = {book._id}>
                        <Book  book = {book} author = {author} publisher = {publisher} purchased = {purchased}  admin ={this.props.authUser.admin}
                        onPurchase = {
                            (bookId) =>{
                                this.props.purchaseBook(bookId)
                            }
                        }
                        
                        onEdit = {(bookId)=>{
                            this.props.history.push('/edit/' + book._id)
                        }}
                        
                        onDelete = {(bookId)=>{
                            console.log('delete2', bookId)
                            this.props.deleteBook(bookId)

                        }}
                        
                        ></Book>
                    </li>)
                })}
                </ul>

        </div>
    }
}





const mapStateToProps = (state)=>{
    console.log('mapStateToProps', this.props)
    return {
        authUser: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        
    }
}


export default  connect(mapStateToProps, mapDispatchToProps)( withRouter(withMainStore(SearchScreen)) );