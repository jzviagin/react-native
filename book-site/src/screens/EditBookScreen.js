import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'
import loading from '../assets/images/loading.gif'
import classes from './EditBookScreen.css'
import arrow from '../assets/images/arrow.png'
import {NavLink, withRouter} from 'react-router-dom'
import withMainStore from '../hooks/withMainStore';




class EditBookScreen extends React.Component{

    constructor(){
        super();
        this.state = {
            inputsValid:false,
            name: '',
            publisherId: null,
            authorId: null,
            bookLoaded : false
           
        }

    }


    componentDidMount(){
        this.props.fetchPublishers()
        this.props.fetchAuthors()
        console.log('editBook mount')
       
    }

    shouldComponentUpdate(nextProps, nextState){
       // console.log('editBook mount', this.props)

        if (this.props.bookId != nextProps.bookId){
            console.log('editBook book id updated')

            this.setState({
                ...this.state,
                inputsValid:false,
                name: '',
                publisherId: null,
                authorId: null,
                bookLoaded : false
            })

            this.props.fetchPublishers()
            this.props.fetchAuthors()


            return false;
        }

        return true;
    }

    submitForm (){

        const name = document.getElementById("name").value ;
        const author = document.getElementById("author").value ;
        const publisher = document.getElementById("publisher").value ;

        console.log("submit!!!!!!!!!!!!!!!",this.props.onSubmit)
        console.log("submit!!!!!!!!!!!!!!!",{
            _id: this.props.bookId,
            name: this.state.name,
            authorId: author,
            publisherId: publisher
        })
        this.props.onSubmit({
            _id: this.props.bookId,
            name: this.state.name,
            authorId: author,
            publisherId: publisher
        })

        this.props.history.push('/books' )
    

     
        return true;
    }

    




    render(){

        console.log('book edit screen state', this.state)

        const publishers = this.props.getPublishers()

        if(this.props.bookId){

            const book = this.props.getBook(this.props.bookId)
            console.log('editBook book ,', book)
            if(!this.state.bookLoaded && book){
                console.log('editBook book loaded')
                this.setState({
                    ...this.state,
                    name:book.name,
                    authorId: book.authorId,
                    publisherId:book.publisherId,
                    inputsValid: true,
                    bookLoaded: true
                    
                })
            }

            
        }

        if (!publishers || Object.keys (publishers).length == 0|| (this.props.bookId && !this.state.bookLoaded)){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }


        const publishersArray = [];
        Object.keys(publishers).forEach((key)=>{
            publishersArray.push(publishers[key])
        })

        const authors = this.props.getAuthors()

        if (!authors  || Object.keys (authors).length == 0){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }


        const authorsArray = [];
        Object.keys(authors).forEach((key)=>{
            authorsArray.push(authors[key])
        })





      




        const checkName = (name)=>{
            if (name.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z0-9]*$/;
            const res =  pattern.test(name);
            console.log('res', name, res)
            return res;
        }

        const checkValidity = ()=>{
           
            const name = document.getElementById("name").value ;
   
   
            if(checkName(name) == false){
                return false;
            }
            return true;
        }

    

        



        return (<div className = {classes.Main}>
                <h1>Book Details</h1>
                <form className = {classes.Form} onSubmit = {
                    (e)=>{
                        this.submitForm.call(this);
                        e.preventDefault()
                        return true;
                    }
                }>
                
                    
                    
                    <input value = {this.state.name} type="text" onChange = {
                        (event)=>{

                            if (checkName(event.target.value) == false && event.target.value.length != 0){
                                return;
                            }

                            this.setState({
                                ...this.state,
                                name: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="name" name="fname" placeholder = "Book Name"/>

                    
                    <label className = {classes.Label} for="publishers">Publisher:</label>
                    <select className = {classes.Select} name="publishers" id="publisher">
                        {publishersArray.map((publisher)=>{
                            return <option selected = {this.state.publisherId && publisher._id == this.state.publisherId} value={publisher._id}>{publisher.name}</option>
                        })}
                        
             
                    </select>
                    <label className = {classes.Label} for="authors">Author:</label>
                    <select className = {classes.Select} name="authors" id="author">
                        {authorsArray.map((author)=>{
                            return <option selected = {this.state.authorId && author._id == this.state.authorId} value={author._id}>{author.name}</option>
                        })}
                        
             
                    </select>

                   

                  

                 
                    
                   
                   
                    <button disabled = {this.state.inputsValid == false? true: false} className={classes.Submit} > <img  src= {arrow} /></button>
                </form>
                
        </div>)
    }
}


export default  withMainStore( withRouter(EditBookScreen) );