import axios from 'axios'

import cookies from "js-cookies";



const instance = axios.create({
    baseURL: 'http://127.0.0.1:3002/',
    withCredentials: true,
    timeout: 10000,
});

instance.interceptors.request.use(
    async (config)=>{
        /*const token = await cookies.getItem('token');
        console.log('using token' , token)
        if (token){
            config.headers.Authorization = 'Bearer ' + token;
        }*/
        return config;
    },
    (err)=>{
        return Promise.reject('update',err);
    }
)
export default instance;