


import { search } from 'superagent';
import * as actionTypes from '../actions/actionTypes'


const initialState = {  search: {}, searchTerm: '', books:{}, purchases: {}}


const mainReducer = (state = initialState, action) => {
    switch(action.type){

        case actionTypes.ACTION_INIT:
            return initialState;

        case actionTypes.ACTION_CLEAR_SEARCH_BOOKS:
            return {...state, search: {...state.search, books:[]}}
        case actionTypes.ACTION_CLEAR_AUTHORS:
            return {...state,  authors:{}}
        case actionTypes.ACTION_CLEAR_PUBLISHERS:
            return {...state,  publishers:{}}

        case actionTypes.ACTION_SET_SEARCH_TERM:
            return {...state, searchTerm: action.value}
        case actionTypes.ACTION_DELETE_BOOK:
            const books = {};
            const searchBooks = [];
            state.search.books.forEach((id)=>{
                if(action.value!= id){
                    searchBooks.push(id)
                }
            })
            Object.keys(state.books).forEach((key)=>{
                if (action.value!= key){
                    books[key] = state.books[key]
                }
            })

            console.log('book deleted from state')

            return {...state, books:books, search: {...search, books:searchBooks}}


        case actionTypes.ACTION_SET_DATA:

          
         /*   if (action.data.search && state.searchTerm.trim().length == 0){
                return state;
            }*/
            const newState = {...state};
            const keys = Object.keys(action.data);
            for (let i = 0 ; i < keys.length; i++){
                const entityName = keys[i]
                //console.log('entity name', entityName)

                const entity = {...state[entityName]};
                const ids = Object.keys(action.data[entityName]);
                for (let j = 0 ; j < ids.length; j++){
                    const id = ids[j];
                    entity[id] = action.data[entityName][id]
                }
                newState[entityName] = {...entity}
            }
            return newState;
        
        default:
            return state;
    }
}

export default mainReducer;
