import * as actionTypes from './actionTypes'

import booksApi from '../../api/books'


const inProgressApiCalls = {}

let timer = null;

const apiCall = async (method, api, params, dispatch) => {
    const callKey = api + '_' + JSON.stringify(params);
    try{

       
        if (inProgressApiCalls[callKey]){
            return;
        }
        inProgressApiCalls[callKey] = true;
        console.log('params', params)
        let response = null;
        params.withCredentials = true
        switch(method){
            case 'get':
                response = await booksApi.get('/'+ api, params);
                break;
            case 'post':
                response = await booksApi.post('/'+ api, params);
                break;
            case 'put':
                response = await booksApi.put('/'+ api, params);
                break;
            case 'delete':
                response = await booksApi.delete('/'+ api, params);
                break;
        }
        inProgressApiCalls[callKey] = false;
        console.log(response.data);
        dispatch ({
            type: actionTypes.ACTION_SET_DATA,
            data: response.data
        });
             
            
    }catch(error){
        inProgressApiCalls[callKey] = false;
        console.log(error);
            
    }
}




export const getBook = (bookId)=>{

    return (dispatch, getState)=>{
        apiCall('get','books/' +bookId  ,{}, dispatch);
    }

   
        
}


export const getPublishers = ()=>{

    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_CLEAR_PUBLISHERS,
          
        });
        apiCall('get','publishers' ,{}, dispatch);
    }

   
        
}

export const getAuthors = ()=>{

    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_CLEAR_AUTHORS,
          
        });
        apiCall('get','authors' ,{}, dispatch);
    }

   
        
}


export const updateBook = (book)=>{

    return (dispatch, getState)=>{

        apiCall('put','books' ,{...book}, dispatch);
    }

   
        
}

export const addBook = (book)=>{

    return (dispatch, getState)=>{

        apiCall('post','books' ,{...book}, dispatch);
    }

   
        
}



















/*export const setSearchTerm =  (book)=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_SET_SEARCH_TERM,
            value: book
        });

       
    } 
}*/



export const searchBooks =  (book)=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_SET_SEARCH_TERM,
            value: book?book:''
        });
        dispatch ({
            type: actionTypes.ACTION_CLEAR_SEARCH_BOOKS
           
        });

        //if (book.trim().length > 0){

        if( timer){
            clearTimeout(timer)
        }
        timer = setTimeout(()=>{
            apiCall('get','books' +(book && book.trim().length > 0 ?'?book=' +book:'') ,{}, dispatch);
        }, 1000)
            
       // }else{
        //    dispatch ({
        //        type: actionTypes.ACTION_CLEAR_SEARCH_BOOKS
               
        //    });
       // }
    } 
}

export const allBooks =  ()=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_SET_SEARCH_TERM,
            value: ''
        });
        dispatch ({
                type: actionTypes.ACTION_CLEAR_SEARCH_BOOKS
               
            });

        //if (book.trim().length > 0){

        if( timer){
            clearTimeout(timer)
        }
        timer = setTimeout(()=>{
            apiCall('get','books/all' ,{}, dispatch);
        }, 0)
            
       // }else{
        //    dispatch ({
        //        type: actionTypes.ACTION_CLEAR_SEARCH_BOOKS
               
        //    });
       // }
    } 
}


export const init =  ()=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_INIT,
        });

      
    } 
}


export const purchaseBook =  (bookId)=>{
    return (dispatch, getState)=>{

        apiCall('post','purchases'  ,{bookId: bookId}, dispatch);
        
    } 
}


export const deleteBook =  (bookId)=>{
    return (dispatch, getState)=>{

        console.log('delete3', bookId)
        dispatch ({
            type: actionTypes.ACTION_DELETE_BOOK,
            value: bookId
        });

        apiCall('delete','books'  ,{headers:{bookId: bookId}}, dispatch);

        
    } 
}

















