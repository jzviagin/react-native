import * as actionTypes from './actionTypes'

import booksApi from '../../api/books'


import * as mainActions from './mainActions'
import cookies from "js-cookies";



const clearErrorAction = () => ()=>{
    return {
        type: actionTypes.ACTION_CLEAR_ERROR    
     };
}


const loginAction = ( user)=>{
    return {
        type: actionTypes.ACTION_LOGIN,
        user: user
    }
}





const setErrorMessageAction = (errorMessage)=>{
    return {
        type: actionTypes.ACTION_SET_ERROR_MESSAGE,
        payload: errorMessage
    }
}


const actionStartedAction = ()=>{
    return {
        type: actionTypes.ACTION_STARTED
    }
}

const signOutAction = ()=>{
    return {
        type: actionTypes.ACTION_SIGN_OUT
    }
}




export const tryLocalSignIn = ()=>{

    return async (dispatch, getState)=>{
        try{
            dispatch(actionStartedAction());
            console.log("try");
            //const token = await  cookies.getItem('token');// localStorage.getItem('token');
           // console.log("token", token);
         
                const response = await booksApi.get('/login', { withCredentials: true });

                if (response.data.user){
                    dispatch(loginAction(response.data.user)) ;
                    dispatch(mainActions.init())
                }else{
                    dispatch(setErrorMessageAction(''));
                }
       
    
            
        }catch(error){
            console.log("error", error);
            dispatch(setErrorMessageAction(error.response.data));
          
        }
    }
} 


export const signup = (data)=>{
    return async (dispatch, getState)=>{
        console.log(data);

        dispatch(actionStartedAction());

        try{

            console.log("signup1");
            const response = await booksApi.post('/signup', {
                password: data.password,
                username: data.username,
              });
              console.log("signup2");
              console.log(response);

              //console.log("token2", token);
              dispatch(loginAction(response.data.user));       
              dispatch(mainActions.init())     
        }catch(error){
            console.log("signup error", error.message);
            console.log(error);
            dispatch(setErrorMessageAction(error.response.data));
        }
    }

} 





export const signin = (data) => {

        return async(dispatch, getState)=>{
   

            dispatch(actionStartedAction());

            console.log(data);

            try{
                console.log('logging in');
                const response = await booksApi.get('/login', 
                {
                    withCredentials: true ,
                   
                    headers: {
                    username: data.username,
                    password: data.password
                  }});
                console.log('res!!!!!!!', response);
                if (response.data.user){
                    dispatch(loginAction( response.data.user));
                    dispatch(mainActions.init())
                    return;
                }
                dispatch(setErrorMessageAction('no token'))

                
            }catch(error){
                console.log('log in error', error);
                dispatch(setErrorMessageAction(error.response.data))
            }

    }
}



export const signout = ()=>{

    return async (dispatch, getState)=>{
        try{
            console.log("try");
            //await localStorage.setItem('token', '');
            cookies.removeItem('token')
            dispatch(signOutAction())
        }catch(error){
            console.log("error", error);
          
        }
    }

} 




