import axios from 'axios'
import {AsyncStorage} from 'react-native'

const token = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJlNGQ2OTljZDRhNTc2NjdhMTFiODg3YmRmZWFiYzU1MyIsInN1YiI6IjVlZjBjNjY5YjBjZDIwMDAzNGI5MjQ0ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.JwsPPTsqMNZ7TTTMYBNsR-XN-wZtuRsEUXe3EIPhRDc'

const instance = axios.create({
    baseURL: 'https://api.themoviedb.org/3/'
});

instance.interceptors.request.use(
    async (config)=>{
        if (token){
            config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
    },
    (err)=>{
        return Promise.reject(err);
    }
)
export default instance;