import React, {useContext, useState} from 'react';
import {View, StyleSheet, Text, ActivityIndicator} from 'react-native';
import {Image, Button} from 'react-native-elements'
import { Directions, ScrollView } from 'react-native-gesture-handler';

import { FontAwesome } from '@expo/vector-icons'; 

//import { withNavigationFocus} from 'react-navigation'
import useAuth from '../hooks/useAuth'

import {Context as AuthContext} from '../context/authContext'

import UserDetails from "../components/UserDetails"

import Modal from 'react-native-modal';
import MovieList from '../components/MovieList'


const HomeScreen = (props) => {

 

    const [LoginComponent,logOut] = useAuth();

        const {state, setUser,setLoading} = useContext(AuthContext);
        const [showModal, setShowModal] = useState(false);

        if(!state.user){
          props.navigation.navigate('loginFlow');
          return <></>
        }

    
  


  /**
   * < Text style = {styles.welcome}>Welcome {state.user.name}</Text>
        <View style={styles.imageContainer}>
        <Image
            style={styles.image}
        
            source={{ uri: state.user.photo  }}
            PlaceholderContent={<ActivityIndicator />}
            />
        </View>
   */
  
    
return <>
    <Modal isVisible={showModal}>
        <View style={{flex: 1, borderColor: 'grey', borderWidth: 2, borderRadius:10, overflow: 'hidden', backgroundColor:'white'}}>
            <Text style = {{ textAlign: 'center', fontSize: 22, padding:10, backgroundColor:'white'}}>Favorite Movies</Text>
            <MovieList favorites = {true} onMovieSelected = {()=> {setShowModal(false)}}></MovieList>
            <Button title="Close" onPress={()=>setShowModal(false)} />
          </View>
        </Modal>


    <View style = {styles.container}>
        <UserDetails userName = {state.user.name} photo = {state.user.photo}/>
        
        <View style = {{display: 'flex', flexDirection: 'row'}}>
           
            <Button buttonStyle ={styles.signOut} title = "Favorites" onPress = {()=>{setShowModal(true)}}></Button>
            <Button buttonStyle ={styles.signOut} title = "Log Out" onPress = {()=>{logOut()}}></Button>
        </View>

       
    </View>

      
    </>
};

const icon = <FontAwesome name="home" size={24} color="black" />

HomeScreen.navigationOptions = {
    title: 'Home',
    tabBarIcon: icon
  }



const styles = StyleSheet.create({

    container:{
        flex:1,
        display: "flex",
        alignItems: "center",
        justifyContent : "center"
       
    },
    welcome: {
        fontSize:20, margin:20
    },
    signOut:{
        marginTop: 20,
        borderRadius: 20,
        borderWidth:6,
        margin: 5
    },
   
    imageContainer: {
        marginTop: 20,
        marginBottom: 20,
        borderRadius:150,
        width: 300, height:300 ,
  
        borderWidth: 2,
        borderColor: "grey",
        overflow: 'hidden'
    },
    image: {
        width: '100%' , height:'100%' 
     
    }
});

export default HomeScreen;