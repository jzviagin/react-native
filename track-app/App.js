import React, {useContext,useEffect, useCallback} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SigninScreen from './src/screens/SigninScreen'
import SignupScreen from './src/screens/SignupScreen'
import TrackCreateScreen from './src/screens/TrackCreateScreen'
import TrackDetailScreen from './src/screens/TrackDetailScreen'
import ResolveAuthScreen from './src/screens/ResolveAuthScreen'
import TrackListScreen from './src/screens/TrackListScreen'
import { createAppContainer,  createSwitchNavigator } from 'react-navigation'

import {createStackNavigator } from 'react-navigation-stack'

import {createBottomTabNavigator } from 'react-navigation-tabs'
import AccountScreen from './src/screens/AccountScreen';
import {Provider as AuthProvider, Context as AuthContext} from './src/context/authContext'
import {Provider as LocationProvider, Context as LocationContext} from './src/context/locationContext'
import {Provider as TrackProvider, Context as TrackContext} from './src/context/trackContext'

//import useLocation from './src/hooks/useLocation'

import * as locationActions from './src/store/locationActions'


import {createStore, applyMiddleware, compose, combineReducers} from 'redux';

import {Provider, connect} from 'react-redux'

import {SafeAreaView} from 'react-navigation'

import {setNavigator} from './src/navigationRef'

import { FontAwesome } from '@expo/vector-icons'; 

import locationReducer from './src/store/locationReducer'

import AsyncStorage from '@react-native-community/async-storage';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web


import { PersistGate } from 'redux-persist/integration/react'

import  {requestPermissionsAsync, watchPositionAsync, Accuracy, startLocationUpdatesAsync, stopLocationUpdatesAsync, hasStartedLocationUpdatesAsync} from 'expo-location';
import * as TaskManager from 'expo-task-manager';

//import DeviceInfo from 'react-native-device-info';



const persistConfig = {
  key: 'root',
  version: 0,
  storage: AsyncStorage
}



const persistedReducer = persistReducer(persistConfig, locationReducer);

const store = createStore(persistedReducer);

console.log('created store');

const persistor = persistStore(store);





const locationTaskFunc = (data)=>{
  //console.log('app name', DeviceInfo.getApplicationName);
  console.log('locationTask', data.data.locations.length);

  TaskManager.getRegisteredTasksAsync().then(tasks => {
    console.log('tasks', tasks);
  })
  

 
  store.dispatch(locationActions.addLocations(data.data.locations));
  

}





TaskManager.defineTask('locationTask', locationTaskFunc);
TaskManager.getRegisteredTasksAsync().then(tasks => {
  console.log('tasks', tasks);
})


//global.tracking = false;


store.subscribe(()=>{


 

  const state = store.getState();

  
  const shouldTrack = state.recording;
  const startWathing = async () =>{
      try{
          await requestPermissionsAsync();
          hasStartedLocationUpdatesAsync('locationTask').then(async (started)=>{
            console.log('has started ', started)

            if(!started){
                  /*subscriber =*/  await startLocationUpdatesAsync ('locationTask', {
                  accuracy: Accuracy.BestForNavigation,
                  deferredUpdatesInterval : 2000,
                  enableHighAccuracy:true,
                  deferredUpdatesDistance : 0,
                  foregroundService :{
                      notificationTitle: 'Tracker is using location',
                      notificationBody: 'Tracker is using location',
                      //notificationColor: '#RRGGBB'
                  }
              });
            }

          })
         
      }catch (e){
          setErr(e)
      }
  };

  console.log('should track', shouldTrack);

  if(shouldTrack){
     // console.log('should track')
      startWathing();
  }else{
    hasStartedLocationUpdatesAsync('locationTask').then((started)=>{

      if(started){
        stopLocationUpdatesAsync('locationTask');
      }

    })
  }
});



  
  
  


const icon = <FontAwesome name="th-list" size={24} color="black" />

const stackNavigator = createStackNavigator({
  TrackList: TrackListScreen,
  TrackDetails: TrackDetailScreen
})
stackNavigator.navigationOptions = {
  title: 'Tracks',
  tabBarIcon: icon
}

const switchNavigator = createSwitchNavigator({
  resolveAuth: ResolveAuthScreen,
  loginFlow:  createStackNavigator({
    Signup: SignupScreen,
    Signin: SigninScreen
  }),
  mainFlow: createBottomTabNavigator({
    TrackListFlow: stackNavigator,
    TrackCreate: TrackCreateScreen,
    Account: AccountScreen

  })
});




const App = createAppContainer(switchNavigator);




const mapStateToProps = (state) =>{
  return {
      recording: state.recording,
      locations: state.locations,
      name: state.name,
      currentLocation: state.currentLocation
  }
}





const mapDispatchToProps = (dispatch) => {
  return {
      startRecording: ()=>dispatch(locationActions.startRecording()),
      stopRecording: ()=>dispatch(locationActions.stopRecording()),
      addLocations: (locations)=>dispatch(locationActions.addLocations(locations)),
      setState: (state)=>dispatch(locationActions.setState(state)),
      setName: (name)=>dispatch(locationActions.setName(name)),
      reset: ()=>dispatch(locationActions.reset())
  }
}

//useLocation();

const AppWrapper = connect(mapStateToProps, mapDispatchToProps)( (props)=>{

  /*const callback = useCallback((locations)=> {
    console.log('original callback:', locations.length);
    props.addLocations(locations)
  }, []);*/
  console.log('AppWrapper rendering');
  /*const callback = (locations)=> {
    console.log('original callback:', locations.length);
    props.addLocations(locations)
  }*/
  //const [err] = useLocation(props.recording,  callback);



  return <App ref = { (navigator) => {setNavigator(navigator)}}/>
});


/**
 * 
 *  <PersistGate loading={null} persistor={persistor}>
            <AppWrapper/>
          </PersistGate>
 */



export default () => {

  
 
  console.log('App rendering');
  //tryLocalSignIn();
  return (
    

    <AuthProvider>
      <TrackProvider>
        <Provider store = {store}>
          <PersistGate loading={null} persistor={persistor}>
            <AppWrapper/>
          </PersistGate>
        </Provider>
      </TrackProvider>
    </AuthProvider>




  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
