import * as Location from 'expo-location'

const tenMetersWithDegrees = 0.0001;

const getLocation = increment => {
    console.log((new Date()).getTime());
    return {
        timestamp: new Date().getTime(),
        coords : {
            speed: 0,
            heading: 0,
            accuracy: 5,
            altitudeAcccuracy: 5,
            altitude: 5,
            latitude:37.33233 + increment * tenMetersWithDegrees,
            longitude: -122.0312186 + increment * tenMetersWithDegrees,
        }

    }
}


let counter = 0;
setInterval ( () => {
    //console.log("emitting")
    Location.EventEmitter.emit('Expo.locationChanged', {
        watchId: Location._getCurrentWatchId(),
        location : getLocation(counter)
    });
    counter++;
}, 1000);
