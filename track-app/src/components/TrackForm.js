import React, { useContext } from 'react';
import {Input, Button, Text} from 'react-native-elements';
import {StyleSheet, View} from 'react-native'
import Spacer from './Spacer'

import {connect} from 'react-redux'


//import {Context as LocationContext} from '../context/locationContext'
import {Context as TrackContext} from '../context/trackContext'
import useSaveTrack from '../hooks/useSaveTrack'
import ProgressBar from '../components/ProgressBar'

import * as locationActions from '../store/locationActions'

const TrackForm = (props) =>{

    //const {setName, startRecording, state: locationState , stopRecording} =  useContext(LocationContext);
    const { state : trackState, fetchTracks, createTrack} =  useContext(TrackContext);

    const [saveTrack] = useSaveTrack();

    console.log('isSaving', trackState.isSaving)

/*if(trackState.isSaving){
        return <Text value="Please wait..."></Text>
    }*/

    if(trackState.isSaving){
        return <ProgressBar />
    }

    return <View style = {styles.form}> 
        <Spacer>
            <Input placeHolder= "Enter Name"
            value = {props.name}
             onChangeText = { (name)=> { props.setName(name)}}/>
        </Spacer>
       {props.recording ?  <Button title="Stop Recording" onPress= {props.stopRecording}/> : <Button title="Start Recording" onPress= {props.startRecording}/>}
       {(!(props.recording) && props.locations.length) ?  <Button title="Save Recording" disabled= {
           (()=>{
               if (!(props.name)){
                   return true;
               }
               let str = props.name.trim();
               if (str.length ===0){
                   return true;
               }
               return false;
           })()
       } onPress= {saveTrack}/> : null}
    </View> 
};


const styles = StyleSheet.create({
    form: {
    }
});




const mapStateToProps = (state) =>{
    return {
        recording: state.recording,
        locations: state.locations,
        name: state.name,
        currentLocation: state.currentLocation
    }
}



const mapDispatchToProps = (dispatch) => {
    return {


        startRecording: ()=>dispatch(locationActions.startRecording()),
        stopRecording: ()=>dispatch(locationActions.stopRecording()),
        addLocations: (locations)=>dispatch(locationActions.addLocations(locations)),
        setState: (state)=>dispatch(locationActions.setState(state)),
        setName: (name)=>dispatch(locationActions.setName(name)),
        reset: ()=>dispatch(locationActions.reset())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TrackForm);



