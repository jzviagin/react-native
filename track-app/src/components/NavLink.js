import React from 'react'
import {StyleSheet, TouchableOpacity} from 'react-native'
import {withNavigation } from 'react-navigation'

import {Text} from 'react-native-elements'
import  Spacer from './Spacer'
const NavLink = (props)=>{
    console.log(props);
    return (
        <TouchableOpacity onPress = { ()=> {props.navigation.navigate(props.link)}}>
        <Spacer>
            <Text style = {styles.link}>{props.text}</Text>
        </Spacer>
        
    </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    link:{
        color: 'blue'
    }
});

export default withNavigation(NavLink);