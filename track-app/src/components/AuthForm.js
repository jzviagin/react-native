import React, {useState} from 'react';
import { StyleSheet, View} from 'react-native';
import {Text, Input, Button} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import Icon from 'react-native-vector-icons/Feather';



const SignupScreen = ({navigation, title, buttonTitle, buttonAction, errorMessage, email: providedEmail, password: providedPassword}) => {

    const [email, setEmail] = useState(providedEmail);
    const [password, setPassword] = useState(providedPassword)

    console.log(providedEmail);
    const checkInputs = ()=>{
        if (password.includes(' ') || email.trim().length === 0 || password.trim().length === 0){
            return false;
        }
        return true;
    }

    //const {state, signup} = useContext(AuthContext);

    let passwordInput;

    return (
        <View style= {styles.container} >
            <Spacer>
                <Text style = {styles.title}>{title}</Text>
            </Spacer>

            <Spacer>
                <Input  
                leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                keyboardType="email-address"
                containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft:0, paddingRight:0 }}
                returnKeyType="next"
                onSubmitEditing={() => { passwordInput.focus(); }}
                leftIcon={
                    <Icon style={{ margin:0}}
                      name='mail'
                      size={18}
                      color='grey'
                    />
                  }
                    label="Email"
                    value = {email} 
                    onChangeText = { (newEmail)=> { setEmail(newEmail)}}
                    autoCapitalize = "none"
                    autoCorrect = {false}>

                    </Input>
            </Spacer>
            <Spacer>
                <Input 
                    ref = {(input)=> { passwordInput= input}}
                    leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                    containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft:0, paddingRight:0 }}
                    leftIcon={
                        <Icon
                          name='lock'
                          size={18}
                          color='grey'
                        />
                      }
                    value = {password} 
                    onChangeText = { (newPassword)=> { setPassword(newPassword)}}
                    autoCapitalize = "none"
                    autoCorrect = {false}
                    returnKeyType="none"
                    onSubmitEditing={()=> {
                        if(checkInputs() == false){
                            return;
                        }
                        buttonAction(email, password)
                    }}
                    secureTextEntry
                    enablesReturnKeyAutomatically
                ></Input>
            </Spacer>
           
            {errorMessage ? ( <Spacer><Text style = {styles.errorMessage}>{errorMessage}</Text></Spacer> ): null}
            <Spacer>
            <Button disabled = {checkInputs() == false} title={buttonTitle} onPress = {()=> {buttonAction(email, password)}}/>
            </Spacer>
        </View>
        )
};


SignupScreen.navigationOptions =  {
    title:'Home',
    header: null
}


const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        marginBottom: 0
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    },
    title:{
        fontSize:30,
        fontWeight: "bold"
    },

    inputContainer: {
        margin:0,
        padding:0,
        backgroundColor:'red'
    },
    input: {
        margin:0,
        padding:0,
        backgroundColor:'blue'
    },

});

export default SignupScreen;