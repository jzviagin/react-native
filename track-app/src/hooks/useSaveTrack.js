import {useContext} from 'react';
import {Context as TrackContext} from '../context/trackContext'
//import {Context as LocationContext} from '../context/locationContext'
import {navigate} from '../navigationRef'
import {connect} from 'react-redux'

import { shallowEqual, useSelector , useDispatch} from 'react-redux'

import * as locationActions from '../store/locationActions'
//import {Context as AuthContext} from '../context/authContext'

const useSaveTrack =  ()=>{
    const {createTrack} = useContext(TrackContext);

    const locations = useSelector( (state)=>{
        return state.locations;
    });
    const name = useSelector( (state)=>{
        return state.name;
    });
    const dispatch = useDispatch();

    //const {state: locationState, reset} = useContext(LocationContext);
    //const {state: authState} = useContext(AuthContext);


    const saveTrack = async ()=> {
        console.log('save track')


        console.log('name', name);
        await createTrack(locations, name);
        dispatch(locationActions.reset());
        navigate('TrackList')
    }
   
    return [saveTrack];
}



export default useSaveTrack;


