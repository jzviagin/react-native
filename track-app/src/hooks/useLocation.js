import {useState, useEffect, useContext} from 'react';
import {AsyncStorage} from 'react-native'

import  {requestPermissionsAsync, watchPositionAsync, Accuracy, startLocationUpdatesAsync, stopLocationUpdatesAsync} from 'expo-location';
import * as TaskManager from 'expo-task-manager';

import { shallowEqual, useSelector , useDispatch} from 'react-redux'

import * as locationActions from '../store/locationActions'

//import {Context as LocationContext} from '../context/locationContext'



//let globalCallback;
//let globalAproxCallback;



const locationTaskFunc = (data)=>{
    console.log('locationTask', data.data.locations.length);

    console.log('globalCallback', global.globalCallback);
   
    if(global.globalCallback){
        global.globalCallback(data.data.locations);
    }
    

}





TaskManager.defineTask('locationTask', locationTaskFunc);



export default (/*shouldTrack, callback*//*, aproxCallback*/)=> {
    const [err, setErr] = useState(null);

    //console.log('useLocation Called callback', callback);

    const shouldTrack = useSelector( (state)=>{
        return state.recording;
    });
    
    const dispatch = useDispatch();
    
  
    useEffect ( ()=> {

        //global.globalCallback = callback;

        global.globalCallback = (locations ) =>  {
            dispatch(locationActions.addLocations(locations));
        }
        const startWathing = async () =>{
            try{
                await requestPermissionsAsync();
                /*subscriber =*/  await startLocationUpdatesAsync ('locationTask', {
                    accuracy: Accuracy.BestForNavigation,
                    deferredUpdatesInterval : 200,
                    enableHighAccuracy:true,
                    deferredUpdatesDistance : 0,
                    foregroundService :{
                        notificationTitle: 'Tracker is using location',
                        notificationBody: 'Tracker is using location',
                        //notificationColor: '#RRGGBB'
                    }
                });
            }catch (e){
                setErr(e)
            }
        };
    
    

        if(shouldTrack){
            console.log('should track')
            startWathing();
        }else{
            console.log('should not track')
            stopLocationUpdatesAsync('locationTask');
          
        }
        return ()=>{
            console.log('eseLocation unmount')
            stopLocationUpdatesAsync('locationTask');
           
            
        }
    }, [shouldTrack/*, callback*/]);
    return [err]
}