import { getPreciseDistance } from 'geolib';


export default ()=>{


    const calcTrackDistance = (track)=>{
        let sum = 0 ;
        let prevLocationIndex = 0;
        for (let i = 1 ; i < track.locations.length; i++){

            const distance = getPreciseDistance({
                latitude: track.locations[prevLocationIndex].coords.latitude,
                longitude: track.locations[prevLocationIndex].coords.longitude
            }, 
            {
                latitude: track.locations[i].coords.latitude,
                longitude: track.locations[i].coords.longitude
            }
            );
            if (true){
                prevLocationIndex = i;
                sum+= distance;
            }
        }
        return sum;
    }

    const getTrackStartStartTime = (track) => {
        const date = new Date(track.locations[0].timestamp);
        return (date.getDate() + '.' + (date.getMonth() +1)  + '.' + date.getFullYear()  + ' ' + date.getHours() + ':' + pad(date.getMinutes()));
    }

    const pad = (num)=>{
        if (num < 10){
            return '0' + num;
        }
        return ''+ num;
    }

    const getTrackDuration = (track)=>{
        const milis = track.locations[track.locations.length -1 ].timestamp - track.locations[0].timestamp;
        const date = new Date(milis);
        const seconds = date.getUTCSeconds();
        const hours= date.getUTCHours();
        const minutes = date.getUTCMinutes();
        return pad(hours) + ":" + pad(minutes) + ":" + pad(seconds);
    }

    const getPace = (track) => {
        //console.log('get pace', track)
        const milis = track.locations[track.locations.length -1 ].timestamp - track.locations[0].timestamp;
        const distance = calcTrackDistance(track)/1000;
        const milisPerKm = milis/ distance;
        return ((milisPerKm /1000)/ 60);
    }
    return [calcTrackDistance, getTrackStartStartTime, getTrackDuration, getPace];
} 