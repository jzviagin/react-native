import React, {useEffect, useContext, useState, useCallback} from 'react';
import { StyleSheet,View} from 'react-native';
import {Text} from 'react-native-elements';
import Map from '../components/Map';
import {SafeAreaView, withNavigationFocus} from 'react-navigation'
import TrackDetails from  '../components/TrackDetails'



import { FontAwesome } from '@expo/vector-icons'; 

//import  {requestPermissionsAsync, watchPositionAsync, Accuracy} from 'expo-location'

//import useLocation from '../hooks/useLocation'

import TrackForm from '../components/TrackForm';

import {connect} from 'react-redux'

//import '../_mockLocation'
//import {Context as LocationContext} from '../context/locationContext'
import {Context as TrackContext} from '../context/trackContext'
import ProgressBar from '../components/ProgressBar'

import * as locationActions from '../store/locationActions'




const icon = <FontAwesome name="plus" size={24} color="black" />

const TrackCreateScreen = (props) => {
    //const {state : locationState, startRecording, stopRecording, addLocations/*, addAproxLocation*/} = useContext(LocationContext);


    const { state : trackState, fetchTracks, createTrack} =  useContext(TrackContext);




    /*console.log('isFocused', props.isFocused)
    useEffect (()=>{ 
        startRecording();
        return () => {

        }
    }, []);*/

    /*if(trackState.isSaving){
        return (
            <SafeAreaView style = {styles.spinnerTextContainer} forceInset= {{top: 'always'}}>
                <Text style = {styles.spinnerText} value="Please wait...">Please wait...</Text>
            </SafeAreaView>
       
        )
    }*/

    if(trackState.isSaving){
        return <ProgressBar/>
    }

    

//{err? <Text>Please enable location services </Text>: null}
    
    return (
        <SafeAreaView   style = {styles.main} forceInset= {{top: 'always'}}>
            <Text h3>Track Record {/*locationState.currentLocation?locationState.currentLocation.coords.accuracy: ''*/}</Text>
            <Map />
            
            <TrackForm />
            <TrackDetails/>
        </SafeAreaView>
    )
};



const styles = StyleSheet.create({
    spinnerText:{
        fontSize:40
    },
    spinnerTextContainer:{
        display: "flex",
        flex: 1,
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    main:{
        display: 'flex',
        flex: 1
    }
});

TrackCreateScreen.navigationOptions = {
    title: 'Add track',
    tabBarIcon:icon
}


const mapStateToProps = (state) =>{
    return {
        recording: state.recording,
        locations: state.locations,
        name: state.name,
        currentLocation: state.currentLocation
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        //onFetchOrders: (token, userId) => dispatch(locationActions.fetchOrdersStart (token, userId)),
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})

        startRecording: ()=>dispatch(locationActions.startRecording()),
        stopRecording: ()=>dispatch(locationActions.stopRecording()),
        addLocations: (locations)=>dispatch(locationActions.addLocations(locations)),
        setState: (state)=>dispatch(locationActions.setState(state)),
        setName: (name)=>dispatch(locationActions.setName(name)),
        reset: ()=>dispatch(locationActions.reset())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(TrackCreateScreen));
