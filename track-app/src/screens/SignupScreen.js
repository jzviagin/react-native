import React, { useContext, useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import {NavigationEvents} from 'react-navigation'
import {Text} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import {Context as AuthContext} from '../context/authContext'
import NavLink from '../components/NavLink'

import AuthForm from '../components/AuthForm'
import ProgressBar from '../components/ProgressBar'

import {SafeAreaView} from 'react-navigation'



const SignupScreen = ({navigation}) => {



    const {state, signup, clearError} = useContext(AuthContext);
    console.log('token: ', state.token)
    console.log('errorMessage: ', state.errorMessage);

    const [email , setEmail] = useState('');
    const [password, setPassword]= useState('') ;

    if (state.inProgress){
        return <ProgressBar/>
    }



    return (
        <SafeAreaView style = {styles.container} forceInset= {{top: 'always'}}>

                            <NavigationEvents onWillFocus = { ()=>{
                                clearError();
                            }}/>
                            <AuthForm email = {email}  password = {password} title = "Sign up for tracker" buttonTitle = "Sign Up" buttonAction = {
                                (email, password) => {
                                    setPassword(password);
                                    setEmail(email);
                                    signup({email: email,
                                        password: password});
                                }
                            } errorMessage  = {state.errorMessage}/>
                        <NavLink  link = "Signin" text = "Already have an account? Sign in instead"/>
        </SafeAreaView>
        
    )

    
};


SignupScreen.navigationOptions =  {
    title:'Home',
    header: null,
    headerShown: false
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        flexDirection: 'column',
        marginBottom: 0,
        paddingTop:80
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    }
});

export default SignupScreen;