import React, {useContext} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Context as TrackContext} from '../context/trackContext'
import MapView, {Polyline, Circle} from 'react-native-maps'
import { Directions } from 'react-native-gesture-handler';
import useTrackTools from '../hooks/useTrackTools'


const TrackDetailScreen = (props) => {

    const {state} = useContext(TrackContext);

    console.log('track screen state: ', state)

    const id = props.navigation.getParam("id");
    const track = state.tracks.find( (elem) => {return elem._id === id});



    console.log('track screen track: ', track);

    console.log('track screen props: ', props);

    console.log('track screen props: ', id);
    const points = [];
    for (let i = 0 ; i < track.locations.length ; i++){
        points.push({
            latitude: track.locations[i].coords.latitude,
            longitude: track.locations[i].coords.longitude
        })
    }
    const [calcTrackDistance, getTrackStartStartTime, getTrackDuration, getPace] = useTrackTools();

    
    //return <Text style = {{fontSize:48}}>TrackDetailScreen</Text>
return <>
        <  Text style = {{fontSize:48}}>{track.name}</Text>



        <MapView style = {styles.map}
        initialRegion = {{
            latitude:track.locations[0].coords.latitude,
            longitude: track.locations[0].coords.longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
        }}
        >

            <Polyline coordinates = {points}/>
            <Circle
                center = {track.locations[0].coords}
                radius = {30}
                strokeColor = "rgba(158,158,255, 1.0)"
                fillColor = "rgba(158,158,255,0.3)"
            />
        </MapView>
        <View style = {styles.rows}>
            <View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Start Time: </Text>
                    <Text style = {styles.dataText}>{getTrackStartStartTime(track)}</Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Duration: </Text>
                    <Text style = {styles.dataText}>{getTrackDuration(track)} </Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Distance: </Text>
                    <Text style = {styles.dataText}>{(calcTrackDistance(track)/ 1000).toFixed(2) +' KM'} </Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Average Pace: </Text>
                    <Text style = {styles.dataText}>{getPace(track).toFixed(2) + ' Min/KM'} </Text>
                </View>
            </View>
        </View>
    </>
};



const styles = StyleSheet.create({
    map:{
        height: 300
    },
    rows: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flex:1
    },
    columns: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 22
    },
    dataText: {
        fontSize: 20
    }
});

export default TrackDetailScreen;