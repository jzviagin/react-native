import React, { useContext, useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {NavigationEvents} from 'react-navigation'
import {Text} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import {Context as AuthContext} from '../context/authContext'
import NavLink from '../components/NavLink'

import AuthForm from '../components/AuthForm'
import ProgressBar from '../components/ProgressBar'
import {SafeAreaView} from 'react-navigation'



const SigninScreen = ({navigation}) => {



    const {state, signin, clearError} = useContext(AuthContext);
    console.log(clearError);
    console.log('token: ', state.token)
    console.log('errorMessage: ', state.errorMessage);
    console.log('inProgress: ', state.inProgress);

        
    const [email , setEmail] = useState('');
    const [password, setPassword]= useState('') ;

    if (state.inProgress){
        return <ProgressBar/>
    }




    console.log('email', email, 'password', password);


    return (
        <SafeAreaView style = {styles.container} forceInset= {{top: 'always'}}>
            <NavigationEvents onWillFocus = { ()=>{
                clearError();
            }}/>
            < AuthForm email = {email}  password = {password}  title = "Sign In to your Accont" buttonTitle = "Sign In" buttonAction = {
                
                (email, password) => {
                    setPassword(password);
                    setEmail(email);
                    signin({email: email,
                        password: password});
                }
                 } errorMessage  = {state.errorMessage}/>
            <NavLink  link = "Signup" text = "Don't have an account? Sign Up instead"/>
        </SafeAreaView>
        
    )

    
};


SigninScreen.navigationOptions =  {
    title:'Home',
    header: null,
    headerShown: false
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'flex-start',
        flexDirection: 'column',
        marginBottom: 0,
        paddingTop:80
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    }
});

export default SigninScreen;