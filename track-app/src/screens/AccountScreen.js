import React, { useContext} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {NavigationEvents} from 'react-navigation'
import { Button} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import {SafeAreaView} from 'react-navigation'
import {Context as AuthContext} from '../context/authContext'
import NavLink from '../components/NavLink'

import AuthForm from '../components/AuthForm'

import { FontAwesome } from '@expo/vector-icons'; 
const icon = <FontAwesome name="gear" size={24} color="black" />



const AccountScreen = ({navigation}) => {



    const { signout} = useContext(AuthContext);


    return (
        <SafeAreaView forceInset= {{top: 'always'}}>

        <Spacer>
            <Button title="Sign out" onPress = {()=> {signout()}}/>
        </Spacer>

        </SafeAreaView>
        
    )

    
};


AccountScreen.navigationOptions =  {
    title:'Account',
    tabBarIcon: icon,
    header: null,
    headerShown: false
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        marginBottom: 250
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    }
});

export default AccountScreen;