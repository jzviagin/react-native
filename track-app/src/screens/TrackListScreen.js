import React, {useEffect, useContext} from 'react';
import {View, StyleSheet, Text, Button, FlatList, TouchableOpacity} from 'react-native';
import {ListItem} from 'react-native-elements'
import {Context as TrackContext}  from '../context/trackContext'
import {NavigationEvents} from 'react-navigation'
import useTrackTools from '../hooks/useTrackTools'
import { FontAwesome } from '@expo/vector-icons'; 


const TrackListScreen = (props) => {
    const {state, fetchTracks} = useContext(TrackContext);
   /* useEffect  (()=> {
        fetchTracks();
    },[])*/
    const [calcTrackDistance, getTrackStartStartTime] = useTrackTools();
//<ListItem chevron = {true} title= {item.item.name + ' Distance: ' + calcTrackDistance(item.item)}/>
    //console.log(state);


    return (
        <>
        <NavigationEvents onWillFocus = {() => { 
             fetchTracks();
        }} />

        
        
        <FlatList data = {state.tracks}
            keyExtractor = {item => item._id}
            renderItem = {
                (item) => {
                    //console.log('item', item)
                    
                    return (<TouchableOpacity style= {styles.itemContainer} onPress = { ()=> {props.navigation.navigate('TrackDetails', {id: item.item._id})}}>
                        <View style= {styles.itemContainerData}>
                            <Text style= {styles.itemTitle} >{item.item.name}</Text>
                            <Text style= {styles.itemTime}>{getTrackStartStartTime(item.item)}</Text>
                        </View>
                        <View style= {styles.chevron}>
                            <FontAwesome  name="chevron-circle-right" size={34} color="black" />
                        </View>
                       

                    </TouchableOpacity>
                    )
                }
            }>
        
        </FlatList>


        
        </>
        )
};

TrackListScreen.navigationOptions = {
    title: 'Tracks'
}



const styles = StyleSheet.create({
    itemContainerData: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: "flex-start",
        margin:1,
        backgroundColor: 'white'
    },
    itemContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: "center",
        margin:1,
        backgroundColor: 'white'
    },
    itemTitle: {
        padding: 10,
        fontSize:22,
        fontWeight:'bold'
    },
    itemTime: {
        padding: 10
    },
    chevron:{
        display: "flex",
        flex: 1,
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginEnd: 15
        
    }
});

export default TrackListScreen;