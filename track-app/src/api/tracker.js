import axios from 'axios'
import {AsyncStorage} from 'react-native'



const instance = axios.create({
    baseURL: 'http://ec2-18-217-89-47.us-east-2.compute.amazonaws.com/'
});

instance.interceptors.request.use(
    async (config)=>{
        const token = await AsyncStorage.getItem('@trackApp:token');
        if (token){
            config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
    },
    (err)=>{
        return Promise.reject(err);
    }
)
export default instance;