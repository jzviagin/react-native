import createDataContext from './createDataContext'
import {AsyncStorage} from 'react-native'
import {navigate} from '../navigationRef'


import trackerApi from '../api/tracker'

const authReducer = (state, action) => {
    switch(action.type){
        case 'test':
            console.log ('test dispatched')
            return state;
        case 'set_token':
            return {...state, token: action.payload, errorMessage: '',  inProgress: false};


        case 'set_error_message':
            return {...state, errorMessage: action.payload,  inProgress: false};

        case 'clear_error':
            return {...state, errorMessage: ''};
        case 'action_started':
            return {...state, inProgress: true};

        default:
            return state;
    }
}
const clearError = (dispatch) => ()=>{
    dispatch({
        type: 'clear_error'    
     });
}

const tryLocalSignIn = (dispatch) => async ()=>{
    try{
        console.log("try");
        const token = await AsyncStorage.getItem('@trackApp:token');
        console.log("token", token);
        if (token){
            
            dispatch({
                type: 'set_token',
                payload: token
            });
            navigate('mainFlow', null);
            return;
        }
        
    }catch(error){
        console.log("error", error);
      
    }
    navigate('loginFlow', null);
        
    
}


const signup = (dispatch) => async (data) => {
        //make api request 
        //if we sign up, modify the state

        //show message on failure

        console.log(data);

        dispatch({
            type: 'action_started'
        });

        try{
            const response = await trackerApi.post('/signup', {
                email: data.email,
                password: data.password
              });
              console.log(response);
              const token3 = await AsyncStorage.getItem('@trackApp:token');
              console.log("token3", token3);
              await AsyncStorage.setItem('@trackApp:token', response.data);
              const token = await AsyncStorage.getItem('@trackApp:token');
              console.log("token2", token);
              dispatch({
                  type: 'set_token',
                  payload: response.data
              });
              navigate('mainFlow', null);
            
        }catch(error){
            console.log(error);
            dispatch({
                type: 'set_error_message',
                payload: error.response.data
            })
        }
            
      

    
}

const signin = (dispatch) =>  async (data) => {


           //make api request 
        //if we sign up, modify the state

        //show message on failure

        dispatch({
            type: 'action_started'
        });

        console.log(data);

        try{
            const response = await trackerApi.post('/login', {
                email: data.email,
                password: data.password
              });
              console.log(response);
              await AsyncStorage.setItem('token', response.data);
              dispatch({
                  type: 'set_token',
                  payload: response.data
              });
              navigate('mainFlow', null);
            
        }catch(error){
            console.log(error.response.data);
            dispatch({
                type: 'set_error_message',
                payload: error.response.data
            })
        }

    }



const signout = (dispatch) => async  (data) => {
    try{
        console.log("try");
        await AsyncStorage.setItem('@trackApp:token', '');
        console.log("success");
        navigate('loginFlow', null);
        
    }catch(error){
        console.log("error", error);
      
    }

}





export const {Provider, Context} = createDataContext(authReducer, {signin, signout, signup, clearError, tryLocalSignIn}, {errorMessage: '',  inProgress: false})