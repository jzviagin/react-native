import createDataContext from './createDataContext'


import  {requestPermissionsAsync, watchPositionAsync, Accuracy} from 'expo-location'

import {  AsyncStorage } from 'react-native';


import trackerApi from '../api/tracker'

import {getPreciseDistance} from 'geolib'

const locationReducer = (state, action) => {
    switch(action.type){
        case 'start_recording':
            return {...state, recording: true, locations:[]}
        case 'stop_recording':
            return {...state, recording: false}

        case 'add_locations':
                console.log('currect locations', state.locations.length);
                if (state.recording){

                    //let newLocations = [];
                    //for (let i = 0; i < 100 ; i++){
                    //    newLocations.push(action.location);
                   // }
                   /* if (state.locations.length === 0){
                        for (let i = 0 ; i < 10000; i++){
                            action.locations = [...action.locations, action.locations[action.locations.length -1] ];
                        }
                    }*/

                    let newLocations = [...state.locations];
                    for (let i = 0; i < action.locations.length ; i++){
                        console.log('adding new locations: ', newLocations.length);
                        if (newLocations.length != 0){
                            const distance = getPreciseDistance({
                                latitude: newLocations[newLocations.length -1 ].coords.latitude,
                                longitude: newLocations[newLocations.length -1].coords.longitude
                            }, 
                            {
                                latitude: action.locations[i].coords.latitude,
                                longitude: action.locations[i].coords.longitude
                            });
                            console.log('distance', distance);
                            console.log('accuracy', action.locations[i].coords.accuracy)
                            if (true/*distance >10 &&  action.locations[i].coords.accuracy < 12*/){
                                console.log('meets creteria');
                                newLocations.push(action.locations[i]); 
                            }else{
                                console.log('not meets creteria');
                            }
                        }else{
                            console.log('1st location');
                            newLocations.push(action.locations[i]);
                        }
                    }

                   
                    return {...state, locations: [...newLocations], currentLocation: action.locations[action.locations.length -1]}

                }else{
                    return state;
                }

      /*  case 'add_aprox_location':
            if (state.recording){
                return {...state, currentLocation: action.location}

            }else{
                return state;
            }*/
        case 'set_name':
            console.log(action.name);
            return {...state, name: action.name}

        case 'set_state':
            return action.state;

        case 'reset':
            return {...state, locations: [], name: ''}
    

        default:
            return state;
    }
}





const startRecording = dispatch => async ()=>{
    dispatch ({
        type: 'start_recording'
    });


}




const stopRecording = dispatch => ()=>{
    dispatch ({
        type: 'stop_recording'
    })
}

const addLocations = dispatch => (locations)=>{
    dispatch(
        {
            type: 'add_locations',
            locations: locations
        }
    )
}


const setState = dispatch => (state)=>{
    dispatch(
        {
            type: 'set_state',
            state: state
        }
    )
}

/*const addAproxLocation = dispatch => (location)=>{
    dispatch(
        {
            type: 'add_aprox_location',
            location: location
        }
    )
}*/

const setName = dispatch => (name)=>{
    dispatch(
        {
            type: 'set_name',
            name: name
        }
    )
}

const reset = dispatch => ()=>{
    dispatch(
        {
            type: 'reset'
        }
    )
}
/*let currentData = await AsyncStorage.getItem('@trackApp:locationData');
let recording = false;
if(currentData && currentData.length > 0){
    recording = true;
}*/

/*AsyncStorage.getItem('@trackApp:locationState').then(state=>{
     
    if (state){
      const parsedState = JSON.parse(state);
      console.log('loaded ', parsedState.locations.length);
    }
    
  })*/


export const {Provider, Context} = createDataContext(locationReducer, {startRecording, stopRecording, addLocations/*, addAproxLocation*/, setName, reset, setState}, {name: "", recording: false, locations : [], currentLocation: null})