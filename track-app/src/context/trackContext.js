import createDataContext from './createDataContext'


import  {requestPermissionsAsync, watchPositionAsync, Accuracy} from 'expo-location'


import trackerApi from '../api/tracker'

const trackReducer = (state, action) => {
    switch(action.type){
        case 'fetch_tracks':
            return {...state, tracks: action.tracks}
        case 'create_done':
            //console.log('create track', 'locations:' + action.locations, 'name:' + action.name);
            return {...state, isSaving: false, error:null};

        case 'create_started':
            return {...state, isSaving: true, error:null};

        case 'create_error':
            return {...state, isSaving: false, error: action.error};

        default:
            return state;
    }
}







const createTrack = dispatch => async ( locations, name)=>{
    try{

        dispatch ({
            type: 'create_started'
    
         });

                      
        /*for (let i = 0 ; i < 13; i++){
            console.log('iteration', i);
            locations = [...locations, ...locations ];
        }*/
                    
        console.log('create track', locations.length);
        const response = await trackerApi.post('/tracks', {
            name,
            locations
          });
          console.log('create track res', response);
          //console.log(response);
          dispatch ({
            type: 'create_done'

         });

         // navigate('mainFlow', null);
        
    }catch(error){
        console.log('create track error', error);
        dispatch ({
            type: 'create_error',
            error: error
            

         });
        
    }
    /*dispatch ({
        type: 'create_track',
        locations:locations,
        name: name
    });*/
}


const fetchTracks = dispatch => async ()=>{

    try{
        const response = await trackerApi.get('/tracks');
          //console.log(response);
          dispatch ({
            type: 'fetch_tracks',
            tracks:response.data
         });
         // navigate('mainFlow', null);
        
    }catch(error){
        console.log(error);
        
    }
    
}




export const {Provider, Context} = createDataContext(trackReducer, { createTrack, fetchTracks}, {isSaving:false})