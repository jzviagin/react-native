import axios from 'axios'
import {AsyncStorage} from 'react-native'



const instance = axios.create({
    baseURL: 'http://ec2-3-17-34-61.us-east-2.compute.amazonaws.com:3004/'
});

instance.interceptors.request.use(
    async (config)=>{
        const token = await AsyncStorage.getItem('@trackApp:token');
        console.log('using token' , token)
        if (token){
            config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
    },
    (err)=>{
        return Promise.reject(err);
    }
)
export default instance;