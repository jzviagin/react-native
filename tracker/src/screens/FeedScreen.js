import React, {useEffect, useContext, useState, useCallback} from 'react';
import { StyleSheet,View, ActivityIndicator, FlatList, TouchableOpacity, Text} from 'react-native';
import {SafeAreaView, withNavigationFocus , NavigationEvents} from 'react-navigation'



import { FontAwesome } from '@expo/vector-icons'; 

import trackerApi from '../api/tracker'

import {Context as MainContext}  from '../context/mainContext'

import TrackDetailFeed from '../components/TrackDetailFeed'

import {Button} from 'react-native-elements'

import  Spacer from '../components/Spacer'

import UserList from '../components/UsersList'


import FeedComments from '../components/FeedComments'










const icon = <FontAwesome name="feed" size={24} color="black" />


const Loader = (props)=>{
    const {fetchFeedPage, getFeed} = useContext(MainContext);
    console.log('rendering loader')
    useEffect( () =>{
        console.log('rendering loader 1st time')
        fetchFeedPage();
        
        

        /*try{
            const response = await trackerApi.post('/getUser', {userId: "5f049ec75d8eca07ec4c1a80"});
              console.log(response.data);
             
            
        }catch(error){
            console.log(error);
            
        }*/
    },[])

    return <ActivityIndicator/>
}



const FeedScreen = (props) => {

  

    const {getFeed, clearFeed} = useContext(MainContext);

    const feed = getFeed();

    let trackIds = [];

    const keys = Object.keys(feed);
    for (let i = 0 ; i < keys.length; i++){
        trackIds = [...trackIds, ...feed[i]];

    }
    console.log('feed page pages', keys.length)
    if(keys.length == 0){
        trackIds.push(0);
    }
    if(keys.length != 0 && feed[ keys.length - 1].length != 0){
        //console.log('feed last page', feed[ keys.length - 1])
        trackIds.push(0);
    }

    for (let i = 0 ; i < trackIds.length ; i++){
        console.log(trackIds[i])
    }

    //const [isFocused, setIsFocused] = useState(false);

   

    //const [offset, setOffset] = useState(0);

    const dataRef = React.useRef({
        isFocused: false,
        offset: 0
    })


    useEffect(() => {

        props.setTabPressHandler(()=>{
            console.log('is focused', dataRef.current.isFocused)
            if(dataRef.current.isFocused){
                if (dataRef.current.offset == 0){
                    clearFeed();
                }else{
                    //setOffset(0)
                    dataRef.current.offset = 0;
                    flatListRef.current.scrollToOffset({ animated: true, offset: 0 });

                }
            }
            
        })
        
    
        return ()=>{};
      }, []);

      const onViewRef = React.useRef((data)=> {
       
    })

    const flatListRef = React.useRef()

    


    return (
        <SafeAreaView   style = {styles.main} forceInset= {{top: 'always'}}>
            <NavigationEvents 
                onWillFocus = {
                   ()=>{
                    //setIsFocused(true)
                    dataRef.current.isFocused = true;
                   }
                }

                onWillBlur = {
                    ()=>{
                    //setIsFocused(false)
                    dataRef.current.isFocused = false
                    }
                }
       
           />

            <FlatList
                keyboardShouldPersistTaps ="handled"

                onScroll = { 
                    (event)=>{
                        //console.log(event.nativeEvent.contentOffset)
                        //setOffset(event.nativeEvent.contentOffset.y)
                        dataRef.current.offset = event.nativeEvent.contentOffset.y;
                    }
                    
                }
                refreshing = {false}
            
                onRefresh = {()=>{
                    console.log('refreshing');
                    clearFeed()
                }}
                ref = {flatListRef} 
                data = {trackIds}
                initialNumToRender = {1}
                keyExtractor = {item => { return ((item === 0 ? trackIds.length:item)+ '')}}
                renderItem = {
                    (item) => {
                        console.log('rendering item ', item.item)
                        
                        if (item.item === 0){

                            return <Loader/>
                        }
                        
                        return (
                                <View style = {{flexDirection: "column", borderBottomColor:"rgb(220, 220, 220)", borderBottomWidth: 3}}>
                                    <Spacer>
                                        <TrackDetailFeed trackId = {item.item}/>
                                    </Spacer>
                                </View>
                               
                        )
                    }
                }
                
                onViewableItemsChanged= { onViewRef.current} >


            
            </FlatList>
        </SafeAreaView>
    )
};



const styles = StyleSheet.create({
  
    main:{
        display: 'flex',
        flex: 1,
        backgroundColor: 'white'
    }
});

FeedScreen.navigationOptions = {
    title: 'Feed gggggggg',
    tabBarIcon:icon,
    header: null,
    headerShown: false
}





export default withNavigationFocus(FeedScreen);
