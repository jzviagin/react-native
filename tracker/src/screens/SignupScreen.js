import React, { useContext, useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import {NavigationEvents} from 'react-navigation'
import {Text} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import {Context as AuthContext} from '../context/authContext'
import NavLink from '../components/NavLink'

import AuthForm from '../components/AuthForm'
import ProgressBar from '../components/ProgressBar'

import {SafeAreaView} from 'react-navigation'





const SignupScreen = ({navigation}) => {



    const {state, signup, clearError} = useContext(AuthContext);
    //console.log('token: ', state.token)
    //console.log('errorMessage: ', state.errorMessage);

    const [email , setEmail] = useState('');
    const [password, setPassword]= useState('') ;
    const [username, setUsername]= useState('') ;
    const [firstName, setFirstName]= useState('') ;
    const [lastName, setLastName]= useState('') ;

    if (state.inProgress){
        return <ProgressBar/>
    }



    return (
        <SafeAreaView style = {styles.container} forceInset= {{top: 'always'}}>

                            <NavigationEvents onWillFocus = { ()=>{
                                clearError();
                            }}/>
                            <AuthForm style = {{backgroundColor:"red"}} isSignup = {true} email = {email}  password = {password}  username = {username}  firstName = {firstName} lastName = {lastName} title = "Sign up for tracker" buttonTitle = "Sign Up" buttonAction = {
                                (email, password, username, firstName, lastName) => {
                                    console.log('sign up action', email, password, username, firstName, lastName)
                                    setPassword(password);
                                    setEmail(email);
                                    setUsername(username);
                                    setFirstName(firstName);
                                    setLastName(lastName);
                                    signup({email: email,
                                        password: password,
                                        username: username,
                                        firstName: firstName,
                                        lastName: lastName
                                    });
                                }
                            } errorMessage  = {state.errorMessage}/>
                        
        </SafeAreaView>
        
    )

    
};


SignupScreen.navigationOptions =  {
    title:'Home',
    header: null,
    headerShown: false
}


const styles = StyleSheet.create({
    container: {
        display: "flex",
        justifyContent: "center",
        flexDirection: 'column',
        flex: 1,
        
        marginBottom: 0,
        paddingTop:0,
        backgroundColor:"white"
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    }
});

export default SignupScreen;