import React, {useEffect, useContext} from 'react';
import {View, StyleSheet, Text, Button, FlatList, TouchableOpacity} from 'react-native';
import {ListItem} from 'react-native-elements'
import {Context as MainContext}  from '../context/mainContext'
import {Context as AuthContext}  from '../context/authContext'
import {NavigationEvents} from 'react-navigation'
import useTrackTools from '../hooks/useTrackTools'
import { FontAwesome } from '@expo/vector-icons'; 


const TrackListScreen = (props) => {
    const {getUserTracks, getTrack} = useContext(MainContext);
    const {state: authState} = useContext(AuthContext);
   /* useEffect  (()=> {
        fetchTracks();
    },[])*/
    const [calcTrackDistance, getTrackStartStartTime] = useTrackTools();
//<ListItem chevron = {true} title= {item.item.name + ' Distance: ' + calcTrackDistance(item.item)}/>
    //console.log(state);

    //console.log(state.tracks);

    const trackIds = getUserTracks(authState.user._id);
    const tracks = [];
    for (let i = 0 ; i < trackIds.length; i++){
        const track = getTrack(trackIds[i]);
        tracks.push(track);
    }
    return (
        <>
        <NavigationEvents onWillFocus = {() => { 
             //fetchTracks();
        }} />

        
        
        <FlatList data = {tracks}
            keyExtractor = {item => item._id}
            renderItem = {
                (item) => {
                    //console.log('item', item)
                    
                    return (<TouchableOpacity style= {styles.itemContainer} onPress = { ()=> {props.navigation.navigate('TrackDetails', {id: item.item._id})}}>
                        <View style= {styles.itemContainerData}>
                            <Text style= {styles.itemTitle} >{item.item.description}</Text>
                            <Text style= {styles.itemTime}>{getTrackStartStartTime(item.item)}</Text>
                        </View>
                        <View style= {styles.chevron}>
                            <FontAwesome  name="chevron-circle-right" size={34} color="black" />
                        </View>
                       

                    </TouchableOpacity>
                    )
                }
            }>
        
        </FlatList>


        
        </>
        )
};

TrackListScreen.navigationOptions = {
    title: 'Tracks'
}



const styles = StyleSheet.create({
    itemContainerData: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: "flex-start",
        margin:1,
        backgroundColor: 'white'
    },
    itemContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: "center",
        margin:1,
        backgroundColor: 'white'
    },
    itemTitle: {
        padding: 10,
        fontSize:22,
        fontWeight:'bold'
    },
    itemTime: {
        padding: 10
    },
    chevron:{
        display: "flex",
        flex: 1,
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginEnd: 15
        
    }
});

TrackListScreen.navigationOptions = {
    headerShown: false,
    header: null
}


export default TrackListScreen;