import React, { useContext, useEffect, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, KeyboardAvoidingView, ActivityIndicator, ScrollView, TextInput} from 'react-native';
import {NavigationEvents} from 'react-navigation'
import  Spacer from '../components/Spacer'
import {Context as AuthContext} from '../context/authContext'
import {Context as MainContext} from '../context/mainContext'

import ProgressBar from '../components/ProgressBar'

import {SafeAreaView} from 'react-navigation'

import {Text, Input, Button, Image} from 'react-native-elements'
import Icon from 'react-native-vector-icons/Feather';
import { FontAwesome } from '@expo/vector-icons';

import ImagePicker from 'react-native-image-picker';

import AuthImage from '../components/AuthImage'

import withBackNavigation from '../hooks/withBackNavigation'


 
// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
  title: 'Select profile photo',
  customButtons: [],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
 
const chooseImage = ()=>{
    console.log('choose image');
    ImagePicker.showImagePicker(options, (response) => {
        //console.log('Response = ', response);
       
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        /*  if (true){
            ImagePicker.launchCamera(options, (response) => {
                // Same code as in above section!
            });
            }else{
          // Open Image Library:
            ImagePicker.launchImageLibrary(options, (response) => {
            // Same code as in above section!
          });
            }*/
           

        } else {
          const source = { uri: response.uri };

          console.log('source', source);

        }
      });
}






const ProfileEditScreen = ({navigation}) => {




    const {state, update, clearError} = useContext(AuthContext)
    const {fetchUser} = useContext(MainContext)

    const [firstName, setFirstName]= useState(state.user.firstName) ;
    const [lastName, setLastName]= useState(state.user.lastName) ;

  

    let firstNameInput;
    let lastNameInput;


    const checkInputs = ()=>{

        if ( firstName.trim().length === 0 || lastName.trim().length === 0){
            return false;
        }
        
        return true;
    }


    const [imageUri, setImageUri] = useState(null);

    if (state.inProgress){
        return <ProgressBar/>
    }

    
    const chooseImage = ()=>{
        console.log('choose image');
        ImagePicker.showImagePicker(options, (response) => {
            //console.log('Response = ', response);
        
            if (response.didCancel) {
            console.log('User cancelled image picker');
            } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            /*  if (true){
                ImagePicker.launchCamera(options, (response) => {
                    // Same code as in above section!
                });
                }else{
            // Open Image Library:
                ImagePicker.launchImageLibrary(options, (response) => {
                // Same code as in above section!
            });
                }*/
                /** */
            

            } else {
            const source = { uri: response.uri };

            setImageUri(response.uri)

            console.log('source', source);

            }
        });
    }

/*


            <ScrollView style= {{display: "flex", width: "100%"}
                }

                contentContainerStyle = {{ flexGrow: 1, justifyContent: "center", alignItems:"center"}}
                    >


                    */





    return (
        <SafeAreaView style = {styles.container} forceInset= {{top: 'always'}}>

                    <NavigationEvents onWillFocus = { ()=>{
                            clearError()
                    }}/>


            <KeyboardAvoidingView 
                                        
                                        behavior={Platform.OS == "ios" ? "padding" : "position"}
                                        contentContainerStyle = {{flex:1, flexGrow: 1, width:'100%', alignItems:"center"}}
                                        style={{  flex: 1, flexGrow: 1, alignItems:"center", width: '100%'}}
                                        >
                <TouchableOpacity style={styles.imageContainer}  onPress = {()=>{chooseImage()}}>
                        {(state.user.photo || imageUri )?<AuthImage
                           
                            style={styles.image}
                        
                            source={{ uri: imageUri?imageUri:state.user.photo 
                                } }
                            PlaceholderContent={<ActivityIndicator />}
                            />:
                            <View style = {{display: 'flex', alignItems:'center', justifyContent:'center', width: '100%' , height:'100%'  }}> 
                                <FontAwesome name="user"    size = {250} color="grey" />
                            </View>
                            
                        }
                </TouchableOpacity>

                <View style= {{ width: "100%"}}>
                

                
                    <Spacer>

                        <View style= {{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "flex-start",
                            //backgroundColor:"purple"
                            backgroundColor:'rgb(240,240,240)',
                            padding: 10,
                            borderRadius: 20,
                            overflow: 'hidden'                }} >


                            <TextInput 


                            

                                ref = {(input)=> {firstNameInput= input}}
                                value = {firstName}

                                rightIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                                style = {{ height: "100%", flex:1,  marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}
                                placeholder="First Name"
                                onSubmitEditing={() => { lastNameInput.focus(); }}

                                autoCapitalize = "none"
                                autoCorrect = {false}
                                returnKeyType="next"
                                multiline = {false}
                                onChangeText = { (text)=> { setFirstName(text)}}
                            
                            ></TextInput>
                        </View>
                    </Spacer>

                    <Spacer>

                        <View style= {{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "flex-start",
                            //backgroundColor:"purple"
                            backgroundColor:'rgb(240,240,240)',
                            padding: 10,
                            borderRadius: 20,
                            overflow: 'hidden'                }} >


                            <TextInput 


                            

                                ref = {(input)=> { lastNameInput= input}}
                                value = {lastName}

                                rightIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                                style = {{ height: "100%", flex:1,  marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}
                                placeholder="First Name"
                                onSubmitEditing={()=> {
                                    console.log('submit')
                                    if(checkInputs() == false){
                                        return;
                                    }
                                    update({
                                        firstName,
                                        lastName,
                                        imageUri
                                    },
                                    ()=>{
                                        fetchUser(state.user._id) 
                                    })
                                }}

                                autoCapitalize = "none"
                                autoCorrect = {false}
                                returnKeyType="done"
                                multiline = {false}
                                onChangeText = { (text)=> { setLastName(text)}}
                            
                            ></TextInput>
                        </View>
                </Spacer>
                   
              
                
                {state.errorMessage ? ( <Spacer><Text style = {styles.errorMessage}>{state.errorMessage}</Text></Spacer> ): null}
                <Spacer></Spacer>
               
                <View style={{flexDirection:'row', justifyContent: 'center'}}>
                <TouchableOpacity 
                            
                            style= {{
                                width: 50,
                                alignItems: "center",
                                justifyContent: "center",
                                borderRadius: 20,
                                borderColor: "black",
                                borderWidth: 1,
                                padding:10,
                                margin:5,
                            }} onPress = {()=> {
                                console.log('clicked save', imageUri);
                                update({
                                    firstName,
                                    lastName,
                                    imageUri
                                },
                                ()=>{
                                    fetchUser(state.user._id) 
                                 }
                                )
                            }}>
                            <Icon
                                name='save'
                                size={30}
                                color="black"
                                
                                />
                        </TouchableOpacity>
                </View>
                
                
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
        
    )

    
};





const styles = StyleSheet.create({
    container: {
        display: "flex",
        justifyContent: "center",
        flexDirection: 'column',
        flex: 1,
        
        marginBottom: 0,
        paddingTop:0,
        alignItems: "center",
        backgroundColor:"white",
        width:"100%"
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    },
    imageContainer: {
        marginTop: 20,
        marginBottom: 20,
        borderRadius:150,
        width: 300, height:300 ,
  
        borderWidth: 2,
        borderColor: "grey",
        overflow: 'hidden',
        
    },
    image: {
        width: '100%' , height:'100%' 
     
    }
});

ProfileEditScreen.navigationOptions = {
    headerShown: false,
    header: null
}


export default withBackNavigation(ProfileEditScreen);