import React, {useEffect, useContext, useState, useCallback, useRef} from 'react';
import { StyleSheet,View,  FlatList, TouchableOpacity, TextInput} from 'react-native';
import {SafeAreaView, withNavigationFocus,NavigationEvents} from 'react-navigation'


import trackerApi from '../api/tracker'

import {Context as MainContext} from '../context/mainContext'
import {Context as AuthContext} from '../context/authContext'



import { FontAwesome } from '@expo/vector-icons'; 
import {Text, Input, Button} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import Icon from 'react-native-vector-icons/Feather';

import UserDetailsSmall from '../components/UserDetailsSmall'

import SearchUserItem from '../components/SearchUserItem'









const icon = <FontAwesome name="search" size={24} color="black" />

const SearchScreen = (props) => {

    const {follow, unfollow, getFollowers, searchUsers, getUsers, getUser} = useContext(MainContext);


    const {state: authState, signup} = useContext(AuthContext);

    const userIds = getUsers();
    let textInput = useRef(null);

    const timer = useRef({timer: null});




   
    console.log('search screen ', userIds)



    return (
        <SafeAreaView   style = {styles.main} forceInset= {{top: 'always'}}>
            <NavigationEvents 
                onDidFocus = {
                   ()=>{
                       console.log('searchfocused!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', textInput)
                    //setIsFocused(true)
                    //dataRef.current.isFocused = true;
                    setTimeout(()=>{
                        textInput.current.focus();
                    },300)

                    
                   
                    
                   }
                }

                onWillBlur = {
                    ()=>{
                    //setIsFocused(false)
                    //dataRef.current.isFocused = false
                    }
                }
       
           />

            <Spacer>

                <View style= {{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "flex-start",
                    //backgroundColor:"purple"
                    backgroundColor:'rgb(240,240,240)',
                    padding: 10,
                    borderRadius: 20,
                    overflow: 'hidden'                }} >

            
                    <TextInput 

                        ref = { textInput}
                    


                        rightIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                        style = {{ height: "100%", flex:1, marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}
                
                        onChangeText = {  (term)=> { 

                            clearInterval(timer.current.timer);
                            timer.current.timer = setTimeout(()=> {
                                searchUsers(term);
                            }, 300);
                           
                            
                            
                        }}
                        placeholder="Type a username"
        
                        autoCapitalize = "none"
                        autoCorrect = {false}
                        returnKeyType="done"
                        multiline = {false}
                    
                    ></TextInput>
                </View>

               
            </Spacer>


            <Spacer>
                <FlatList data = {userIds}
                    keyboardShouldPersistTaps ="handled"
                    keyExtractor = {item => item._id}
                    renderItem = {
                        (item) => {
                            return (<SearchUserItem userId = {item.item}></SearchUserItem>)
                           
                           
                        }
                    }>
                
                </FlatList>
            </Spacer>

        </SafeAreaView>
    )
};



const styles = StyleSheet.create({
  
    main:{
        display: 'flex',
        flex: 1,
        backgroundColor: "white"
    },
    itemContainerData: {
        display: 'flex',
        flex: 1,
        flexDirection : 'row-reverse'
    },
    userText:{
        display: 'flex',
        flex: 1,
        flexDirection : 'row-reverse',
        fontSize:24
    }
});

SearchScreen.navigationOptions = {
    title: 'Search',
    tabBarIcon:icon,
    headerShown: false,
    header: null
}



export default withNavigationFocus(SearchScreen);
