import React, {useEffect, useContext, useState, useRef} from 'react';
import { StyleSheet,View, KeyboardAvoidingView, Keyboard} from 'react-native';
import {Text} from 'react-native-elements';
import Map from '../components/Map';
import Map2 from '../components/Map2';
import {SafeAreaView, withNavigationFocus} from 'react-navigation'
import TrackDetails from  '../components/TrackDetails'



import { FontAwesome } from '@expo/vector-icons'; 

//import  {requestPermissionsAsync, watchPositionAsync, Accuracy} from 'expo-location'

//import useLocation from '../hooks/useLocation'

import TrackForm from '../components/TrackForm';

import {connect} from 'react-redux'

import {Context as BgLocationContext} from '../context/bgLocationContext'
import {Context as MainContext} from '../context/mainContext'
import {Context as AuthContext} from '../context/authContext'
import ProgressBar from '../components/ProgressBar'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';







const icon = <FontAwesome name="plus" size={24} color="black" />

const TrackCreateScreen = (props) => {
    const {state : locationState, startRecording, stopRecording, addLocations/*, addAproxLocation*/} = useContext(BgLocationContext);


    const {getUserTracks, getTrack, saveTrack} = useContext(MainContext);
    const {state: authState} = useContext(AuthContext);

    const scrollViewRef = useRef(null);

    /**
     * 
     * 
     *   <ScrollView  
                        
                        ref = {scrollViewRef}
                onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
                style= {{}}
                contentContainerStyle = {{ flexGrow: 1, justifyContent: "center" }}>


                </ScrollView>
     * 
     * 
     */

    /*const [keyboardVisible, setKeyboardVisible] = useState(false);

    
    useEffect( ()=>{
        const keyboardDidShowListener = Keyboard.addListener(
            "keyboardDidShow",
            ()=>{
                setKeyboardVisible(true);
            }
          );
          const keyboardDidHideListener = Keyboard.addListener(
            "keyboardDidHide",
            ()=>{
                setKeyboardVisible(false);
            }
          );
    },[])*/
    
    return (
        <SafeAreaView   style = {styles.main} forceInset= {{top: 'always'}}>
                                      <KeyboardAvoidingView 
                            
                            behavior={Platform.OS == "ios" ? "padding" : "position"}
                            contentContainerStyle = {{flex:1, flexGrow: 1}}
                            style={{  flex: 1, flexGrow: 1}}
                            >
               
                      
                          <Map></Map>
                         

                                <TrackForm />
                        

           
                </KeyboardAvoidingView>
                

              
            
        
        </SafeAreaView>
    )
};



const styles = StyleSheet.create({
    spinnerText:{
        fontSize:40
    },
    spinnerTextContainer:{
        display: "flex",
        flex: 1,
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    main:{
        display: 'flex',
        flex: 1,
        flexGrow:1
    }
});

TrackCreateScreen.navigationOptions = {
    title: 'Add track',
    tabBarIcon:icon
}



export default withNavigationFocus(TrackCreateScreen);
