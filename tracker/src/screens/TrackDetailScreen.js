import React, {useContext, useEffect} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Context as mainContext} from '../context/mainContext'
import MapView, {Polyline, Circle, Marker} from 'react-native-maps'
import useTrackTools from '../hooks/useTrackTools'

import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';

import trackerApi from '../api/tracker'


const TrackDetailScreen = (props) => {

    const {getTrack} = useContext(mainContext);

    //console.log('track screen state: ', state)

    const id = props.navigation.getParam("id");
    //const track = state.tracks.find( (elem) => {return elem._id === id});
    const track = getTrack(id);

    //console.log('track screen track: ', track);
    //console.log('track screen props: ', props);
    //('track screen props: ', id);



    const points = [];
    //let dups = [];
    for (let i = 0 ; i < track.locations.length ; i++){
        //console.log(new Date(track.locations[i].timestamp));
        //console.log(track.locations[i].insertTime);

        //console.log(track.locations[i]);
        points.push({
            latitude: track.locations[i].coords.latitude,
            longitude: track.locations[i].coords.longitude
        })
        /*if( i < track.locations.length - 1){
            dupsItem = [];
            for (let j = i + 1; j < track.locations.length; j++){
                if (track.locations[i].coords.latitude === track.locations[j].coords.latitude &&
                    track.locations[i].coords.longitude === track.locations[j].coords.longitude)
                    {
                        if (dupsItem.length === 0){
                            dupsItem.push( {
                                index: i,
                                item: track.locations[i]
                            })
                        }
                        dupsItem.push( {
                            index: j,
                            item: track.locations[j]
                        })
                    }
            }
            if (dupsItem.length != 0){
                dups.push(dupsItem)
                console.log(dupsItem)
            }
        }*/
        
    }
    //console.log (dups);
    const [calcTrackDistance, getTrackStartStartTime, getTrackDuration, getPace] = useTrackTools();

    
    //return <Text style = {{fontSize:48}}>TrackDetailScreen</Text>
return <>

     
        <  Text style = {{fontSize:48}}>{track.description}</Text>



        <MapView style = {styles.map}
        initialRegion = {{
            latitude:track.locations[0].coords.latitude,
            longitude: track.locations[0].coords.longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
        }}
        >

            <Polyline coordinates = {points}/>
            <Circle
                center = {track.locations[0].coords}
                radius = {30}
                strokeColor = "rgba(158,158,255, 1.0)"
                fillColor = "rgba(158,158,255,0.3)"
            />
           {/*points.map ((point,index) => {
                           return <Marker
                           coordinate = {point}
                           radius = {1}
                           strokeColor = "rgba(158,158,255, 1.0)"
                           fillColor = "rgba(158,158,255,0.3)"
           ><Text>{index}</Text></Marker>
           })*/} 
           
        </MapView>
        <View style = {styles.rows}>
            <View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Start Time: </Text>
                    <Text style = {styles.dataText}>{getTrackStartStartTime(track)}</Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Duration: </Text>
                    <Text style = {styles.dataText}>{getTrackDuration(track)} </Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Distance: </Text>
                    <Text style = {styles.dataText}>{(calcTrackDistance(track)/ 1000).toFixed(2) +' KM'} </Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Average Pace: </Text>
                    <Text style = {styles.dataText}>{getPace(track).toFixed(2) + ' Min/KM'} </Text>
                </View>
            </View>
        </View>
    </>
};



const styles = StyleSheet.create({
    map:{
        height: 300
    },
    rows: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flex:1
    },
    columns: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 22
    },
    dataText: {
        fontSize: 20
    }
});

TrackDetailScreen.navigationOptions = {
    headerShown: false,
    header: null
}


export default TrackDetailScreen;