import React, { useContext} from 'react';
import {View, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import {NavigationEvents} from 'react-navigation'
import { Button} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import {SafeAreaView} from 'react-navigation'
import {Context as AuthContext} from '../context/authContext'
import NavLink from '../components/NavLink'

import AuthForm from '../components/AuthForm'
import UserDetails from '../components/UserDetails'

import { FontAwesome } from '@expo/vector-icons'; 
import Icon from 'react-native-vector-icons/FontAwesome';
const icon = <FontAwesome name="gear" size={24} color="black" />



const AccountScreen = ({navigation}) => {



    const { state,signout} = useContext(AuthContext);

    const showSignOutDialog = () =>
        Alert.alert(
        "Are you sure?",
        "Are you sure you want to sign out",
        [
            {
            text: "CANCEL",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
            },
            { text: "OK", onPress: () => {
                signout();
            } }
        ],
        { cancelable: false }
    );


    return (



        <SafeAreaView style={{backgroundColor:"white", flex:1}} forceInset= {{top: 'always'}}>
            <View style= {{marginTop:20}}>
                <UserDetails  userId = {state.user._id}/>
            </View>
            <View style= {{flexDirection:'row' ,justifyContent:'center', }} >
                <TouchableOpacity 
                            
                            style= {{
                                width: 50,
                                alignItems: "center",
                                justifyContent: "center",
                                borderRadius: 20,
                                borderColor: "black",
                                borderWidth: 1,
                                padding:10,
                                margin:5,
                            }} onPress = {()=> {
                                navigation.navigate('EditProfile');
                            }}>
                            <Icon
                                name='edit'
                                size={30}
                                color="black"
                                
                                />
                        </TouchableOpacity>

                        <TouchableOpacity 
                            
                            style= {{
                                width: 50,
                                alignItems: "center",
                                justifyContent: "center",
                                borderRadius: 20,
                                borderColor: "black",
                                borderWidth: 1,
                                padding:10,
                                margin:5,
                            }} onPress = {()=> {
                                showSignOutDialog();
                            }}>
                            <Icon
                                name='sign-out'
                                size={30}
                                color="black"
                                
                                />
                        </TouchableOpacity>
            </View>
    

        </SafeAreaView>
        
    )

    
};


AccountScreen.navigationOptions =  {
    title:'Account',
    tabBarIcon: icon,
    header: null,
    headerShown: false
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        marginBottom: 250
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    }
});

export default AccountScreen;