import createDataContext from './createDataContext'
import {AsyncStorage} from 'react-native'
import {navigate, back} from '../navigationRef'


import trackerApi from '../api/tracker'

const authReducer = (state, action) => {
    switch(action.type){
        case 'test':
            console.log ('test dispatched')
            return state;
        case 'login':
            return {...state, token: action.token,  user:action.user, errorMessage: '',  inProgress: false};

        case 'update':
            return {...state, user:action.user, errorMessage: '',  inProgress: false};
    


        case 'set_error_message':
            return {...state, errorMessage: action.payload,  inProgress: false};

        case 'clear_error':
            return {...state, errorMessage: ''};
        case 'action_started':
            return {...state, inProgress: true};

        default:
            return state;
    }
}
const clearError = (dispatch, state) => ()=>{
    dispatch({
        type: 'clear_error'    
     });
}

const tryLocalSignIn = (dispatch, state) => async ()=>{
    try{
        console.log("try");
        const token = await AsyncStorage.getItem('@trackApp:token');
        console.log("token", token);
        if (token){
            const response = await trackerApi.get('/login');
            console.log(response.data);
            //await AsyncStorage.setItem('token', response.data);
            dispatch({
                type: 'login',
                token: response.data.token,
                user: response.data.user
            });
            navigate('mainFlow', null);
            return;
        }
        
    }catch(error){
        console.log("error", error);
      
    }
    navigate('loginFlow', null);
        
    
}


const signup = (dispatch, state) => async (data) => {
        //make api request 
        //if we sign up, modify the state

        //show message on failure

        console.log(data);

        dispatch({
            type: 'action_started'
        });

        try{

            console.log("signup1");
            const response = await trackerApi.post('/signup', {
                email: data.email,
                password: data.password,
                username: data.username,
                firstName: data.firstName,
                lastName: data.lastName
              });
              console.log("signup2");
              console.log(response);

              await AsyncStorage.setItem('@trackApp:token', response.data.token);
              const token = await AsyncStorage.getItem('@trackApp:token');
              console.log("token2", token);
              dispatch({
                type: 'login',
                token: response.data.token,
                user: response.data.user
            });
              navigate('mainFlow', null);
            
        }catch(error){
            console.log("signup error", error.message);
            console.log(error);
            dispatch({
                type: 'set_error_message',
                payload: error.response.data
            })
        }
            
      

    
}


const update = (dispatch, state) => async (data,callback) => {
    //make api request 
    //if we sign up, modify the state

    //show message on failure

    console.log(data);

    dispatch({
        type: 'action_started'
    });

    try{


        const formData = new FormData();

        console.log('image uri', data.imageUri, data);
        if (data.imageUri){
            console.log('appending image')
            formData.append('image', {
            uri: Platform.OS === 'android' ? data.imageUri : image.uri.replace('file://', ''),
            name: `image`,
            type: 'image/jpeg', // it may be necessary in Android. 
          });
        }

        formData.append('firstName', data.firstName);
        formData.append('lastName', data.lastName);
       
        const headers = {
          'Content-Type': 'multipart/form-data'
        }
        //client.post('/items/save', formData, headers);
        /*{
            firstName: data.firstName,
            lastName: data.lastName
          }*/ 
        const response = await trackerApi.post('/updateUser', formData , headers);
          console.log(response);


          dispatch({
            type: 'update',
            user: response.data.user
        });
        callback();
        back();
        
    }catch(error){
        console.log("signup error", error.message);
        console.log(error);
        dispatch({
            type: 'set_error_message',
            payload: error.response.data
        })
    }
        
  


}

const signin = (dispatch, state) =>  async (data) => {


           //make api request 
        //if we sign up, modify the state

        //show message on failure

        dispatch({
            type: 'action_started'
        });

        console.log(data);

        try{
            console.log('logging in');
            const response = await trackerApi.post('/login', {
                email: data.email,
                password: data.password
              });
              console.log('res!!!!!!!', response.data);
              await AsyncStorage.setItem('@trackApp:token', response.data.token);
              dispatch({
                type: 'login',
                token: response.data.token,
                user: response.data.user
                });
              navigate('mainFlow', null);
            
        }catch(error){
            console.log(error.response.data);
            dispatch({
                type: 'set_error_message',
                payload: error.response.data
            })
        }

    }



const signout = (dispatch, state) => async  (data) => {
    try{
        console.log("try");
        await AsyncStorage.setItem('@trackApp:token', '');
        console.log("success");
        navigate('loginFlow', null);
        
    }catch(error){
        console.log("error", error);
      
    }

}





export const {Provider, Context} = createDataContext(authReducer, {signin, signout, update,  signup, clearError, tryLocalSignIn}, {errorMessage: '',  inProgress: false})