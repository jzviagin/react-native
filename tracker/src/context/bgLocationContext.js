import React, {useReducer} from 'react';
import useBgLocation from '../hooks/useBgLocation'


export const Context = React.createContext();
export const Provider = (props)=>{

    
    const [err, locationState, startRecording, stopRecording, setDescription, reset, getCurrentLocation] = useBgLocation()
    console.log('location context refreshing locations', locationState.locations.length)
    return (
    <Context.Provider value = {{state: locationState, startRecording, stopRecording, setDescription, reset, getCurrentLocation}}>{props.children}</Context.Provider>
    )
}


