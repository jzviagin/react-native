import createDataContext from './createDataContext'


//import  {requestPermissionsAsync, watchPositionAsync, Accuracy} from 'expo-location'


import trackerApi from '../api/tracker'
import { ActivityIndicator } from 'react-native';




const mainReducer = (state, action) => {
    switch(action.type){

        case 'clear_search_users':
            return {...state, search: {...state.search, users:[]}}

        case 'set_search_term':
            return {...state, searchTerm: action.value}


        case 'set_data':

            if (action.data.feed){
                const feedKeys = Object.keys(action.data.feed);
                const stateFeedKeys = Object.keys(state.feed);
                for (let i = 0 ; i < feedKeys.length; i++){
                    const key = feedKeys[i];
                    if (key > stateFeedKeys.length){
                        console.log('feed update ignored')
                        return state;
                    }
                }
            }

            if (action.data.userTracks){
                const userTracksKeys = Object.keys(action.data.userTracks);
                const stateuserTracksKeys = Object.keys(state.userTracks);
                for (let i = 0 ; i < userTracksKeys.length; i++){
                    const key = userTracksKeys[i];
                    if (key > stateuserTracksKeys.length){
                        console.log('feed update ignored')
                        return state;
                    }
                }
            }
            if (action.data.search && state.searchTerm.trim().length == 0){
                return state;
            }
            //console.log('!!!!!!!!!!received data', JSON.stringify(action.data));
            const newState = {...state};
            const keys = Object.keys(action.data);
            for (let i = 0 ; i < keys.length; i++){
                const entityName = keys[i]
                //console.log('entity name', entityName)

                const entity = {...state[entityName]};
                const ids = Object.keys(action.data[entityName]);
                for (let j = 0 ; j < ids.length; j++){
                    const id = ids[j];
                    entity[id] = action.data[entityName][id]
                }
                newState[entityName] = {...entity}
            }
            //console.log('new state', newState);
            return newState;
        


        case 'clear_feed':
            return {...state, feed: {}}
        case 'clear_user_tracks':
            return {...state, userTracks: {}}
        default:
            return state;
    }
}

const inProgressApiCalls = {}

const apiCall = async (api, params, dispatch) => {
    const callKey = api + '_' + JSON.stringify(params);
    try{

       
        if (inProgressApiCalls[callKey]){
            return;
        }
        inProgressApiCalls[callKey] = true;
        const response = await trackerApi.post('/'+ api, params);
        inProgressApiCalls[callKey] = false;
        //console.log(response.data);
        dispatch ({
            type: 'set_data',
            data: response.data
        });
             
            
    }catch(error){
        inProgressApiCalls[callKey] = false;
        console.log(error);
            
    }
}




const getUser = (dispatch, state) =>  (userId)=>{
    if (state['users'][userId]){
        return state['users'][userId]
    }
    apiCall('getUser' ,{userId: userId}, dispatch);
    return null;
  

   
        
}

const fetchUser = (dispatch, state) =>  (userId)=>{

    apiCall('getUser' ,{userId: userId}, dispatch);
    return null;
  

   
        
}


const getTrack = (dispatch, state) =>  (trackId)=>{
    //console.log('getUser current state', state)
    if (state['tracks'][trackId]){
        return state['tracks'][trackId]
    }
    apiCall('getTrack' ,{trackId: trackId}, dispatch);
    return null;
  

   
        
}

const getComment = (dispatch, state) =>  (commentId)=>{
    //console.log('getUser current state', state)
    if (state['commentsData'][commentId]){
        return state['commentsData'][commentId]
    }
    apiCall('getComment' ,{commentId: commentId}, dispatch);
    return null;
  

   
        
}

const getFollowing = (dispatch, state) =>  (userId)=>{
    
    if (state['following'][userId]){
        return state['following'][userId]
    }
    apiCall('getFollowing' ,{userId: userId}, dispatch);
    return [];
  

   
        
}

/*const getUserTracks = (dispatch, state) =>  (userId)=>{
    
    if (state['userTracks'][userId]){
        return state['userTracks'][userId]
    }
    apiCall('getUserTracks' ,{userId: userId}, dispatch);
    return [];
  

   
        
}*/

const createTrack = (dispatch, state) => (locations, description)=>{
    apiCall('createTrack', {
        description,
        locations
      },dispatch );
}



const getLikes = (dispatch, state) =>  (trackId)=>{

    //console.log('getLikes', trackId);
    //console.log('likes collection', state['likes'])
    
    if (state['likes'][trackId]){
        return state['likes'][trackId]
    }
    console.log('get likes api',trackId)
    apiCall('getLikes' ,{trackId: trackId}, dispatch);
    return [];
  

   
        
}

const getComments = (dispatch, state) =>  (trackId)=>{
    
    if (state['comments'][trackId]){
        return state['comments'][trackId]
    }
    apiCall('getComments' ,{trackId: trackId}, dispatch);
    return [];
  

   
        
}



const like = (dispatch, state) =>  (trackId)=>{
    

    apiCall('like' ,{trackId: trackId}, dispatch);
  
  

   
        
}


const addComment = (dispatch, state) =>  (trackId, text)=>{
    

    apiCall('addComment' ,{trackId: trackId, text: text}, dispatch);
  
  

   
        
}


const unlike = (dispatch, state) =>  (trackId)=>{
    

    apiCall('unlike' ,{trackId: trackId}, dispatch);
  
  

   
        
}


const follow = (dispatch, state) =>  (userId)=>{
    

    apiCall('follow' ,{userId: userId}, dispatch);
  
  

   
        
}

const unfollow = (dispatch, state) =>  (userId)=>{
    

    apiCall('unfollow' ,{userId: userId}, dispatch);
  
  

   
        
}

const getFollowers = (dispatch, state) =>  (userId)=>{

    console.log('getFollofers', state['followers']);
    
    if (state['followers'][userId]){
        return state['followers'][userId]
    }
    apiCall('getFollowers' ,{userId: userId}, dispatch);
    return [];
  

   
        
}


const getUsers = (dispatch, state) =>  ()=>{

    
    if (state['search']['users']){
        return state['search']['users']
    }
    return [];
  

   
        
}




const searchUsers = (dispatch, state) =>  (username)=>{


    dispatch ({
        type: 'set_search_term',
        value: username
    });
    if (username.trim().length > 0){
        apiCall('findUsers' ,{username: username}, dispatch);
    }else{
        dispatch ({
            type: 'clear_search_users'
           
        });
    }
   
    return [];
  

   
        
}


const getFeed = (dispatch, state) =>  ()=>{
    return state['feed'];
}

const getUserTracks = (dispatch, state) =>  ()=>{
    return state['userTracks'];
}

const clearUserTracks = (dispatch, state) =>  ()=>{
    dispatch ({
        type: 'clear_user_tracks'
    });
}

const clearFeed = (dispatch, state) =>  ()=>{
    dispatch ({
        type: 'clear_feed'
    });
}

const fetchUserTracksPage = (dispatch, state) =>  (userId)=>{
    console.log('get feed page')
    const keys = Object.keys(state['userTracks']);
    console.log('feed keys ', keys)
    apiCall('userTracks' ,{page: keys.length, userId}, dispatch);
   


}

const fetchFeedPage = (dispatch, state) =>  ()=>{
    console.log('get feed page')
    const keys = Object.keys(state['feed']);
    console.log('feed keys ', keys)
    apiCall('feed' ,{page: keys.length}, dispatch);
   


}



















    




export const {Provider, Context} = createDataContext(mainReducer, {createTrack, getUserTracks, getFeed, clearFeed, clearUserTracks, fetchFeedPage, fetchUserTracksPage,  searchUsers, getUsers, getUser, fetchUser,  follow, unfollow, getFollowing, getFollowers, like, unlike, getLikes, getComments, getTrack, getComment, addComment}, { userTracks: {}, feed: {}, userTracks: {}, search: {}, searchTerm: '', users:{}, followers:{}, following: {}, tracks:{}, likes: {}, comments:{}, commentsData: {}})