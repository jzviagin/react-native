import createDataContext from './createDataContext'


//import  {requestPermissionsAsync, watchPositionAsync, Accuracy} from 'expo-location'

import {  AsyncStorage } from 'react-native';


import trackerApi from '../api/tracker'

import {getPreciseDistance} from 'geolib'


const addLocationsToState = (state,action)=> {
    let newLocations = [...state.locations];
    for (let i = 0; i < action.locations.length ; i++){
        //console.log('adding new locations: ', newLocations.length);
        if (newLocations.length != 0){
            const distance = getPreciseDistance({
                latitude: newLocations[newLocations.length -1 ].coords.latitude,
                longitude: newLocations[newLocations.length -1].coords.longitude
            }, 
            {
                latitude: action.locations[i].coords.latitude,
                longitude: action.locations[i].coords.longitude
            });
            //console.log('distance', distance);
            //console.log('accuracy', action.locations[i].coords.accuracy)
            if (true/*distance >10 &&  action.locations[i].coords.accuracy < 12*/){
                //console.log('meets creteria');
                newLocations.push(action.locations[i]); 
            }else{
                //console.log('not meets creteria');
            }
        }else{
            //console.log('1st location');
            newLocations.push(action.locations[i]);
        }
    }

   
    return {...state, locations: [...newLocations], currentLocation: action.locations[action.locations.length -1]}
}

const locationReducer = (state, action) => {
    switch(action.type){
        case 'start_recording':
            AsyncStorage.setItem('@trackApp:recording', 'true');
            return {...state, recording: true, locations:[]}
        case 'stop_recording':
            AsyncStorage.setItem('@trackApp:recording', 'false');
            return {...state, recording: false}

        case 'set_locations':
            state.locations = [];

            const newState = addLocationsToState(state, action);
            /*if (newState.locations.length != 0){
                newState.recording = true;
            }*/
            return newState;

        case 'add_locations':
                console.log('currect locations', state.locations.length);
                if (state.recording){

                    //let newLocations = [];
                    //for (let i = 0; i < 100 ; i++){
                    //    newLocations.push(action.location);
                   // }
                   /* if (state.locations.length === 0){
                        for (let i = 0 ; i < 10000; i++){
                            action.locations = [...action.locations, action.locations[action.locations.length -1] ];
                        }
                    }*/
                    return addLocationsToState(state, action);

                 

                }else{
                    return state;
                }

      /*  case 'add_aprox_location':
            if (state.recording){
                return {...state, currentLocation: action.location}

            }else{
                return state;
            }*/
        case 'set_description':
            console.log(action.description);
            return {...state, description: action.description}

        case 'set_state':
            return action.state;

        case 'reset':
            return {...state, locations: [], description: ''}
    

        default:
            return state;
    }
}





const startRecording = (dispatch, state) => async ()=>{
    dispatch ({
        type: 'start_recording'
    });


}




const stopRecording = (dispatch, state) => ()=>{
    dispatch ({
        type: 'stop_recording'
    })
}

const addLocations = (dispatch, state) => (locations)=>{
    console.log('dispatching locations', dispatch)
    dispatch(
        {
            type: 'add_locations',
            locations: locations
        }
    )
}



const setLocations = (dispatch, state) => (locations)=>{
    dispatch(
        {
            type: 'set_locations',
            locations: locations
        }
    )
}


const setState = (dispatch, state) => (state)=>{
    dispatch(
        {
            type: 'set_state',
            state: state
        }
    )
}

/*const addAproxLocation = dispatch => (location)=>{
    dispatch(
        {
            type: 'add_aprox_location',
            location: location
        }
    )
}*/

const setDescription = (dispatch, state) => (description)=>{
    dispatch(
        {
            type: 'set_description',
            description: description
        }
    )
}

const reset = (dispatch, state) => ()=>{
    dispatch(
        {
            type: 'reset'
        }
    )
}
/*let currentData = await AsyncStorage.getItem('@trackApp:locationData');
let recording = false;
if(currentData && currentData.length > 0){
    recording = true;
}*/

/*AsyncStorage.getItem('@trackApp:locationState').then(state=>{
     
    if (state){
      const parsedState = JSON.parse(state);
      console.log('loaded ', parsedState.locations.length);
    }
    
  })*/


  


export const {Provider, Context} = createDataContext(locationReducer, {startRecording, stopRecording, addLocations, setLocations/*, addAproxLocation*/, setDescription, reset, setState}, {name: "", recording: false, locations : [], currentLocation: null})