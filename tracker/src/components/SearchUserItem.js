import React, {useContext, useEffect, useState} from 'react';
import {View, StyleSheet, Text, ActivityIndicator, TouchableOpacity} from 'react-native';
import {Image, Button} from 'react-native-elements'
import { FontAwesome } from '@expo/vector-icons'; 

import {withNavigation } from 'react-navigation'

import LinkText from '../components/LinkText'

import AuthImage from './AuthImage'

import Icon from 'react-native-vector-icons/Feather';

import {Provider as MainProvider, Context as MainContext} from '../context/mainContext'
import {Provider as AuthProvider, Context as AuthContext} from '../context/authContext'
import Spacer from '../components/Spacer'
import UserDetailsSmall from '../components/UserDetailsSmall'





const SearchUserItem = (props) => {


    
    const {follow, unfollow, getFollowers, searchUsers, getUsers, getUser} = useContext(MainContext);


    const {state: authState, signup} = useContext(AuthContext);



    const user = getUser(props.userId);
    const followers = getFollowers(props.userId)



    let followed = followers.find((follower) => {
        return follower == authState.user._id ? true: false
    });

    const [followState, setFollowState] = useState(null);
    if (followState){
        if (followState.followed !== followed){
            followed = followState.followed
        }else{
            setFollowState(null);
        }
    }

    return (<TouchableOpacity  onPress = { ()=> {}}>
    <Spacer></Spacer>
        <View style= {{    display: 'flex',
            flex: 1,
            flexDirection : 'row-reverse'}}>
            

            {authState.user._id === props.userId?null:followed?  <TouchableOpacity 

                style= {{
                    width: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    padding:5,
                    borderWidth: 1,
                    borderRadius: 30,
                    borderColor: "black",
                    
                }} onPress = {()=> {
                    unfollow(user._id);
                    setFollowState({
                        followed:false
                    })
                    
                }}>
                <Icon
                    name='user-minus'
                    size={20}
                    color="black"
                    
                    />
            </TouchableOpacity>:
            <TouchableOpacity 
                
            style= {{
                width: 50,
                alignItems: "center",
                justifyContent: "center",
                padding:5,
                borderWidth: 1,
                borderRadius: 30,
                borderColor: "black",
                
            }} onPress = {()=> {
                follow(user._id)
                setFollowState({
                    followed:true
                })
                
            }}>
            <Icon
                name='user-plus'
                size={20}
                color="black"
                
                />
        </TouchableOpacity>}

            <View style = {{
                display: "flex",
                flexDirection: "row",
                marginRight: 10,
                flex: 1
            }}>
         <UserDetailsSmall  userId = {user._id} textSize = {30} imageSize = {30}/>

     </View>
 </View>
</TouchableOpacity>
)
};







export default withNavigation(SearchUserItem);