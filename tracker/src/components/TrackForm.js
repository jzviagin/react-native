import React, { useContext , useRef, useEffect, useState} from 'react';
import {Input, Button, Text} from 'react-native-elements';
import {StyleSheet, View, TextInput, KeyboardAvoidingView, Keyboard, Alert} from 'react-native'
import Spacer from './Spacer'

import {connect} from 'react-redux'


import {Context as BgLocationContext} from '../context/bgLocationContext'
import {Context as MainContext} from '../context/mainContext'
import useSaveTrack from '../hooks/useSaveTrack'
import ProgressBar from '../components/ProgressBar'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import TrackDetails from './TrackDetails'



const TrackForm = (props) =>{

    const {getUserTracks, getTrack, createTrack} = useContext(MainContext);
    //const {state: authState} = useContext(AuthContext);

    const {setDescription, startRecording, state: locationState , stopRecording} =  useContext(BgLocationContext);


    const [saveTrack] = useSaveTrack();

    /*const [keyboardVisible, setKeyboardVisible] = useState(false);

    
    useEffect( ()=>{
        const keyboardDidShowListener = Keyboard.addListener(
            "keyboardDidShow",
            ()=>{
                setKeyboardVisible(true);
            }
          );
          const keyboardDidHideListener = Keyboard.addListener(
            "keyboardDidHide",
            ()=>{
                setKeyboardVisible(false);
            }
          );
    },[])*/

   /* componentDidMount() {
      
      }
    
      componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }*/


    console.log('track form rendering recording: ', locationState.recording)
    const scrollViewRef = useRef();

    const showNewTrackDialog = () =>
    Alert.alert(
      "Are you sure?",
      "Starting a new track will discard the current one, press CANCEL and share the current track, or press OK to discard the track and track a new one",
      [
        {
          text: "CANCEL",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => {
            startRecording();
        } }
      ],
      { cancelable: false }
    );

    
    const showEndTrackDialog = () =>
    Alert.alert(
      "Are you sure?",
      "Are you sure you want to end the current track?",
      [
        {
          text: "CANCEL",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => {
            stopRecording()
        } }
      ],
      { cancelable: false }
    );


    return <View style = {{ display:"flex", flexDirection: "column", flex: 1}}> 
        <Spacer>
        
            <TrackDetails/>
            
            {
                ((!(locationState.recording)) && (locationState.locations.length != 0))?
               
                <View style= {{
                    display: "flex",
                    flexDirection: "row-reverse",
                    alignItems: "flex-end",
                    //backgroundColor:"purple"
                    backgroundColor:'rgb(240,240,240)',
                    borderRadius: 20,
                    overflow: 'hidden',
                    
                }} >
                <TouchableOpacity 
                        
                            style= {{

                                padding:10,
                                borderWidth: 1,
                                borderRadius:20,
                                margin: 10,
                                width: 50
                            }} onPress = {()=> {
                                saveTrack();
                            }}>
                            <Icon
                                name='share-alt'
                                size={30}
                                color="black"
                                
                                />
                        </TouchableOpacity>

                    
           

                        <ScrollView  
                            ref = {scrollViewRef}
                            onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
                            style= {{maxHeight: 150}}
                            contentContainerStyle = {{ flexGrow: 1, justifyContent: "center" }}>
                        <TextInput 
                            value = {locationState.description} 
                            rightIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                            style = {{ height: "100%",  marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 , display: 'flex', flex:1}}

                            onChangeText = { (description)=> { setDescription(description)}}
                            placeholder="Write a comment..."

                            autoCapitalize = "none"
                            autoCorrect = {false}
                            returnKeyType="none"
                            multiline = {true}

                        ></TextInput>
                        </ScrollView>
            </View>
            

                :null
            }
         
           

             

        </Spacer>
      
        <View  style={{ display:"flex", flexGrow: 1, flexDirection:"column", alignItems: "center", justifyContent: "flex-end"}}>

        
        {locationState.recording ?                  <TouchableOpacity 
                         
                         style= {{
                             width: 100,
                             alignItems: "center",
                             justifyContent: "center",
                             borderRadius: 30,
                             borderColor: "black",
                             borderWidth: 1,
                             padding:20,
                             margin:50,
                         }} onPress = {()=> {
                             showEndTrackDialog();
                         }}>
                         <Icon
                             name='stop'
                             size={50}
                             color="black"
                             
                             />
                     </TouchableOpacity> :                 <TouchableOpacity 
                         
                         style= {{
                             width: 100,
                             alignItems: "center",
                             justifyContent: "center",
                             padding:20,
                             borderWidth: 1,
                             borderRadius: 30,
                             borderColor: "black",
                             margin:50
                         }} onPress = {()=> {
                             if (locationState.locations.length != 0){
                                 showNewTrackDialog();
                             }else{
                                startRecording();
                             }
                             
                         }}>
                         <Icon
                             name='play'
                             size={50}
                             color="black"
                             
                             />
                     </TouchableOpacity>}
             </View>
        
        
    </View> 
};


const styles = StyleSheet.create({

});


/*

const mapStateToProps = (state) =>{
    return {
        recording: state.recording,
        locations: state.locations,
        name: state.name,
        currentLocation: state.currentLocation
    }
}



const mapDispatchToProps = (dispatch) => {
    return {


        startRecording: ()=>dispatch(locationActions.startRecording()),
        stopRecording: ()=>dispatch(locationActions.stopRecording()),
        addLocations: (locations)=>dispatch(locationActions.addLocations(locations)),
        setState: (state)=>dispatch(locationActions.setState(state)),
        setName: (name)=>dispatch(locationActions.setName(name)),
        reset: ()=>dispatch(locationActions.reset())
    }
}
*/

export default TrackForm;
//export default connect(mapStateToProps, mapDispatchToProps)(TrackForm);



