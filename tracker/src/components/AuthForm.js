import React, {useState} from 'react';
import { StyleSheet, View, ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {Text, Input, Button} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import Icon from 'react-native-vector-icons/Feather';
import NavLink from '../components/NavLink'




const SignupScreen = ({isSignup,navigation, title, buttonTitle, buttonAction, errorMessage, email: providedEmail, password: providedPassword , username: providedUsername, firstName: providedFirstName, lastName: providedLastName}) => {

    const [email, setEmail] = useState(providedEmail);
    const [password, setPassword] = useState(providedPassword)
    const [username, setUsername] = useState(providedUsername)
    const [firstName, setFirstName] = useState(providedFirstName)
    const [lastName, setLastName] = useState(providedLastName)

    console.log('provided 1st', providedFirstName);
    console.log('provided last', providedLastName);
    const checkInputs = ()=>{
        if (password.includes(' ') || email.trim().length === 0 || password.trim().length === 0){
            return false;
        }
        if (isSignup){
            if (username.trim().length === 0 || firstName.trim().length === 0 || lastName.trim().length === 0){
                return false;
            }
        }
        return true;
    }

    //const {state, signup} = useContext(AuthContext);

    let passwordInput;
    let usernameInput;
    let firstNameInput;
    let lastNameInput;

    return (
        <ScrollView style= {{display: "flex"}
    }

    contentContainerStyle = {{ flexGrow: 1, justifyContent: "center"}}
        >
        <View style= {styles.container} >
            
                <Spacer>
                    <Text style = {styles.title}>{title}</Text>
                </Spacer>
                {isSignup?<NavLink style= {{fontWeight:"bold"}} link = "Signin" text = "Already have an account? Sign in instead"/>:
                <NavLink  link = "Signup" text = "Don't have an account? Sign Up instead"/>}

                <Spacer>
                    
                    <View style= {{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "flex-start",
                                //backgroundColor:"purple"
                                backgroundColor:'rgb(240,240,240)',
                                padding: 10,
                                borderRadius: 20,
                                overflow: 'hidden'                }} >
                        <TextInput  
                        style = {{ height: "100%", flex:1, marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}
                        errorStyle = {{display  : "none"}}
                        leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                        keyboardType="email-address"
                        placeholder= "E-mail"
                        containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft:0, paddingRight:0 }}
                        returnKeyType="next"
                        onSubmitEditing={() => { isSignup? usernameInput.focus():passwordInput.focus(); }}
                        leftIcon={
                            <Icon style={{ margin:0}}
                            name='mail'
                            size={18}
                            color='grey'
                            />
                        }
                            label="Email"
                            value = {email} 
                            onChangeText = { (newEmail)=> { setEmail(newEmail)}}
                            autoCapitalize = "none"
                            autoCorrect = {false}>

                            </TextInput>
                        </View>
                </Spacer>
                {
                    isSignup? 
                    <>
                    <Spacer>
                    <View style= {{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "flex-start",
                                //backgroundColor:"purple"
                                backgroundColor:'rgb(240,240,240)',
                                padding: 10,
                                borderRadius: 20,
                                overflow: 'hidden'                }} >
                    <TextInput 
                    style = {{ height: "100%", flex:1, marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}

                    errorStyle = {{display  : "none"}} 
                    ref = {(input)=> { usernameInput= input}}
                    leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                    containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft:0, paddingRight:0 }}
                    returnKeyType="next"
                    onSubmitEditing={() => { firstNameInput.focus(); }}
                    leftIcon={
                        <Icon style={{ margin:0}}
                        name='user'
                        size={18}
                        color='grey'
                        />
                    }
                        placeholder="User Name"
                        value = {username} 
                        onChangeText = { (newUsername)=> { setUsername(newUsername)}}
                        autoCapitalize = "none"
                        autoCorrect = {false}>

                        </TextInput></View>
                </Spacer>
                <Spacer>
                <View style= {{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "flex-start",
                                //backgroundColor:"purple"
                                backgroundColor:'rgb(240,240,240)',
                                padding: 10,
                                borderRadius: 20,
                                overflow: 'hidden'                }} >
                    <TextInput
                        style = {{ height: "100%", flex:1, marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}

                        errorStyle = {{display  : "none"}}
                        ref = {(input)=> {firstNameInput= input}}
                        leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                        containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft:0, paddingRight:0 }}
                        returnKeyType="next"
                        onSubmitEditing={() => { lastNameInput.focus(); }}
                        leftIcon={
                            <Icon style={{ margin:0}}
                            name='user'
                            size={18}
                            color='grey'
                            />
                        }
                            placeholder="First Name"
                            value = {firstName} 
                            onChangeText = { (text)=> { setFirstName(text)}}
                            autoCapitalize = "none"
                            autoCorrect = {false}>

                        </TextInput>
                        </View>
                </Spacer>
                <Spacer>
                    <View style= {{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "flex-start",
                                    //backgroundColor:"purple"
                                    backgroundColor:'rgb(240,240,240)',
                                    padding: 10,
                                    borderRadius: 20,
                                    overflow: 'hidden'                }} >
                    <TextInput  
                        style = {{ height: "100%", flex:1, marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}

                        errorStyle = {{display  : "none"}}
                        ref = {(input)=> { lastNameInput= input}}
                        leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                        containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft:0, paddingRight:0 }}
                        returnKeyType="next"
                        onSubmitEditing={() => { passwordInput.focus(); }}
                        leftIcon={
                            <Icon style={{ margin:0}}
                            name='user'
                            size={18}
                            color='grey'
                            />
                        }
                            placeholder="Last Name"
                            value = {lastName} 
                            onChangeText = { (text)=> { setLastName(text)}}
                            autoCapitalize = "none"
                            autoCorrect = {false}>

                        </TextInput>
                        </View>
                </Spacer></>:null
                }
                <Spacer>
                    <View style= {{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "flex-start",
                                    //backgroundColor:"purple"
                                    backgroundColor:'rgb(240,240,240)',
                                    padding: 10,
                                    borderRadius: 20,
                                    overflow: 'hidden'                }} >
                    <TextInput
                        style = {{ height: "100%", flex:1, marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}

                        errorStyle = {{display  : "none"}}
                        ref = {(input)=> { passwordInput= input}}
                        leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                        containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft:0, paddingRight:0 }}
                        leftIcon={
                            <Icon
                            name='lock'
                            size={18}
                            color='grey'
                            />
                        }
                        placeholder = "Password"
                        value = {password} 
                        onChangeText = { (newPassword)=> { setPassword(newPassword)}}
                        autoCapitalize = "none"
                        autoCorrect = {false}
                        returnKeyType="none"
                        onSubmitEditing={()=> {
                            if(checkInputs() == false){
                                return;
                            }
                            buttonAction(email, password, username, firstName, lastName)
                        }}
                        secureTextEntry
                        enablesReturnKeyAutomatically
                    ></TextInput>
                    </View>
                </Spacer>
            
                {errorMessage ? ( <Spacer><Text style = {styles.errorMessage}>{errorMessage}</Text></Spacer> ): null}
                <Spacer>

                <View style={{flexDirection:'row', justifyContent: 'center'}}>
                <TouchableOpacity 
                            disabled = {checkInputs() == false}
                            style= {{
                                width: 50,
                                alignItems: "center",
                                justifyContent: "center",
                                borderRadius: 20,
                                borderColor: checkInputs()===true?"black":"rgb(200,200,200)",
                                borderWidth: 1,
                                padding:10,
                                margin:5,
                            }} onPress = {()=> {
                                console.log('pressed action', email, password, username, firstName, lastName)
                                buttonAction(email, password, username, firstName, lastName)
                            }}>
                            <Icon
                                name='arrow-right'
                                size={30}
                                color={checkInputs()===true?"black":"rgb(200,200,200)"}
                                
                                />
                        </TouchableOpacity>
                </View>
          
                </Spacer>
                

            
        </View>
        </ScrollView>
        )
};


SignupScreen.navigationOptions =  {
    title:'Home',
    header: null
}


const styles = StyleSheet.create({
    container: {
        marginBottom: 0,
        marginTop:0,
        backgroundColor:"white"
    
    },
    errorMessage :{
        fontSize: 16,
        color: 'red'
    },
    link:{
        color: 'blue'
    },
    title:{
        fontSize:30,
        fontWeight: "bold"
    },

    inputContainer: {
        margin:0,
        padding:0,
        backgroundColor:'red'
    },
    input: {
        margin:0,
        padding:0,
        backgroundColor:'blue'
    },

});

export default SignupScreen;