import React , {useContext} from 'react';
import { StyleSheet, View, ActivityIndicator} from 'react-native';
import {Text} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import  UserPhoto from './UserPhoto'
import {Provider as MainProvider, Context as MainContext} from '../context/mainContext'
import LinkText from '../components/LinkText'
import {withNavigation } from 'react-navigation'
import { TouchableOpacity } from 'react-native-gesture-handler';



const Comment = (props) => {

  

    const {state,  getUser, getTrack, getLikes, getComments,  getComment, like } = useContext(MainContext);

    const comment = getComment(props.commentId);
    if(!comment){
        return <ActivityIndicator/>
    }

    const user = getUser(comment.userId);
    console.log('comment user', user)


    return (
        <View style= {styles.container} >
                <View style = {{display:'flex', flexDirection: 'row'}}>
                    <TouchableOpacity onPress = { ()=> {
                            props.navigation.push('User', {userId: comment.userId});
            }}>
                        <UserPhoto userId = {comment.userId}  imageSize = {30} onNavigate = {
                            props.onNavigate
                        }/>
                    </TouchableOpacity>

                    <View style = {{ padding: 5, display:'flex', overflow: 'hidden', borderRadius: 10, flexDirection: 'column', marginLeft: 10, borderColor: "black", backgroundColor: "rgb(220, 220, 220)"}}>
                        <LinkText textStyle= {{fontSize:18, fontWeight: "bold"}} onPress = { ()=> {
                            props.navigation.push('User', {userId: comment.userId});
            }}>{user.username}</LinkText>
                        <Text style = {{fontSize:18}}>{comment.text}</Text>
                    </View>
                </View>

               
          

           
        </View>
        )
};




const styles = StyleSheet.create({
    container: {
        justifyContent: "flex-start",
        alignItems: "flex-start",
        marginBottom: 5
    },

    author:{
        fontSize:16,
        fontWeight: "bold"
    },
    comment:{
        fontSize:14,
        marginLeft:30
    }

    

});

export default withNavigation(Comment);