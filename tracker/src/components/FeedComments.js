import React, {useState, useContext, useRef} from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Keyboard} from 'react-native';
import {Text, Input, Button} from 'react-native-elements'
import  Spacer from '../components/Spacer'
import Icon from 'react-native-vector-icons/FontAwesome';
import Comment from './Comment'


import {Context as AuthContext}  from '../context/authContext'
import {Provider as MainProvider, Context as MainContext} from '../context/mainContext'
import {  ScrollView } from 'react-native-gesture-handler';



const TrackComments = (props) => {

  



    const { state: authState} = useContext(AuthContext);
    const [commentText, setCommentText] = useState('');

    const {state,  getUser, getTrack, getLikes, getComments,  getComment, like , addComment} = useContext(MainContext);

    const commentsForTrack = getComments(props.trackId);
    const comments = [];
    if(props.limitComments && commentsForTrack.length > 3){
        for (let i = commentsForTrack.length - props.limitComments ; i < commentsForTrack.length ; i++){
            comments.push(commentsForTrack[i]);
        }
    }else{
        comments.push.apply(comments, commentsForTrack);
    }

/* leftIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}

                    leftIcon={
                        <Icon
                        name='align-center'
                        size={18}
                        color='grey'
                        />
                    }


*/

    const scrollViewRef = useRef();

    const checkInput = ()=>{

        const ret =  ((commentText.trim() === '') ? false: true);
        console.log('check input ', ret, commentText);
        return ret;
    }

    return (
        <View style= {{
            justifyContent: "center",
            marginBottom: 0,
            flex: 1
        }} >
           
            <ScrollView  ref = {scrollViewRef}
            onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
            style= {{}}
            contentContainerStyle = {{ flexGrow: 1, justifyContent: "flex-end"}}>
            {
                comments.map( comment => {
                    return <Comment key = {comment} commentId = {comment}/>
                })
            }
            </ScrollView>
            





            <View style= {{
                display: "flex",
                flexDirection: "row-reverse",
                alignItems: "flex-end",
                //backgroundColor:"purple"
                backgroundColor:'rgb(240,240,240)',
                borderRadius: 20,
                overflow: 'hidden'
            }} >

           
          
                <TouchableOpacity 
                    disabled = {checkInput()==false}
                    style= {{
                        padding:10
                    }} onPress = {()=> {
                        addComment(props.trackId, commentText);
                        setCommentText("");
                        Keyboard.dismiss();
                    }}>
                    <Icon
                        name='send-o'
                        size={22}
                        color={checkInput()===true?"black":"rgb(200,200,200)"}
                        
                        />
                </TouchableOpacity>

                    
           



                <TextInput 


                  

                    value = {commentText} 
                    

                    rightIconContainerStyle={{ marginLeft: 0, marginRight: 10 }}
                    style = {{ height: "100%",  marginTop :0, paddingTop: 0 , marginBottom :0, paddingBottom: 0, marginLeft: 0, marginLeft: 10, paddingLeft:0, paddingRight:0 ,...styles.input2}}
            
                    onChangeText = { (text)=>{

                        console.log(text);
                        setCommentText(text);
                    }}
                    placeholder="Write a comment..."
                   
                    autoCapitalize = "none"
                    autoCorrect = {false}
                    returnKeyType="none"
                    multiline = {true}
                   
                ></TextInput>
            </View>

        
            

            
        </View>
        )
};




const styles = StyleSheet.create({

    title:{
        fontSize:30,
        fontWeight: "bold"
    },
    input: {
        display: "flex",
        flex: 1,
        //backgroundColor:"red",
        paddingBottom: 0,
        marginBottom: 0
       // height: 50
    },
    input2: {
        display: "flex",
       // height: 20,
        flex: 1,
        //backgroundColor:"blue"

    }

});

export default TrackComments;