import React, {useContext, useEffect}from 'react';
import {Text, StyleSheet, ActivityIndicator} from 'react-native';
import MapView, {Polyline, Circle} from 'react-native-maps'
import {connect} from 'react-redux'
//import {Context as LocationContext} from '../context/locationContext'
import {Context as BgLocationContext} from '../context/bgLocationContext'
//import useBgLocation from '../hooks/useBgLocation'


const Map = (props) => {
    console.log('rendering map')

    const {state, getCurrentLocation} = useContext(BgLocationContext);
    //const [err, locationState, startRecording, stopRecording, setName] = useBgLocation()
    //console.log('map:locationState', state)
    let points = [];
    /*for (let i = 0 ; i <20 ; i++){
        if (i % 2 === 0){
            points.push({
                latitude: 37.33233 + i * 0.001,
                longitude: -122.03121  + i * 0.001
            })
        }else{
            points.push({
                latitude: 37.33233 - i * 0.002,
                longitude: -122.03121  +  i * 0.001
            })
        }

    }*/

    if (!state.currentLocation){
        getCurrentLocation();
        return <ActivityIndicator size="large" style =  {{marginTop: 200}} />
    }


    for (let i = 0 ; i < state.locations.length ; i++){

        points.push({
            latitude: state.locations[i].coords.latitude,
            longitude: state.locations[i].coords.longitude
        })
    }


    console.log(points.length, ' points');
    return <MapView style = {styles.map}
        region = {{
            latitude:state.currentLocation.coords.latitude,
            longitude: state.currentLocation.coords.longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
        }}
        >

            <Polyline coordinates = {points}/>
            <Circle
                center = {state.currentLocation.coords}
                radius = {15}
                strokeColor = "rgba(158,158,255, 1.0)"
                fillColor = "rgba(158,158,255,0.3)"
            />
        </MapView>
}

const styles = StyleSheet.create({
    map:{
        height: 300
    }

});




const mapStateToProps = (state) =>{
    return {
        recording: state.recording,
        locations: state.locations,
        name: state.name,
        currentLocation: state.currentLocation
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        //onFetchOrders: (token, userId) => dispatch(locationActions.fetchOrdersStart (token, userId)),
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})

        startRecording: ()=>dispatch(locationActions.startRecording()),
        stopRecording: ()=>dispatch(locationActions.stopRecording()),
        addLocations: (locations)=>dispatch(locationActions.addLocations(locations)),
        setState: (state)=>dispatch(locationActions.setState(state)),
        setName: (name)=>dispatch(locationActions.setName(name)),
        reset: ()=>dispatch(locationActions.reset())
    }
}

export default Map;
//export default connect(mapStateToProps, mapDispatchToProps)(Map);

