import React, {useContext, useEffect} from 'react';
import {View, StyleSheet, Text, ActivityIndicator, TouchableOpacity} from 'react-native';
import {Image, Button} from 'react-native-elements'

import {withNavigation } from 'react-navigation'

import Icon from 'react-native-vector-icons/FontAwesome'




const ModalElement = (props)=>{

    //console.log('modal element main element ', props.mainElement);
    return (

        <View style={{ padding: 10, height: "100%", flex: 1, borderColor: 'grey', borderWidth: 1, borderRadius:30, overflow: 'hidden', backgroundColor:'white'}}>
                            <TouchableOpacity 
                 
                    style= {{

                        padding:10
                    }} onPress = {
                        ()=>{props.onDismiss()}
                    }>
                    <Icon
                        name='arrow-left'
                        size={22}
                        
                        
                        />
                </TouchableOpacity>
            {props.mainElement}
          
        </View>
    )
}





export default withNavigation(ModalElement);