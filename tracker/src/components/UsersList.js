import React, {useState} from 'react';
import { StyleSheet, View} from 'react-native';
import {Text, Input, Button} from 'react-native-elements'
import  Spacer from './Spacer'
import Icon from 'react-native-vector-icons/Feather';
import UserDetailsSmall from './UserDetailsSmall'



const UserList = (props) => {

    return (
        <View style= {styles.container} >
           
            <Text style = {styles.title}>{props.title}:</Text>
            

            {
                props.users.map( user => {
                    return <UserDetailsSmall userId = {user} textSize = {30} imageSize = {30}/>
                })
            }
        </View>
        )
};




const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        marginBottom: 0
    },

    title:{
        fontSize:30,
        fontWeight: "bold"
    },

   

});

export default UserList;