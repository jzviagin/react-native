import React, { useContext } from 'react';
import {Input, Button, Text} from 'react-native-elements';
import {StyleSheet, View} from 'react-native'

import {connect} from 'react-redux'


//import {Context as LocationContext} from '../context/locationContext'
import {Context as BgLocationContext} from '../context/bgLocationContext'
//import {Context as TrackContext} from '../context/trackContext'

import useTrackTools from '../hooks/useTrackTools'

const TrackDetails = (props) =>{

    const {setDescription, startRecording, state: locationState , stopRecording} =  useContext(BgLocationContext);
    //const { state : trackState, fetchTracks, createTrack} =  useContext(TrackContext);


    const [calcTrackDistance, getTrackStartStartTime, getTrackDuration, getPace] = useTrackTools();

    const track  = {
        locations: [...locationState.locations, locationState.currentLocation]
    }

    return  (locationState.locations && locationState.locations != 0)?(
        <View style = {styles.rows}>
                <View>
                    <View style = {styles.columns}>
                        <Text style = {styles.titleText}>Start Time: </Text>
                        <Text style = {styles.dataText}>{getTrackStartStartTime(track)}</Text>
                    </View>
                    <View style = {styles.columns}>
                        <Text style = {styles.titleText}>Duration: </Text>
                        <Text style = {styles.dataText}>{getTrackDuration(track)} </Text>
                    </View>
                    <View style = {styles.columns}>
                        <Text style = {styles.titleText}>Distance: </Text>
                        <Text style = {styles.dataText}>{(calcTrackDistance(track)/ 1000).toFixed(2) +' KM'} </Text>
                    </View>
                    <View style = {styles.columns}>
                        <Text style = {styles.titleText}>Average Pace: </Text>
                        <Text style = {styles.dataText}>{getPace(track).toFixed(2) + ' Min/KM'} </Text>
                    </View>
                </View>
            </View>
    ):null;
};


const styles = StyleSheet.create({
    rows: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    columns: {
        
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 22
    },
    dataText: {
        fontSize: 20
    }
});


/*

const mapStateToProps = (state) =>{
    return {
        recording: state.recording,
        locations: state.locations,
        name: state.name,
        currentLocation: state.currentLocation
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        //onFetchOrders: (token, userId) => dispatch(locationActions.fetchOrdersStart (token, userId)),
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})

        startRecording: ()=>dispatch(locationActions.startRecording()),
        stopRecording: ()=>dispatch(locationActions.stopRecording()),
        addLocations: (locations)=>dispatch(locationActions.addLocations(locations)),
        setState: (state)=>dispatch(locationActions.setState(state)),
        setName: (name)=>dispatch(locationActions.setName(name)),
        reset: ()=>dispatch(locationActions.reset())
    }
}
*/

export default TrackDetails;
//export default connect(mapStateToProps, mapDispatchToProps)(TrackDetails);


