import React, {useContext, useEffect,useState} from 'react';
import {View, StyleSheet, Text, ActivityIndicator, Linking, TouchableOpacity} from 'react-native';
import {Image, Button} from 'react-native-elements'
import { FontAwesome } from '@expo/vector-icons'; 
import { color } from 'react-native-reanimated';

import {Provider as MainProvider, Context as MainContext} from '../context/mainContext'
import {Provider as AuthProvider, Context as AuthContext} from '../context/authContext'


import AuthImage from './AuthImage'

import useModal from '../hooks/useModal'

import UsersList from './UsersList'

import UserTracks from './UserTracks'

import LinkText from '../components/LinkText'
import Icon from 'react-native-vector-icons/Feather';



const UserDetails = (props) => {


    const {state,  getUser, getFollowing, getFollowers, follow, unfollow } = useContext(MainContext);
    const {state:authState } = useContext(AuthContext);

    console.log('user details ',  props);



    const user = getUser(props.userId);

    const followers = getFollowers(props.userId);

    const following = getFollowing(props.userId);

    const [ModalElem, showModal] = useModal();
    const [followState, setFollowState] = useState(null);



    if(!user){
        return <ActivityIndicator />
    }

    let followed = followers.find((follower) => {
        return follower == authState.user._id ? true: false
    });

   
    if (followState){
        if (followState.followed !== followed){
            followed = followState.followed
        }else{
            setFollowState(null);
        }
    }




    
return <>

<ModalElem/>

    


    <View style = {styles.container}>
        < Text style = {{ fontSize:30, fontWeight: "bold"}}>{user.username}</Text>
        <View style={styles.imageContainer}>
        {user.photo?<AuthImage
            style={styles.image}
        
            source={{ uri: user.photo  }}
            PlaceholderContent={<ActivityIndicator />}
            />:
            <View style = {{display: 'flex', alignItems:'center', justifyContent:'center', width: '100%' , height:'100%'  }}> 
                <FontAwesome name="user"    size = {250} color="grey" />
            </View>
            
        }
        </View>

        {authState.user._id === props.userId? null:followed?  <TouchableOpacity 

            style= {{
                width: 65,
                alignItems: "center",
                justifyContent: "center",
                padding:5,
                borderWidth: 1,
                borderRadius: 30,
                borderColor: "black",
                
            }} onPress = {()=> {
                unfollow(user._id);
                setFollowState({
                    followed:false
                })
                
            }}>
            <Icon
                name='user-minus'
                size={35}
                color="black"
                
                />
            </TouchableOpacity>:
            <TouchableOpacity 

            style= {{
            width: 65,
            alignItems: "center",
            justifyContent: "center",
            padding:5,
            borderWidth: 1,
            borderRadius: 30,
            borderColor: "black",

            }} onPress = {()=> {
            follow(user._id)
            setFollowState({
                followed:true
            })

            }}>
            <Icon
            name='user-plus'
            size={35}
            color="black"

            />
    </TouchableOpacity>}
    < Text style = {{ marginTop:20, marginBottom:5, fontSize:24, fontWeight: "bold"}}>{user.firstName}  {user.lastName}</Text>
    < LinkText textStyle = {{  marginBottom:20, fontSize:18, fontWeight: "normal" , textDecorationLine: 'underline'}} onPress = {()=>{
        Linking.openURL('mailto:' + user.email)
    }}>{user.email}</LinkText>

     

        <View style = {{display: "flex", flexDirection: "row", alignItems: "center", marginBottom: 10 ,
            borderTopWidth: 1, borderTopColor: "rgb(220, 220, 220)",
            borderBottomWidth: 1, borderBottomColor: "rgb(220, 220, 220)",
            paddingTop: 5,
            paddingBottom: 5}}> 
        
           
            {following.length > 0 ? <LinkText textStyle = {{marginRight:10, marginLeft: 10, fontSize:18, fontWeight: "bold"  }} 
                 onPress = {()=>{
                    showModal(<UsersList title = "Following" users= {following}/>)
            }}>{following.length} following</LinkText>: null}
            {followers.length > 0 ? <LinkText   textStyle= {{marginRight:10, marginLeft: 10, fontSize:18, fontWeight: "bold" }} onPress = {()=>{
                showModal(<UsersList title = "Followers" users= {followers}/>)
            }} >{followers.length} followers</LinkText>: null}

            <LinkText   textStyle= {{marginRight:10, marginLeft: 10, fontSize:18, fontWeight: "bold" }} onPress = {()=>{
                            showModal(<UserTracks userId = {props.userId}></UserTracks>)
                        }} >Tracks</LinkText>

            



        </View>
        
        
          
       
    </View>
      
    </>
};





const styles = StyleSheet.create({

    container:{
      
        display: "flex",
        alignItems: "center",
        justifyContent : "center"
       
    },
    welcome: {
        fontSize:20, margin:20
    },
   
   
    imageContainer: {
        marginTop: 20,
        marginBottom: 20,
        borderRadius:150,
        width: 300, height:300 ,
  
        borderWidth: 2,
        borderColor: "grey",
        overflow: 'hidden',
        
    },
    image: {
        width: '100%' , height:'100%' 
     
    }
});

export default UserDetails;