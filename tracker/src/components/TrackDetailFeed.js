import React, { useEffect , useContext, useState} from 'react';
import {View, StyleSheet, Text, ActivityIndicator, TouchableOpacity} from 'react-native';
import MapView, {Polyline, Circle, Marker} from 'react-native-maps'
import useTrackTools from '../hooks/useTrackTools'
import FeedComments from './FeedComments'
import trackerApi from '../api/tracker'

import {Context as AuthContext}  from '../context/authContext'

import { FontAwesome } from '@expo/vector-icons'; 

import UserDetailsSmall from './UserDetailsSmall'

import {withNavigation } from 'react-navigation'

import {Provider as MainProvider, Context as MainContext} from '../context/mainContext'

import useModal from '../hooks/useModal'

import UserList from '../components/UsersList'


import AuthImage from '../components/AuthImage'
import LinkText from '../components/LinkText'
import {  } from 'react-native-gesture-handler';











const TrackDetailFeed = (props) => {



    /*useEffect(()=>{
        trackerApi.post( '/like', {
            trackId : props.track._id
        })
    })*/



    //const [points, setPoints] = useState([])
    const points = []

    const [ModalElem, showModal] = useModal();

    const {state,  getUser, getTrack, getLikes, getComments, like, unlike } = useContext(MainContext);
    const {state: authState, signup} = useContext(AuthContext);

    const [likeState, setLikeState] = useState(null);



    //const [track, setTrack] = useState(null);

    

    const track = getTrack(props.trackId);

    
    if(!track){
        return <ActivityIndicator />
    }



    const likes = getLikes(props.trackId)

    const comments = getComments(props.trackId)

    




    //let dups = [];



    console.log('rendering track details', props.trackId);

    /*useEffect(()=> {
        console.log('rendering track details 1st time', props.trackId);
        const pts = [];
        for (let i = 0 ; i < track.locations.length ; i++){

            pts.push({
                latitude: track.locations[i].coords.latitude,
                longitude: track.locations[i].coords.longitude
            })
    
            
        }
       
        setPoints(pts);
    }, [props.trackId])*/


    let path = "path=color:0x0000ff";
    /*path=color:0x0000ff|weight:5|40.737102,-73.990318|40.749825,-73.987963|40.752946,-73.987384|40.755823,-73.986397*/

    for (let i = 0 ; i < track.locations.length ; i++){

        path = path + "|" + track.locations[i].coords.latitude + "," + track.locations[i].coords.longitude

        points.push({
            latitude: track.locations[i].coords.latitude,
            longitude: track.locations[i].coords.longitude
        })

        
    }


    const [calcTrackDistance, getTrackStartStartTime, getTrackDuration, getPace] = useTrackTools();

    let liked = likes.find((likeElem) => {
        //console.log('finding like ', likeElem.userId);
        //console.log('finding like  auth state', authState.user);
        return likeElem == authState.user._id ? true: false
    });

    if (likeState){
        if (likeState.liked === liked){
            setLikeState(null);
        }else{
            liked = likeState.liked;
        }
    }

    const mapUri = "https://maps.googleapis.com/maps/api/staticmap?size=600x600&" + path + "&key=AIzaSyB28jN9Ez3EaS8uRUPkMMj-OB_AtGzlRss"

    console.log("path", mapUri);

    
    

    return <View >


            <ModalElem/>

        {props.disableUser?null:<UserDetailsSmall userId = {track.userId} textSize = {30} imageSize = {30}/>}
        
        
        <AuthImage
            style = {styles.map}
        
            source={{ uri: mapUri  }}
            PlaceholderContent={<ActivityIndicator />}
            />

    {
        (()=>{
            if (track.description && track.description.trim() != ""){
                return  (
                    <View style = {{ padding: 15, display:'flex', overflow: 'hidden', borderRadius: 30, flexDirection: 'column', marginTop: 10, marginBottom: 10, borderColor: "black", backgroundColor: "rgb(240, 240, 240)"}}>
                            <  Text style = {{fontSize:18}}>{track.description}</Text>
                    
                    </View>
                  
                    
                    )
            }else{
                return null;
            }
        })()
    }
   

       
        <View style = {styles.rows}>
            <View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Start Time: </Text>
                    <Text style = {styles.dataText}>{getTrackStartStartTime(track)}</Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Duration: </Text>
                    <Text style = {styles.dataText}>{getTrackDuration(track)} </Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Distance: </Text>
                    <Text style = {styles.dataText}>{(calcTrackDistance(track)/ 1000).toFixed(2) +' KM'} </Text>
                </View>
                <View style = {styles.columns}>
                    <Text style = {styles.titleText}>Average Pace: </Text>
                    <Text style = {styles.dataText}>{getPace(track).toFixed(2) + ' Min/KM'} </Text>
                </View>
            </View>
        </View>

       
        <View style = {{display: "flex", flexDirection: "row", alignItems: "center", marginBottom: 10 ,
            borderTopWidth: 1, borderTopColor: "rgb(220, 220, 220)",
            borderBottomWidth: 1, borderBottomColor: "rgb(220, 220, 220)",
            paddingTop: 5,
            paddingBottom: 5}}> 
        <TouchableOpacity onPress = { ()=> {
                        if(liked){
                            unlike(props.trackId);
                            setLikeState({
                                liked:false
                            })
                        }else{
                            like(props.trackId);
                            setLikeState({
                                liked:true
                            })

                            setTimeout(()=>{
                                like(props.trackId);
                            }, 1000);
                        }
                        
                        } 
                        }>
            <FontAwesome style = {{marginRight:10}} 
                        name={liked?"heart":"heart-o"} size={30}  />
        </TouchableOpacity>
           
            {likes.length > 0 ? <LinkText textStyle = {{marginRight:10, fontSize:18, fontWeight: "bold"  }} 
                 onPress = {()=>{
                    showModal(<UserList title = "Likes" users = {likes}/>)
            }}>{likes.length} {likes.length ==1? "like":"likes"}</LinkText>: null}
            {comments.length > 3 ? <LinkText   textStyle= {{fontSize:18, fontWeight: "bold" }} onPress = {()=>{
                showModal(<FeedComments trackId = {props.trackId}/>)
            }} >{comments.length} comments</LinkText>: null}
        </View>
        <FeedComments limitComments = {3} trackId = {props.trackId}/>
    </View>
};



const styles = StyleSheet.create({
    map:{
        height: 300
    },
    rows: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flex:1
    },
    columns: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 22
    },
    dataText: {
        fontSize: 20
    }
});

export default withNavigation(React.memo(TrackDetailFeed));