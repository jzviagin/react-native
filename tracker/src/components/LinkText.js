import React from 'react'
import {StyleSheet, TouchableOpacity} from 'react-native'
import {withNavigation } from 'react-navigation'

import {Text} from 'react-native-elements'
import { FontAwesome } from '@expo/vector-icons'; 
import  Spacer from './Spacer'
const LinkText = (props)=>{
    return (
        <TouchableOpacity style = {{ ...props.style}} onPress = { ()=> {props.onPress()}}>
            <Text {...props } style= {{...props.textStyle}} onPress= {null}>{props.children}</Text>
    </TouchableOpacity>
    )
}



export default withNavigation(LinkText);