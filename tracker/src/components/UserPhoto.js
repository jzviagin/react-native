import React, {useContext, useEffect} from 'react';
import {View, StyleSheet, Text, ActivityIndicator} from 'react-native';
import {Image, Button} from 'react-native-elements'
import { FontAwesome } from '@expo/vector-icons'; 

import {withNavigation } from 'react-navigation'

import AuthImage from './AuthImage'

import {Provider as MainProvider, Context as MainContext} from '../context/mainContext'





const UserPhoto = (props) => {

    const {state,  getUser, getFollowing, getFollowers } = useContext(MainContext);
    const user = getUser(props.userId);

    //console.log('small details nav ', props.navigation)

    if(!user){
        return <ActivityIndicator />
    }

    
return <>

    

    <View style = {styles.container}>
        
        <View style={
            {

                borderRadius:(props.imageSize/2),
                width: props.imageSize, 
                height: props.imageSize ,
          
                borderWidth: props.borderWidth,
                borderColor: "grey",
                overflow: 'hidden',
            }
        }>
        {user.photo?<AuthImage
            style={styles.image}
        
            source={{ uri: user.photo  }}
            PlaceholderContent={<ActivityIndicator />}
            />:
            <View style = {{display: 'flex', alignItems:'center', justifyContent:'center', width: '100%' , height:'100%'  }}> 
                <FontAwesome name="user"    size = {props.imageSize/ 2} color="grey" />
            </View>
            
        }
        </View>

       

       
        
        
          
       
    </View>
      
    </>
};





const styles = StyleSheet.create({

    container:{
      
        display: "flex",
        flexDirection: 'row',
        alignItems: "center",
        justifyContent : "flex-start"
       
    },
  
   
    image: {
        width: '100%' , height:'100%' 
     
    }
});

export default withNavigation(UserPhoto);