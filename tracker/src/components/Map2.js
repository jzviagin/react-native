import React, {useContext}from 'react';
import {Text, StyleSheet, ActivityIndicator} from 'react-native';
import MapView, {Polyline, Circle} from 'react-native-maps'
import {connect} from 'react-redux'
import {Context as BgLocationContext} from '../context/bgLocationContext'
//import useBgLocation from '../hooks/useBgLocation'

import useSaveTrack from '../hooks/useSaveTrack'


const Map2 = (props) => {
    //console.log('rendering map')

    const {state} = useContext(BgLocationContext);
    //const [err, locationState, startRecording, stopRecording, setName] = useBgLocation()

    //const { state : trackState, fetchTracks, createTrack} =  useContext(TrackContext);

    const [saveTrack] = useSaveTrack();


    //console.log('track form rendering')

    //const [err, locationState, startRecording, stopRecording, setName] = useBgLocation()

    //console.log('isSaving', trackState.isSaving)

    //console.log('map:locationState', locationState)
    //let points = [];
    /*for (let i = 0 ; i <20 ; i++){
        if (i % 2 === 0){
            points.push({
                latitude: 37.33233 + i * 0.001,
                longitude: -122.03121  + i * 0.001
            })
        }else{
            points.push({
                latitude: 37.33233 - i * 0.002,
                longitude: -122.03121  +  i * 0.001
            })
        }

    }*/
   /* if (!locationState.currentLocation){
        return <ActivityIndicator size="large" style =  {{marginTop: 200}} />
    }


    for (let i = 0 ; i < locationState.locations.length ; i++){
        points.push({
            latitude: locationState.locations[i].coords.latitude,
            longitude: locationState.locations[i].coords.longitude
        })
    }*/

    return <Text>{state.locations.length}</Text>


    
    /*return <MapView style = {styles.map}
        region = {{
            latitude:locationState.currentLocation.coords.latitude,
            longitude: locationState.currentLocation.coords.longitude,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
        }}
        >

            <Polyline coordinates = {points}/>
            <Circle
                center = {locationState.currentLocation.coords}
                radius = {15}
                strokeColor = "rgba(158,158,255, 1.0)"
                fillColor = "rgba(158,158,255,0.3)"
            />
        </MapView>*/
}

const styles = StyleSheet.create({
    map:{
        height: 300
    }

});




const mapStateToProps = (state) =>{
    return {
        recording: state.recording,
        locations: state.locations,
        name: state.name,
        currentLocation: state.currentLocation
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        //onFetchOrders: (token, userId) => dispatch(locationActions.fetchOrdersStart (token, userId)),
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})

        startRecording: ()=>dispatch(locationActions.startRecording()),
        stopRecording: ()=>dispatch(locationActions.stopRecording()),
        addLocations: (locations)=>dispatch(locationActions.addLocations(locations)),
        setState: (state)=>dispatch(locationActions.setState(state)),
        setName: (name)=>dispatch(locationActions.setName(name)),
        reset: ()=>dispatch(locationActions.reset())
    }
}

export default Map2;
//export default connect(mapStateToProps, mapDispatchToProps)(Map);

