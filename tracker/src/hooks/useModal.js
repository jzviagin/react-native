import React, {useState, useCallback} from 'react';

import ModalElement from '../components/ModalElement'

import {NavigationEvents} from 'react-navigation'


import Modal from 'react-native-modal';








const useModal =  ()=>{
    const [modal, setModal] = useState(null);

    const modalElem =  useCallback((props)=>{
        return (
            <>
            <NavigationEvents onWillBlur = {()=>{
                setModal(null);
            }}/>
            <Modal  onBackButtonPress={() => {setModal(null)}} isVisible={modal=== null ? false:true}>
                <ModalElement mainElement = {modal}  onDismiss = { ()=>{setModal(null)}} />
            </Modal>
            </>
        );
    },
    [modal]
    )



   
    const showModal = (elem)=>{
        setModal(elem);
    }
   
    return [modalElem, showModal];
}



export default useModal;