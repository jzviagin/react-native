import React, {useContext, useEffect} from 'react';

import {withNavigation } from 'react-navigation'


import {View, StyleSheet, Text, ActivityIndicator, TouchableOpacity} from 'react-native';


import Icon from 'react-native-vector-icons/FontAwesome'
const withBackNavigation = (Component)=>{


    const ret =  withNavigation((props)=>{
        


        return (
        <View style={{ padding: 10, height: "100%", flex: 1, backgroundColor:'white'}}>
                            <TouchableOpacity 
                 
                    style= {{

                        padding:10
                    }} onPress = {

                        ()=>{
                            console.log('navigation', props.navigation)
                            props.navigation.goBack()
                        }
                    }>
                    <Icon
                        name='arrow-left'
                        size={22}
                        
                        
                        />
                </TouchableOpacity>
            <Component {...props}/>
          
        </View>)
    
    })

    ret.navigationOptions = {
        headerShown: false,
        header: null
    }
    return ret;
    
}


export default withBackNavigation