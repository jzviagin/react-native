import {useContext} from 'react';
import {Context as MainContext} from '../context/mainContext'
//import {Context as LocationContext} from '../context/locationContext'
import {Context as BgLocationContext} from '../context/bgLocationContext'
import {navigate} from '../navigationRef'
import {connect} from 'react-redux'




import { shallowEqual, useSelector , useDispatch} from 'react-redux'

//import * as locationActions from '../store/locationActions'
//import {Context as AuthContext} from '../context/authContext'

const useSaveTrack =  ()=>{
    const {createTrack} = useContext(MainContext);
    const bla = useContext(MainContext);

   /* const locations = useSelector( (state)=>{
        return state.locations;
    });
    const name = useSelector( (state)=>{
        return state.name;
    });*/
   /// const dispatch = useDispatch();

    const {state: locationState, reset} = useContext(BgLocationContext);
    //const {state: authState} = useContext(AuthContext);


    const saveTrack = async ()=> {
        console.log('save track', bla)


        console.log('name', locationState.description);
        createTrack(locationState.locations, locationState.description);
        //dispatch(locationActions.reset());
        reset();
        navigate('TrackList')
    }
   
    return [saveTrack];
}



export default useSaveTrack;


