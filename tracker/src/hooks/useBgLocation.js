import {useState, useEffect, useContext, useCallback} from 'react';
import {AsyncStorage} from 'react-native'

import * as TaskManager from 'expo-task-manager';
import {getPreciseDistance} from 'geolib'

//import { shallowEqual, useSelector , useDispatch} from 'react-redux'

//import * as locationActions from '../store/locationActions'

import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';

//import {Context as LocationContext} from '../context/locationContext'


console.log('sdsdfsdfsdfsdfsdfs sdf sdf sdf sdf sdfs dfsdf sdfsdfsdf');


let locationCallback = null;


BackgroundGeolocation.configure({
    desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
    stationaryRadius: 0,
    distanceFilter: 0,
    startForeground: true,
    notificationTitle: 'Background tracking',
    notificationText: 'enabled',
    debug: false,
    startOnBoot: false,
    stopOnTerminate: false,
    locationProvider: BackgroundGeolocation.RAW_PROVIDER,
    interval: 200,
    fastestInterval: 200,
    activitiesInterval: 1,
    maxLocations: 100000,
    stopOnStillActivity: false,
});

BackgroundGeolocation.on('stationary', (stationaryLocation) => {
});

BackgroundGeolocation.on('error', (error) => {
    console.log('[ERROR] BackgroundGeolocation error:', error);
});

BackgroundGeolocation.on('start', () => {
    console.log('[INFO] BackgroundGeolocation service has been started');
});

BackgroundGeolocation.on('stop', () => {
    console.log('[INFO] BackgroundGeolocation service has been stopped');
});

BackgroundGeolocation.on('authorization', (status) => {
    console.log('[INFO] BackgroundGeolocation authorization status: ' + status);
    if (status !== BackgroundGeolocation.AUTHORIZED) {

        
    }
});

BackgroundGeolocation.on('background', () => {
    console.log('[INFO] App is in background');
    //BackgroundGeolocation.stop();
    //BackgroundGeolocation.start();

});

BackgroundGeolocation.on('foreground', () => {
    console.log('[INFO] App is in foreground');

});

BackgroundGeolocation.on('abort_requested', () => {
    console.log('[INFO] Server responded with 285 Updates Not Required');

});

BackgroundGeolocation.on('http_authorization', () => {
    console.log('[INFO] App needs to authorize the http requests');
});


const foo = (location)=>{
    console.log('foooooooooooooooooooooooooooooooo')
}


BackgroundGeolocation.on('location', (location) => {

   



});

//BackgroundGeolocation.on('location', foo);

const foo2 = async ()=>{
    
    //console.log('location in real time', location.id)
    //BackgroundGeolocation.deleteLocation(location.id);

    BackgroundGeolocation.getLocations(
        function (locations) {
                const locationsToSet = []
                //console.log('loaded locations ',locations.length);

                for ( let i = 0 ; i < locations.length; i++){
                        console.log('location from storage', locations[i].id);
                        let deleted = false;
                        BackgroundGeolocation.deleteLocation(locations[i].id, ()=>{
                            console.log('delete success')
                            deleted = true;
                        },
                        ()=>{
                            console.log('delete fail');
                            deleted = true;
                        });

                        while (deleted === false){
                            console.log('waiting for delete')
                        }
                        
                        locationsToSet.push({
                        coords: {
                            latitude: locations[i].latitude,
                            longitude: locations[i].longitude,
                            accuracy: locations[i].accuracy
                        },
                        origId: locations[i].id,
                        insertTime: new Date(),
                        timestamp: locations[i].time
                        });
                
                }
                
        
            
                if (locationsToSet.length != 0 && locationCallback){
                    locationCallback(locationsToSet);
                
                 }

                 setTimeout(foo2, 1000);
                
            }
        
    );


    /*if (locationCallback){

        locationCallback([{
        coords: {
            latitude: location.latitude,
            longitude: location.longitude,
            accuracy: location.accuracy
        },
        timestamp: location.time
        }]);
    }*/
      
        
    
}


/*const handleLocations = async(locations, onDone)=>{

}*/

const deleteLocations = async (locations,i, onDone) =>{
    //console.log('location from storage i = ', i);
    if(i < locations.length){
        console.log('location from storage', locations[i].id);
        BackgroundGeolocation.deleteLocation(locations[i].id, ()=>{
            console.log('delete success')
            deleteLocations(locations, i + 1, onDone)
        },
        ()=>{
            console.log('delete fail');
            deleteLocations(locations, i + 1, onDone)
            
    
        });
    
        
    }else{
        //console.log('done')
        onDone()
    }
   

}



const handleStoredLocations = async ()=>{


    BackgroundGeolocation.getLocations(
        function (locations) {
                const locationsToSet = []

                //console.log('loaded locations ',locations.length);

                for ( let i = 0 ; i < locations.length; i++){
                        console.log('location from storage', locations[i].id);
                       
                        
                        locationsToSet.push({
                        coords: {
                            latitude: locations[i].latitude,
                            longitude: locations[i].longitude,
                            accuracy: locations[i].accuracy
                        },
                        origId: locations[i].id,
                        insertTime: new Date(),
                        timestamp: locations[i].time
                        });
                
                }
                
        
            
                if (locationsToSet.length != 0 && locationCallback){
                    locationCallback(locationsToSet);
                
                 }


                deleteLocations(locations, 0, ()=>{
                    setTimeout(handleStoredLocations, 1000);
                })
                
                
        
            
         

                 
                
            }
        
    );
}



handleStoredLocations();



let initialState = {description: "", recording: false, locations : [], currentLocation: null};





export default ()=> {





    

    const [err, setErr] = useState(null);

    const [locationState, setLocationState] = useState(initialState);
    initialState = {...locationState};
    
    console.log('useBgLocation hook running222222 current locations', locationState.locations.length);

    locationCallback =   (locations)=> {
        console.log('adding new locations: ', locations.length);
        let newLocations = [...locationState.locations];
        for (let i = 0; i < locations.length ; i++){
            console.log('adding new locations: ', newLocations.length);
            if (newLocations.length != 0){
                const distance = getPreciseDistance({
                    latitude: newLocations[newLocations.length -1 ].coords.latitude,
                    longitude: newLocations[newLocations.length -1].coords.longitude
                }, 
                {
                    latitude: locations[i].coords.latitude,
                    longitude: locations[i].coords.longitude
                });
                console.log( locations[i]);
                //console.log('distance', distance);
                //console.log('accuracy', action.locations[i].coords.accuracy)
                if (distance > 10 &&  locations[i].coords.accuracy ){
                    //console.log('meets creteria');
                    newLocations.push(locations[i]); 
                }else{
                    //console.log('not meets creteria');
                }
            }else{
                //console.log('1st location');
                newLocations.push(locations[i]);
            }
        }
    

        initialState.locations = [ ...newLocations]
        initialState.currentLocation = locations[locations.length -1]
        console.log('setting location state current length', locationState.locations.length);
        console.log('setting location state adding length', newLocations.length);
        //setTmp('JSON.stringify(locationState)');
        setLocationState( {...locationState, locations: [ ...newLocations], currentLocation: locations[locations.length -1]})
        //console.log('setting location state next length', locationState.locations.length)
    }


    console.log('useBgLocation hook running2 recoriding: ',  locationState.recording);

    let shouldTrack = locationState.recording;


    const startRecording = ()=>{
        //initialState.recording = true;
        console.log('start recording called')
        setLocationState({...locationState, description: "",  recording : true, locations:[]});
    }

    console.log('useBgLocation hook running3');

    const stopRecording = ()=>{
        //initialState.recording = false;
        console.log('stop recording called')
        setLocationState({...locationState, recording : false});
    }

    const setDescription = (description)=>{
        //initialState.name = name;
        setLocationState({...locationState, description : description});
    }

    const reset = ()=>{
        //initialState.name = name;
        setLocationState({...locationState, description : '', currentLocation: null, locations:[]});
    }

    const getCurrentLocation = ()=>{
        console.log('get current location called')
        BackgroundGeolocation.getCurrentLocation( (location)=>{
            console.log('get current locatio', location)
            setLocationState({...locationState, currentLocation : {
                coords: {
                    latitude: location.latitude,
                    longitude: location.longitude,
                    accuracy: location.accuracy
                },
                insertTime: new Date(),
                timestamp: location.time

            }});
        }, (error)=>{
            console.log('location error', error)
        },{
            timeout: 15000,
            enableHighAccuracy: false
        })
    }



    console.log('useBgLocation hook running4');

    
  
    useEffect ( ()=> {

        console.log('useBgLocation hook running5');
     
        if (shouldTrack){
            console.log('state changed start' ,shouldTrack );
            BackgroundGeolocation.start(); //triggers start on start event*/



           /* BackgroundGeolocation.checkStatus(status => {
            console.log('[INFO] BackgroundGeolocation service is running', status.isRunning);
            console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled);
            console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization);
            if (!status.isRunning) {
                BackgroundGeolocation.start(); //triggers start on start event
           
            }
            });*/
        }
        

        if (!shouldTrack ){

            BackgroundGeolocation.stop();
            console.log('deleting all locations ' );
            //BackgroundGeolocation.deleteAllLocations();

          /*  BackgroundGeolocation.checkStatus(status => {
            console.log('[INFO] BackgroundGeolocation service is running', status.isRunning);
            console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled);
            console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization);

            if (status.isRunning) {
                BackgroundGeolocation.stop();
                console.log('deleting all locations ' );
                BackgroundGeolocation.deleteAllLocations();
            }
            });*/

        }



    }, [ shouldTrack]);


 
    return [err, locationState, startRecording, stopRecording, setDescription,reset, getCurrentLocation]
}