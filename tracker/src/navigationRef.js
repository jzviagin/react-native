import {NavigationActions} from 'react-navigation';

let navigator;
export const setNavigator = (nav) => {
    navigator = nav;
}


export const navigate = (routeName, params) =>{
    console.log('navigate' , navigator)
    navigator.dispatch(
        NavigationActions.navigate({
            routeName: routeName,
            params: params
        })
    )
}

export const back = () =>{
    console.log('going back' , navigator)
    navigator.dispatch(
        NavigationActions.back()
    )
}