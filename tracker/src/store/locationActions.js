

export const startRecording =  ()=>{
    return {
        type: 'start_recording'
    };


}




export const stopRecording =  ()=>{
    return {
        type: 'stop_recording'
    }
}

export const addLocations =  (locations)=>{
    return {
            type: 'add_locations',
            locations: locations
        }
    
}


export const setState = (state)=>{
        return {
            type: 'set_state',
            state: state
        }
}

/*const addAproxLocation = dispatch => (location)=>{
    dispatch(
        {
            type: 'add_aprox_location',
            location: location
        }
    )
}*/

export const setDescription =  (description)=>{
    return {
            type: 'set_description',
            description: description        }
}

export const reset =  ()=>{
    return {
        type: 'reset'
    }
}