import { getPreciseDistance } from 'geolib';

export default  locationReducer = (state = {name: "", recording: false, locations : [], currentLocation: null}, action) => {
    //console.log('location reducer')
    switch(action.type){
        case 'start_recording':
            //console.log('start recording')
            return {...state, recording: true, locations:[]}
        case 'stop_recording':
            return {...state, recording: false}

        case 'add_locations':
                //console.log('currect locations', state.locations.length);
                if (state.recording){

                    //let newLocations = [];
                    //for (let i = 0; i < 100 ; i++){
                    //    newLocations.push(action.location);
                   // }
                   /* if (state.locations.length === 0){
                        for (let i = 0 ; i < 10000; i++){
                            action.locations = [...action.locations, action.locations[action.locations.length -1] ];
                        }
                    }*/

                    let newLocations = [...state.locations];
                    for (let i = 0; i < action.locations.length ; i++){
                        //console.log('adding new locations: ', newLocations.length);
                        if (newLocations.length != 0){
                            const distance = getPreciseDistance({
                                latitude: newLocations[newLocations.length -1 ].coords.latitude,
                                longitude: newLocations[newLocations.length -1].coords.longitude
                            }, 
                            {
                                latitude: action.locations[i].coords.latitude,
                                longitude: action.locations[i].coords.longitude
                            });
                            //console.log('distance', distance);
                            //console.log('accuracy', action.locations[i].coords.accuracy)
                            if (true/*distance >10 &&  action.locations[i].coords.accuracy < 12*/){
                                //console.log('meets creteria');
                                newLocations.push(action.locations[i]); 
                            }else{
                                //console.log('not meets creteria');
                            }
                        }else{
                            //console.log('1st location');
                            newLocations.push(action.locations[i]);
                        }
                    }

                    //console.log('state locations', state.locations)

                   
                    return {...state, locations: [...newLocations], currentLocation: action.locations[action.locations.length -1]}

                }else{
                    return state;
                }

      /*  case 'add_aprox_location':
            if (state.recording){
                return {...state, currentLocation: action.location}

            }else{
                return state;
            }*/
        case 'set_description':
            //console.log(action.name);
            return {...state, description: action.description}

        case 'set_state':
            return action.state;

        case 'reset':
            return {...state, locations: [], name: ''}
    

        default:
            return state;
    }
}

