import React, {useContext,useEffect, useCallback} from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import SigninScreen from './src/screens/SigninScreen'
import SignupScreen from './src/screens/SignupScreen'
import SearchScreen from './src/screens/SearchScreen'
import FeedScreen from './src/screens/FeedScreen'
import TrackCreateScreen from './src/screens/TrackCreateScreen'
import TrackDetailScreen from './src/screens/TrackDetailScreen'
import ResolveAuthScreen from './src/screens/ResolveAuthScreen'
import ProfileEditScreen from './src/screens/ProfileEditScreen'
import UserScreen from './src/screens/UserScreen'
import TrackListScreen from './src/screens/TrackListScreen'
import { createAppContainer,  createSwitchNavigator } from 'react-navigation'
import {createStackNavigator } from 'react-navigation-stack'
import {createBottomTabNavigator } from 'react-navigation-tabs'
import AccountScreen from './src/screens/AccountScreen';
import {Provider as AuthProvider, Context as AuthContext} from './src/context/authContext'
import {Provider as BgLocationProvider, Context as BgLocationContext} from './src/context/bgLocationContext'
import {Provider as MainProvider, Context as MainContext} from './src/context/mainContext'
import {setNavigator} from './src/navigationRef'
import { FontAwesome } from '@expo/vector-icons'; 


let loadedItems = 0

class ListItem extends React.Component{
  componentDidMount(){
    console.log('components did mount', loadedItems)
    loadedItems++
  }

  componentWillUnmount(){
    //console.log('components did unmount')
    loadedItems--;
  }
  render(){
    return <Text>{this.props.number}</Text>
  }
}


class App5 extends React.Component{

  constructor(){
    super();
    const data = [];
    for (let i = 0 ; i < 10000; i++){
      data.push(i)
    }

    this.state = {
      data: data
    }

  }

  componentDidUpdate(prev){
    console.log('componentDidUpdate', prev)
  }


  render(){


    return (

      <FlatList
      maxToRenderPerBatch = {1}
      windowSize ={3}
      initialNumToRender = {50}
                refreshing = {false}
            
                onRefresh = {()=>{
                }}
                data = {this.state.data}
                initialNumToRender = {1}
                keyExtractor = {item => { return item}}
                renderItem = {
                    (item) => {

                      return <ListItem number = {item.item}/>
                       
                    }
                }
                >


            
            </FlatList>
    )
  }
}






const iconTrack = <FontAwesome name="th-list" size={24} color="black" />

const iconFeed = <FontAwesome name="feed" size={24} color="black" />

const iconSearch = <FontAwesome name="search" size={24} color="black" />

const iconAccount = <FontAwesome name="user" size={24} color="black" />

const stackNavigatorTrack = createStackNavigator({
  TrackList: TrackListScreen,
  TrackDetails: TrackDetailScreen
})

const feedScreen = (props)=>{
  return <FeedScreen setTabPressHandler = { (func)=> {feedTabPressed = func}}/>
}

feedScreen.navigationOptions = {
  tabBarIcon: iconFeed,
  header: null,
  headerShown: false,
 
}


const stackNavigatorFeed = createStackNavigator({
  Feed: feedScreen,
  User: UserScreen
})

const stackNavigatorSearch = createStackNavigator({
  Search: SearchScreen,
  User: UserScreen
})

const stackNavigatorAccount = createStackNavigator({
  Account: AccountScreen,
  EditProfile: ProfileEditScreen,
  User: UserScreen
})

stackNavigatorTrack.navigationOptions = {
  title: 'Tracks',
  tabBarIcon: iconTrack,
  header: null,
  headerShown: false
}

let feedTabPressed;
stackNavigatorFeed.navigationOptions = {
  title: 'Feed',
  tabBarIcon: iconFeed,
  header: null,
  headerShown: false,
  tabBarOnPress: ({navigation, defaultHandler}) => { 
    feedTabPressed();
    defaultHandler();
  }
}


stackNavigatorSearch.navigationOptions = {
  title: 'Search',
  tabBarIcon: iconSearch,
  header: null,
  headerShown: false
}

stackNavigatorAccount.navigationOptions = {
  title: 'Account',
  tabBarIcon: iconAccount,
  header: null,
  headerShown: false
}



const switchNavigator = createSwitchNavigator({
  resolveAuth: ResolveAuthScreen,
  loginFlow:  createStackNavigator({
    Signup: SignupScreen,
    Signin: SigninScreen
  }),
  mainFlow: createBottomTabNavigator({
    FeedFlow: stackNavigatorFeed,
    SearchFlow: stackNavigatorSearch,
   // TrackListFlow: stackNavigatorTrack,
    TrackCreate: TrackCreateScreen,
    AccountFlow: stackNavigatorAccount

  },
  {
    tabBarOptions: {
      activeBackgroundColor: 'grey',
      inactiveBackgroundColor: 'white',
      activeTintColor: 'white', // tab text color
      inactiveTintColor: 'black',
      // This messes up the bottom bar
      tabStyle: {
        paddingTop: 40,
      },
      style: {
        height: 80,
      },
      labelStyle: {
        marginTop: 20,
        marginBottom: 20,
      },
    },
  }

  
  )
});




const App = createAppContainer(switchNavigator);






const AppWrapper =  (props)=>{
  console.log('AppWrapper rendering');
  return <App ref = { (navigator) => {setNavigator(navigator)}}/>
};





export default () => {
 // return <App5></App5>

  console.log('App rendering');
  return (
    <MainProvider>
      <BgLocationProvider>
        <AuthProvider>
          <AppWrapper/>
        </AuthProvider>
      </BgLocationProvider>
    </MainProvider>
    
   
    
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
