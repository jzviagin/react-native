const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth')

const db = require ('../db/db')

const Author = mongoose.model('Author');


const Publisher = mongoose.model('Publisher');




router.use(requireAuth);





router.post('/authors', async (req, res) =>{

    const {name} = req.body;

    if( !req.user || !req.user.admin){
        res.status(422).send('you must be logged in as admin');
        return;
    }
    
    if (!name || name.length === 0  ){
        res.status(422).send('Please provide a valid name');
        return;
    }
    
   
    
    
    try{
        ret = await db.createAuthor(name);
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});

router.post('/publishers', async (req, res) =>{

    const {name} = req.body;

    if( !req.user || !req.user.admin){
        res.status(422).send('you must be logged in as admin');
        return;
    }
    
    if (!name || name.length === 0  ){
        res.status(422).send('Please provide a valid name');
        return;
    }
    
   
    
    
    try{
        ret = await db.createPublisher(name);
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    
    

});

router.get('/publishers', async (req, res) =>{




 
    
    
    try{
        const ret = await db.getPublishers()
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});


router.get('/authors', async (req, res) =>{




 
    
    
    try{
        const ret = await db.getAuthors()
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});



router.get('/books/all', async (req, res) =>{


  

  

    
    if( !req.user ){
        res.status(422).send('you must be logged in as admin');
        return;
    }

 
    
    
    try{

        const ret = await db.getBooks(null, req.user._id, true)
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});


router.get('/books/:id', async (req, res) =>{


  

    console.log('get book by id ', req.params)

    
    if( !req.user ){
        res.status(422).send('you must be logged in as admin');
        return;
    }

 
    
    
    try{

        const ret = await db.getBook(req.params.id, req.user._id)
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});

router.get('/books', async (req, res) =>{


    let book = req.query.book;

    console.log('get books', book)

    
    if( !req.user ){
        res.status(422).send('you must be logged in as admin');
        return;
    }

 
    
    
    try{

        const ret = await db.getBooks(book, req.user._id)
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});



router.delete('/books', async (req, res) =>{



    console.log('delete', req.headers)
    const {bookid} = req.headers;

    if( !req.user || !req.user.admin){
        res.status(422).send('you must be logged in as admin');
        return;
    }

    if (!bookid || bookid.length === 0  ){
        res.status(422).send('Please provide a valid bookId');
        return;
    }
 
    
    
    try{
        const ret = await db.deleteBook(bookid)
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    
    

});


router.post('/books', async (req, res) =>{




    const {name, authorId, publisherId} = req.body;

    if( !req.user || !req.user.admin){
        res.status(422).send('you must be logged in as admin');
        return;
    }
    
    if (!name || name.length === 0  ){
        res.status(422).send('Please provide a valid name');
        return;
    }

    if (!authorId || authorId.length === 0  ){
        res.status(422).send('Please provide a valid author');
        return;
    }

    if (!publisherId || publisherId.length === 0  ){
        res.status(422).send('Please provide a valid publisher');
        return;
    }
    
   
    
    
    try{
        ret = await db.createBook(name, authorId, publisherId);
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    

});



router.post('/purchases', async (req, res) =>{




    const {bookId} = req.body;

    if( !req.user ){
        res.status(422).send('you must be logged in ');
        return;
    }
    
    if (!bookId || bookId.length === 0  ){
        res.status(422).send('Please provide a valid book id');
        return;
    }

  
   
    
    
    try{
        ret = await db.createPurchase(req.user._id, bookId);
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    

});

router.put('/books', async (req, res) =>{




    const {_id, name, authorId, publisherId} = req.body;
    console.log('update book', req.body)

    if( !req.user || !req.user.admin){
        res.status(422).send('you must be logged in as admin');
        return;
    }

    if (!_id || _id.length === 0  ){
        res.status(422).send('Please provide a valid bookId');
        return;
    }

    
    if (!name || name.length === 0  ){
        res.status(422).send('Please provide a valid name');
        return;
    }

    if (!authorId || authorId.length === 0  ){
        res.status(422).send('Please provide a valid author');
        return;
    }

    if (!publisherId || publisherId.length === 0  ){
        res.status(422).send('Please provide a valid publisher');
        return;
    }
    
   
    
    
    try{
        ret = await db.updateBook(_id, name, authorId, publisherId);
        res.send(ret);
    }catch (err){
        res.status(422).send(err);
    }
    
    

});


























module.exports = router;
