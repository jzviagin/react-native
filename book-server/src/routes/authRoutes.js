const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const EventEmitter = require('events');



const allowAuth = require('../middlewares/allowAuth')

router.use (allowAuth)

const User = mongoose.model('User');





router.post('/signup', async (req, res) =>{
    console.log(req.body);
    console.log('sign up')

    const {username, password} = req.body;

    if (!username || username.length === 0  ){
        res.status(422).send('Please provide a valid username');
        return;
    }

    if (!password || password.length === 0  ){
        res.status(422).send('Please provide a valid password');
        return;
    }

    let existingUser = await User.findOne( {username: username});
    if(existingUser){
        res.status(422).send("username is already registered");
        return;
    }

  

   
    try{
        const user = new User({ username, password, admin:false});
        await user.save();
        console.log('created user', user)
        const token = jwt.sign({userId: user._id}, 'MY_SECRET_KEY');
        res.cookie('token', token)
        res.send({user:user/*,
        token:token*/});
    }catch (err){
        console.log(err)
        res.status(422).send(err.message);
    }

});

/*router.post('/signupAdmin', async (req, res) =>{
    console.log(req.body);

    const {username, password} = req.body;

    if (!username || username.length === 0  ){
        res.status(422).send('Please provide a valid user name');
        return;
    }

    if (!password || password.length === 0  ){
        res.status(422).send('Please provide a valid password');
        return;
    }

    let existingUser = await Admin.findOne( {username: username});
    if(existingUser){
        res.status(422).send("username is already registered");
        return;
    }

  

   
    try{
        const admin = new Admin({ username, password});
        await admin.save();
        const token = jwt.sign({userId: user._id}, 'MY_SECRET_KEY');
        res.send({admin:admin,
            token:token});
    }catch (err){
        res.status(422).send(err.message);
    }

});*/

router.get('/login', async (req, res) =>{
    console.log(req.headers);


    try{
        console.log('login user', req.user);
        const foundUser = await User.findOne( {_id: req.user._id});
    
        if(!foundUser){
            console.log('user not found');
        }else{
            const token = jwt.sign({userId: foundUser._id}, 'MY_SECRET_KEY'); 
            console.log('login token', token);
            res.cookie('token', token)
            res.send ({user:foundUser/*, token:token*/}); 
            return;  
        }
     
    }catch (err){
        console.log('login err', err);
    }



    const {username, password} = req.headers;

    if (!username || username.length === 0 ){
        res.status(422).send('Please provide a valid username');
    }

    try{
        const user = await User.findOne( {username: username});
        if(!user){
            res.status(422).send("username is not registered");
        }
        user.comparePassword(password).then( (success)=>{
            if (success === true){
                const token = jwt.sign({userId: user._id}, 'MY_SECRET_KEY'); 
                res.cookie('token', token)
                res.send({user: user/*, token:token*/});
                return;
            }
            res.status(422).send("wrong password");

        }).catch( (err) => {
            res.status(422).send(err);
        })

        
    }catch (err){
        res.status(422).send(err.message);
    }



    

});

/*router.post('/loginAdmin', async (req, res) =>{
    console.log(req.body);
    const {username, password} = req.body;

    if (!username || email.length === 0 ){
        res.status(422).send('Please provide a valid username');
    }

    try{
        const admin = await Admin.findOne( {username: username});
        if(!admin){
            res.status(422).send("userName not registered");
        }
        admin.comparePassword(password).then( (success)=>{
            if (success === true){
                const token = jwt.sign({adminId: admin._id}, 'MY_SECRET_KEY'); 
                res.send({admin: admin, token:token});
                return;
            }
            res.status(422).send("wrong password");

        }).catch( (err) => {
            res.status(422).send(err);
        })

        
    }catch (err){
        res.status(422).send(err.message);
    }



    

});*/



router.get('/login', async (req, res) =>{
    console.log(req.body);


    console.log('login');

    try{
        console.log('login user', req.user);
        const user = await User.findOne( {_id: req.user._id});
    
        if(!user){
            console.log('user not found');
            res.status(422).send("user not registered");
            return;
        }
        const token = jwt.sign({userId: user._id}, 'MY_SECRET_KEY'); 
        console.log('login token', token);
        res.cookie('token', token)
        res.send ({user:user/*, token:token*/});        
    }catch (err){
        console.log('login err', err);
        res.status(422).send(err.message);
    }



    

});


router.get('/', async (req, res) =>{
    res.cookie('ccc', 'yyyyy')
    res.send ({ttt:'user'/*, token:token*/});  
})


module.exports = router;