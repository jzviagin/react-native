const mongoose = require('mongoose');




const bookSchema = new mongoose.Schema({
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Author',
        required: true
    },
    publisherId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Publisher',
        required: true
    },
    name: {
        type: String,
        default: '',
        required: true
    }
});



mongoose.model('Book', bookSchema);
