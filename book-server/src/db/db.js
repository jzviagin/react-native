const mongoose = require('mongoose');
const User = mongoose.model('User');
const Book = mongoose.model('Book');
const Author = mongoose.model('Author');
const Publisher = mongoose.model('Publisher');
const Purchase = mongoose.model('Purchase');

module.exports = {
    
    getUser : async (userId)=>{
        const user = await User.findOne({_id: userId});

        
        if (user){
            let ret = {users: {[user._id]: user}}

           
            return ret;
        }
        throw 'not found'
    },

    getPublishers : async ()=>{
        const publishers = await Publisher.find();

        
        if (publishers){
            let ret = {publishers: {}}

            publishers.forEach((publisher)=>{
                ret.publishers[publisher._id] = publisher
            })
           
            return ret;
        }
        throw 'not found'
    },
    getAuthors : async ()=>{
        const authors = await Author.find();

        
        if (authors){
            let ret = {authors: {}}

            authors.forEach((author)=>{
                ret.authors[author._id] = author
            })
           
            return ret;
        }
        throw 'not found'
    },
    getBooks : async (book, userId, all)=>{


        let books;

        if (all){
            books = await Book.find()
        }else{

            if(!book || book.trim().length ==0){

                return {books: {}, authors: {}, publishers: {} , search: {books:[]}, purchases:{}};

            }

            books = await Book.find({name: { 
                "$regex": new RegExp(book.replace(/\s+/g,"\\s+"), "gi")
            }});

        }
         
        
        

        
        if (books){

            let authorIds = []
            let publisherIds = []

            books.forEach((book)=>{
                authorIds.push(book.authorId)
                publisherIds.push(book.publisherId)
            })

            let publishers = await Publisher.find({
                '_id': { $in: publisherIds}
            })

            let authors = await Author.find({
                '_id': { $in: authorIds}
            })


            const purchases = await Purchase.find({
                userId: userId
            })

            




            let ret = {books: {}, authors: {}, publishers: {} , search: {books:[]}, purchases:{}}

            books.forEach((book)=>{
                ret.books[book._id] = book
                ret.search.books.push(book._id);
            })

            purchases.forEach((purchase)=>{
                ret.purchases[purchase.bookId] = purchase
                
            })
            publishers.forEach((publisher)=>{
                ret.publishers[publisher._id] = publisher
            })
            authors.forEach((author)=>{
                ret.authors[author._id] = author
            })
            console.log('ret', ret)
           
            return ret;
        }
        throw 'not found'
    },

    getBook : async (id, userId)=>{

        console.log('find book', id)
    
        books = await Book.find({_id: id})
       
        
        console.log('find book', books)

        
        if (books){

            let authorIds = []
            let publisherIds = []

            books.forEach((book)=>{
                authorIds.push(book.authorId)
                publisherIds.push(book.publisherId)
            })

            let publishers = await Publisher.find({
                '_id': { $in: publisherIds}
            })

            let authors = await Author.find({
                '_id': { $in: authorIds}
            })


            const purchases = await Purchase.find({
                userId: userId
            })

            




            let ret = {books: {}, authors: {}, publishers: {} , search: {books:[]}, purchases:{}}

            books.forEach((book)=>{
                ret.books[book._id] = book
                ret.search.books.push(book._id);
            })

            purchases.forEach((purchase)=>{
                ret.purchases[purchase.bookId] = purchase
                
            })
            publishers.forEach((publisher)=>{
                ret.publishers[publisher._id] = publisher
            })
            authors.forEach((author)=>{
                ret.authors[author._id] = author
            })
            console.log('ret', ret)
           
            return ret;
        }
        throw 'not found'
    },
    createAuthor : async (name)=>{
      
            const author = new Author({ name});
            await author.save();
            return {authors: {[author._id]:author}}

        
    },
    createPublisher : async (name)=>{
      
        const publisher = new Publisher({ name});
        await publisher.save();
        return {publishers: {[publisher._id]:publisher}}

    
    },
    createBook : async (name, authorId, publisherId)=>{

        console.log('create book')
        const author = await Author.findOne({_id: authorId})

        if (!author){
            throw ('please provide a legal author id')
        }

        console.log('author found', author)


        const publisher = await Publisher.findOne({_id: publisherId})

        if (!publisher){
            throw ('please provide a legal publisher id')
        }

        console.log('pub found', publisher)

      
        const book = new Book({ name: name, authorId:author._id, publisherId: publisher._id});
        await book.save();
        return {books: {[book._id]:book}}

    
    },
    createPurchase : async (userId, bookId)=>{

        console.log('create purchase')



        const book = await Book.findOne({_id: bookId})

        if (!book){
            throw ('please provide a legal book id')
        }

        console.log('pub found', book)

      
        const purchase = new Purchase({ userId: userId, bookId: book._id});
        await purchase.save();
        return {purchases: {[purchase.bookId]:purchase}}

    
    },
    updateBook : async (bookId, name, authorId, publisherId)=>{

        console.log('update book')


        const book = await Book.findOne({_id: bookId})

        if (!book){
            throw ('please provide a legal book id')
        }

        console.log('book found', book)

        const author = await Author.findOne({_id: authorId})

        if (!author){
            throw ('please provide a legal author id')
        }

        console.log('author found', author)


        const publisher = await Publisher.findOne({_id: publisherId})

        if (!publisher){
            throw ('please provide a legal publisher id')
        }

        console.log('pub found', publisher)

      
        book.name = name;
        book.publisherId = publisher._id
        book.authorId = author._id
        await book.save();
        return {books: {[book._id]:book}}

    
    },    
    deleteBook : async (bookId)=>{

        console.log('delete book')


        const book = await Book.findOne({_id: bookId})

        if (!book){
            throw ('please provide a legal book id')
        }

        await Book.deleteOne({_id: bookId})
        return {}

       

    
    }


 



 
    
    

}
