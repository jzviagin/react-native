require('./models/User')
require('./models/Admin')
require('./models/Author')
require('./models/Book')
require('./models/Publisher')
require('./models/Purchase')
const express = require('express');
const app = express();
const requireAuth = require('./middlewares/requireAuth')
const mongoose = require('mongoose')
const bookRoutes = require('./routes/bookRoutes');
const authRoutes = require('./routes/authRoutes');


const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');





const cors = require('cors')




//app.use(cors());
app.use(cors({credentials: true, origin: true}));
app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true, parameterLimit: 100000 }));
app.use(cookieParser())


app.use(authRoutes)


app.use(bookRoutes);



/*app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '127.0.0.1');
    res.header('Access-Control-Allow-Credential', true);
    res.header('Access-Control-Allow-Headers','*')
    next();
  });*/

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", req.header('Origin'));
    res.header("Access-Control-Allow-Credentials", true);
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
  });

const mongoUri = 'mongodb+srv://admin:manunited99@cluster0-7vcid.mongodb.net/BookStore?retryWrites=true&w=majority'

mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useCreateIndex: true
});

mongoose.connection.on('connected', ()=> {
    console.log('connected to mongo2222');
});

mongoose.connection.on('error', (err)=> {
    console.log('connection to mongo failed ' + err);
});




app.listen( 3002, ()=>{
    console.log('listening3');
})
