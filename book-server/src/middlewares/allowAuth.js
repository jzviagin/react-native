const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = (req, res, next) => {

    console.log('allow auth',req.url);
    //console.log('allow auth',req.cookies.token);
    const {authorization} = req.headers;
    if (!req.cookies){
        next();
        return;
    }
    if (!req.cookies.token){
        next();
        return;
    }
  
    //const token = authorization.replace('Bearer ', '');
    jwt.verify(req.cookies.token, 'MY_SECRET_KEY', async (err, payload) => {
        if (err) {
            return res.status(401).send({error: 'You must be logged in. '});
        }
        const {userId} = payload;
        const user = await User.findById(userId);
        req.user = user;
        next();
    })
};