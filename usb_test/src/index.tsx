import { Transports ,Reflow } from "@mcesystems/reflow";
import { renderDisplayLayer } from "@mcesystems/reflow-react-display-layer";

import DevicesView from "./View/byHeirarchy/DevicesView";
import DevicesViewClass from "./View/byClass/DevicesView";




console.log('main',typeof document.getElementById("main"))

const elem = document.getElementById("main")

const views: any = {
 
	DevicesViewClass,
  DevicesView,
};


console.log('views', views)


const transport = new Transports.WebSocketsTransport({ port: 3002, host: "localhost" });// the host can be changed if running the display container from another machine

renderDisplayLayer({
	element: elem? elem:new Element(),
	transport,
	views,
});