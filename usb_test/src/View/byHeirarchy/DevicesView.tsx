import MyViewInterface, { deviceType }   from "usb_view_interfaces/src/Views/DevicesView";
import { ReflowReactComponent } from "@mcesystems/reflow-react-display-layer";
import * as React from "react";
import Device from './Device'
import classes from './DevicesView.module.css'





// using ReflowReactComponent in this case provides the event() and done() callbacks.
class DevicesView extends ReflowReactComponent<MyViewInterface> {
	render() {
		const { devices, event, done } = this.props;
		return (
			<div className = {classes.MainDiv}>
				<button className = {classes.Button} onClick={()=>{
					done({})
				}}>View By class</button>
				<ul>
				{
					
					 devices.map((device: deviceType)=>{
						return <li key = {device.vendor + "_" + device.product}><Device device = {device}/></li>
					})
				}
				</ul>

			</div>
		);
	}
}

export default DevicesView;