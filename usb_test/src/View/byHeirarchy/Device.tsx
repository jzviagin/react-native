import MyViewInterface   from "usb_view_interfaces/src/Views/DevicesView";
import { ReflowReactComponent } from "@mcesystems/reflow-react-display-layer";
import * as React from "react";
import {deviceType} from 'usb_view_interfaces'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {  faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'
import classes from "./Device.module.css";





class Device extends React.Component<{device:deviceType}, {children:boolean}> {

	constructor(props:{device:deviceType}){
		super(props)
		this.state = {
			children:false
		}
		
	}
	render() {
		const { device } = this.props;
		return (
			<div>
				<div className = {classes.MainDiv} >
					<h1>type: {device.class == 9? 'hub':'device'}</h1>
					<h1>class: { '0x' + ('0000' + device['class'].toString(16).toUpperCase()).substr(-4)}</h1>
					<h1>vendorId: { '0x' + ('0000' + device.vendor.toString(16).toUpperCase()).substr(-4)}</h1>
					<h1>productId: { '0x' + ('0000' + device.product.toString(16).toUpperCase()).substr(-4)}</h1>

					{device.children.length != 0 ?<button className = {classes.PlusButton} onClick = {()=>{
                      this.setState({children: !this.state.children})
					}}><FontAwesomeIcon className = {classes.PlusIcon} width = {50}  icon={this.state.children?faMinus:faPlus} />
					
					</button>:null}

					
				</div>
				{this.state.children? <ul>
				{
					
					device.children.map(device=>{
						return <li key = {device.vendor + "_" + device.product}><Device device = {device}/></li>
					})
				}
				</ul>: null}
				

			</div>
			
		);
	}
}

export default Device;