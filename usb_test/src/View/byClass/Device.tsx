import MyViewInterface   from "usb_view_interfaces/src/Views/DevicesView";
import { ReflowReactComponent } from "@mcesystems/reflow-react-display-layer";
import * as React from "react";
import {deviceTypeClass} from 'usb_view_interfaces'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {  faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'

import classes from "./Device.module.css";



class Device extends React.Component<{device:deviceTypeClass}> {

	constructor(props:{device:deviceTypeClass}){
		super(props)
	
	}
	render() {
		const { device } = this.props;
		return (
			<div>
				<div  className = {classes.MainDiv}>
					<h1>vendorId: { '0x' + ('0000' + device.vendor.toString(16).toUpperCase()).substr(-4)}</h1>
					<h1>productId: { '0x' + ('0000' + device.product.toString(16).toUpperCase()).substr(-4)}</h1>



				</div>
				
				

			</div>
			
		);
	}
}

export default Device;