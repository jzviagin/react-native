import MyViewInterface   from "usb_view_interfaces/src/Views/DevicesView";
import { ReflowReactComponent } from "@mcesystems/reflow-react-display-layer";
import * as React from "react";
import {deviceTypeClass} from 'usb_view_interfaces'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {  faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'
import Device from "./Device";

import classes from "./DeviceGroup.module.css";





class DeviceGroup extends React.Component<{devices:deviceTypeClass[], classNum:number}, {children:boolean}> {

	constructor(props:{devices:deviceTypeClass[], classNum:number}){
		super(props)
		this.state = {
			children:false
		}
		
	}
	render() {
		const { devices } = this.props;
		return (
			<div>
				<div className = {classes.MainDiv} >
					<h1>class: { '0x' + ('0000' + this.props.classNum.toString(16).toUpperCase()).substr(-4)}</h1>
				

					<button  className = {classes.PlusButton} onClick = {()=>{
                      this.setState({children: !this.state.children})
					}}><FontAwesomeIcon className = {classes.PlusIcon} width = {50}  icon={this.state.children?faMinus:faPlus} />
					
					</button>

					
				</div>
				{this.state.children? <ul>
				{
					
					devices.map(device=>{
						return <li key = {device.vendor + "_" + device.product}><Device device = {device}/></li>
					})
				}
				</ul>: null}
				

			</div>
			
		);
	}
}

export default DeviceGroup;