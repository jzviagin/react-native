import MyViewInterface, { devicesByClass, deviceTypeClass }   from "usb_view_interfaces/src/Views/DevicesViewClass";
import { ReflowReactComponent } from "@mcesystems/reflow-react-display-layer";
import * as React from "react";
import DeviceGroup from "./DeviceGroup";
import classes from './DevicesView.module.css'





class DevicesView extends ReflowReactComponent<MyViewInterface> {
	render() {
		const { devices, event, done } = this.props;
		Object.keys(devices).map(key=>{
			return key
		})
		return (
			<div className = {classes.MainDiv}>
				<button className = {classes.Button} onClick={()=>{
					done({})
				}}>View By Heirarchy</button>
				<ul>
				{
					
					Object.keys(devices).map(key=>{
						return <li key = {key}><DeviceGroup devices = {devices[+key]} classNum = {+key}/></li>
					})
					
				}
				</ul>

			</div>
		);
	}
}

export default DevicesView;