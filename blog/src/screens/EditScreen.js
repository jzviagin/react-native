import React, {useContext, useState} from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, TextInput } from "react-native";
import {Context} from '../context/BlogContext'
import { withNavigation } from 'react-navigation';
import BlogPostForm from '../components/BlogPostForm'

const EditScreen = (props) => {
  const {state, addBlogPost, deleteBlogPost, updateBlogPost} = useContext(Context);
 


  const blogId = props.navigation.getParam('id');
  const blog = state.find( (elem) => (elem.id===blogId));
  return <View>

      <BlogPostForm title= {blog.title} body={blog.text} onSubmit= {(title, body)=>{
              updateBlogPost(blogId,title, body, ()=> {props.navigation.pop()})
            }}/>
      
      
    </View>;
};

const styles = StyleSheet.create({
 
});

export default withNavigation(EditScreen);
