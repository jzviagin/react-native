import React, {useContext} from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";
import {Context} from '../context/BlogContext'
import { EvilIcons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';
const ShowScreen = (props) => {
  const {state, addBlogPost, deleteBlogPost} = useContext(Context);
 
  const blogId = props.navigation.getParam('id');
  const blog = state.find( (elem) => (elem.id===blogId));
  return <View>
      <Text style={styles.text}>{blog.title} </Text>
      <Text style={styles.text}>{blog.text} </Text>
      
    </View>;
};

ShowScreen.navigationOptions = (props) =>{
    const blogId = props.navigation.getParam('id');
    return {
      headerRight: ()=><TouchableOpacity onPress= {()=> {props.navigation.navigate('Edit', {id: blogId})}}><EvilIcons style = {styles.listElementIcon} name="pencil"  size={30} color="black" /></TouchableOpacity>
      
    }
  }

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default  withNavigation(ShowScreen);
