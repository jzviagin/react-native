import React, {useContext, useReducer, useEffect} from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, FlatList } from "react-native";
import {Context} from '../context/BlogContext'
import { Feather } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';




const IndexScreen = (props) => {
  const {state, addBlogPost, deleteBlogPost, getBlogPosts} = useContext(Context);
  useEffect(()=>{
    getBlogPosts();
  }, []);
  console.log(state);
  return <View>

      <FlatList data = {state} 
                renderItem = {
                    (elem) => {
                    return (
                      <View >
                        <TouchableOpacity style = {styles.listElement} onPress= {()=>{
                          props.navigation.navigate('Show', {id: elem.item.id})
                        }}>
                              <Text  style = {styles.listElementText}>{elem.item.title} </Text>
                              <TouchableOpacity onPress = { ()=>{
                                  deleteBlogPost(elem.item.id)
                              }}>
                                <Feather style = {styles.listElementIcon} name="trash"  color="black" />
                              </TouchableOpacity>
                        </TouchableOpacity>
                       
                        
                      </View>
                      
                    );
                    }
                }
                keyExtractor={(item, index) => ("blog" + item.id)}
                
                showsHorizontalScrollIndicator= {false}
                />

                
       
      
    </View>;
};

IndexScreen.navigationOptions = (props) =>{
  return {
    headerRight: ()=><TouchableOpacity onPress= {()=> {props.navigation.navigate('Create')}}><Feather style = {styles.listElementIcon} name="plus"  size={30} color="black" /></TouchableOpacity>
    
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  },
  listElement:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingVertical: 20,
    paddingHorizontal: 10,
    borderTopWidth: 1,
    borderColor: 'grey'
  },
  listElementText:{
    fontSize: 18
  },
  listElementIcon:{
    fontSize: 24
  }
});

export default  withNavigation(IndexScreen);
