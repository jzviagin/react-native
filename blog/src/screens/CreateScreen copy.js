import React, {useContext, useState} from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, TextInput } from "react-native";
import {Context} from '../context/BlogContext'
import { withNavigation } from 'react-navigation';


const CreateScreen = (props) => {
  const {state, addBlogPost, deleteBlogPost} = useContext(Context);
 
  const [title, setTitle] = useState();
  const [body, setBody] = useState();
  return <View>
      <Text  style={styles.text}>Title:</Text>
      <TextInput placeholder="title" style={styles.input} onChangeText= {text =>{ 
        console.log(text)
        setTitle(text)
        }}/>
      <Text  style={styles.text}>Content:</Text>
      <TextInput placeholder="content" style={styles.input} onChangeText= {text =>{ 
        setBody(text)
        }}/> 
      <Button title = 'Save' onPress={  ()=>{
        addBlogPost(title, body, ()=> {props.navigation.goBack()}) }}></Button>
      
    </View>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    marginBottom: 15,
    padding: 5,
    marginLeft: 5,
    marginRight: 5
  },
  input: {
    fontSize:18,
    borderWidth: 1,
    borderColor: 'black',
    marginLeft: 5,
    marginRight: 5,
    padding: 2
  }
});

export default withNavigation(CreateScreen);
