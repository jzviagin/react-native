import React, {useContext, useState} from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, TextInput } from "react-native";
import {Context} from '../context/BlogContext'
import { withNavigation } from 'react-navigation';
import BlogPostForm from '../components/BlogPostForm'


const CreateScreen = (props) => {
  const {state, addBlogPost, deleteBlogPost} = useContext(Context);
 

  return <View>

      <BlogPostForm  onSubmit= {(title, body)=>{
        addBlogPost(title, body, ()=> {props.navigation.goBack()})
      }}/>
     
      
    </View>;
};

const styles = StyleSheet.create({
});

export default withNavigation(CreateScreen);
