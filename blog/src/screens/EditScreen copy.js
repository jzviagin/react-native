import React, {useContext, useState} from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, TextInput } from "react-native";
import {Context} from '../context/BlogContext'
import { withNavigation } from 'react-navigation';


const EditScreen = (props) => {
  const {state, addBlogPost, deleteBlogPost, updateBlogPost} = useContext(Context);
 


  const blogId = props.navigation.getParam('id');
  const blog = state.find( (elem) => (elem.id===blogId));
  const [title, setTitle] = useState(blog.title);
  const [body, setBody] = useState(blog.text);
  return <View>



      <Text  style={styles.text}>Title:</Text>
      <TextInput value={title} placeholder="title" style={styles.input} onChangeText= {text =>{ 
        console.log(text)
        setTitle(text)
        }}/>
      <Text  style={styles.text}>Content:</Text>
      <TextInput value={body} placeholder="content" style={styles.input} onChangeText= {text =>{ 
        setBody(text)
        }}/> 
      <Button title = 'Save' onPress={  ()=>{
        updateBlogPost(blogId,title, body, ()=> {props.navigation.navigate('Index')}) }}></Button>
      
    </View>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    marginBottom: 15,
    padding: 5,
    marginLeft: 5,
    marginRight: 5
  },
  input: {
    fontSize:18,
    borderWidth: 1,
    borderColor: 'black',
    marginLeft: 5,
    marginRight: 5,
    padding: 2
  }
});

export default withNavigation(EditScreen);
