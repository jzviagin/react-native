import React, {useReducer} from 'react'

export default (reducer, actionPrototypes, initialState) => {
    const Context= React.createContext();
    const Provider = (props)=> {

        const [state, dispatch] = useReducer(reducer,initialState);
        let actions = {};
        for (action in actionPrototypes){
            actions = {
                ...actions,
                [action]: actionPrototypes[action](dispatch)
            }
           
        }

        return <Context.Provider value = {{state, ...actions}}>{props.children}</Context.Provider>
    }
    
    return {Context, Provider}

}



