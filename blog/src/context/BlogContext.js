import React , {useReducer} from "react";
import jsonServer from '../api/jsonServer'

import createDataContext from './createDataContext'

//const BlogContext = React.createContext();



export const ACTION_ADD_POST = "ACTION_ADD_POST";
export const ACTION_DELETE_POST = "ACTION_DELETE_POST";
export const ACTION_UPDATE_POST = "ACTION_UPDATE_POST"
export const ACTION_SET_POSTS = "ACTION_SET_POSTS"
const reducer = (state, action)=>{
    console.log("type " + action.type);
    switch (action.type){
      case ACTION_ADD_POST:
          console.log("title " + action.title);
          console.log("body " + action.body);
         
        return [
          ...state,
          {
              id: action.id,//Math.floor(Math.random() * 99999),
              title: action.title/*`Blog Post #${state.length + 1}`*/,
              text: action.body /*'blog text bla bla'*/

            }
        ]

      case ACTION_DELETE_POST:
         console.log("delete index " + action.id);
         // const newState = [...state];
         // newState.splice(action.index, 1);
         // return newState;
         const newState = state.filter(elem => (elem.id !== action.id));
         return newState;
      case ACTION_UPDATE_POST:
        const newState2 = [...state];
        const post = newState2.find(elem => elem.id === action.id);
        post.title = action.title;
        post.text = action.body;
        return newState2;

      case ACTION_SET_POSTS:

        return action.posts;
    

     
      default:
        return state;
  
    }
  }

const addBlogPost = (dispatch) => {

        return async (title, body, callback)=>{
            try {
                let response = await jsonServer.post("/blogposts", 
                    {
                        //id: Math.floor(Math.random() * 99999),
                        title: title/*`Blog Post #${state.length + 1}`*/,
                        text: body /*'blog text bla bla'*/
        
                    }
                );
                console.log(response.data);
                dispatch({type: ACTION_ADD_POST, id:response.data.id, title:response.data.title, body:response.data.text});
                if (callback){
                    callback();
                }

               
                
              } catch(err) {
                //alert(err); // TypeError: failed to fetch
              }
           
           
        }
      
    }


const  getBlogPosts = (dispatch)=>{
    
    return async ()=>{
        try {
            let response = await jsonServer.get("/blogposts");
            console.log(response);
            dispatch({type: ACTION_SET_POSTS, posts: response.data});
          } catch(err) {
            //alert(err); // TypeError: failed to fetch
          }
       
       
    }
}

const deleteBlogPost = (dispatch) => {
    return async (id)=>{
       // dispatch({type: ACTION_DELETE_POST, id:id});

        try {
            let response = await jsonServer.delete("/blogposts/" + id);
            dispatch({type: ACTION_DELETE_POST, id:id}); 
          } catch(err) {
            //alert(err); // TypeError: failed to fetch
          }
       
       
    }
    
}

const updateBlogPost = (dispatch) => {
    return async (id, title, body, callback)=>{


        try {
            let response = await jsonServer.put("/blogposts/"+  id, 
                {
                    //id: Math.floor(Math.random() * 99999),
                    title: title/*`Blog Post #${state.length + 1}`*/,
                    text: body /*'blog text bla bla'*/
    
                }
            );
            dispatch({type: ACTION_UPDATE_POST, id:id, title:title, body:body});
            if (callback){
                callback();
            }

           
            
          } catch(err) {
            //alert(err); // TypeError: failed to fetch
          }

       
    }
    
}

export const {Context, Provider} = createDataContext(reducer, {addBlogPost,deleteBlogPost, updateBlogPost, getBlogPosts}, []);


//export const BlogProvider = (props)=> {

    /*const [blogPosts ,setBlogPosts] = useState([
        {
            title : 'Blog Post 1'
        },
        {
            title : 'Blog Post 2'
        }
    ]);*/

   // const [blogPosts, dispatch] = useReducer(reducer, [
   //     {
   //         title : 'Blog Post 1'
   //     },
   //     {
    //        title : 'Blog Post 2'
    //    }
   // ]);

    


   // const addBlogPost = () => {
       // setBlogPosts([...blogPosts,  {title: `Blog Post #${blogPosts.length + 1}`}])
    //   dispatch({type: ACTION_ADD_POST});
    //}

 //   return <BlogContext.Provider value = {{data:blogPosts, addBlogPost:addBlogPost}}>{props.children}</BlogContext.Provider>
//}

//export default BlogContext;
//export BlogProvider;