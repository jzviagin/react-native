import React, {useState, useEffect, useContext} from 'react';
import {AsyncStorage, View, StyleSheet, Text} from 'react-native'


import { LoginButton, AccessToken , GraphRequest, GraphRequestManager, LoginManager  } from 'react-native-fbsdk';
import {Context as AuthContext} from '../context/authContext'



import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import { setLightEstimationEnabled } from 'expo/build/AR';



GoogleSignin.configure({
scopes: [], 
offlineAccess: false, 
hostedDomain: '', 
loginHint: '', 
forceCodeForRefreshToken: true, 
accountName: '', 
});



export default ()=> {

    const {state,setUser,setLoading} = useContext(AuthContext);

    


    const fetchFbData = (id)=>{
        console.log('fetching fb data ', id)
        const responseInfoCallback = (error, result)=> {
            if (error) {
              console.log('Error fetching data: ' + error);
            } else {
              console.log( result);
              setUser({
                  name: result.name,
                  photo: result.picture.data.url
              })
            }
          }
          
          const infoRequest = new GraphRequest(
            '/'+id,
            {
             
               
                parameters: {
                    fields: {
                        string : 'name,picture.type(large)'
                    }
                }
            },
            responseInfoCallback,
          );
          // Start the graph request.
          new GraphRequestManager().addRequest(infoRequest).start();
    }


    const googleSignIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const data = await GoogleSignin.signIn();
            console.log(data);
            setUser( {
                name: data.user.name,
                photo: data.user.photo
            })
        } catch (error) {
            console.log('error', error.code);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
            } else {
            // some other error happened
            }
        }
    };

    useEffect ( ()=>{
        setLoading();
        console.log('checking if signed in to google')

        GoogleSignin.isSignedIn().then( (val)=>{
            console.log('is signed in to google', val);
            if (val){
                GoogleSignin.getCurrentUser().then( (data)=> {
                  console.log('google user', data.user);
                  setUser( {
                      name: data.user.name,
                      photo: data.user.photo
                  })
                }).catch((error)=>{
                    console.log(error)
                })
            }else{
                AccessToken.getCurrentAccessToken().then(
                    (data) => {
                        if (data){
                            console.log(data);
                            console.log('fetching fb data before')
                            fetchFbData(data.userID);
                        }
            
                    }
                  ).catch((error)=>{
                    console.log(error)
                })
            }
        }).catch((error)=>{
            console.log(error)
        })
  
    },[])
    
    
    
    const logOut = async ()=>{
        try {
             GoogleSignin.revokeAccess();
             GoogleSignin.signOut();
             LoginManager.logOut();
            
            
            
          } catch (error) {
            console.error(error);
          }
          setUser(null);

    }


    const LoginComponent = (props)=>{

        return <View style = {styles.container}>
                    <GoogleSigninButton
                    style={{ flex:1, height: 35 }}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Light}
                        onPress={()=> {googleSignIn()}}
                        disabled={false} />
                        <Text> Or  </Text>

                    <LoginButton
                         style={{ flex:1, height: 30 }}
                        onLoginFinished={
                        (error, result) => {
                            console.log("fb login " );
                            if (error) {
                            console.log("login has error: " + result.error);
                            } else if (result.isCancelled) {
                            console.log("login is cancelled.");
                            } else {
                            console.log('fb result', result)
                            AccessToken.getCurrentAccessToken().then(
                                (data) => {
                                    if (data){
                                        console.log(data);
                                        console.log('fetching fb data before')
                                        fetchFbData(data.userID);
                                    }
                        
                                }
                              ).catch((error)=>{
                                  console.log(error)
                              })
                            }
                        }
                        }
                        onLogoutFinished={() => console.log("logout.")}/>
         </View>

    }

    const styles = StyleSheet.create({
        container: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: "center",
            margin:20,
        },
        button:{
            borderWidth:0,
            width:200,
            height: 50,
            marginVertical:0,
            paddingTop:0,
            textAlign:"center",
            borderRadius:50

        }
        
    });
    
    return [LoginComponent, logOut, state.user, state.isLoading]
}