import React from 'react';
import { StyleSheet} from 'react-native';
import SigninScreen from './src/screens/SigninScreen'
import MovieDetailScreen from './src/screens/MovieDetailScreen'
import MovieListScreen from './src/screens/MovieListScreen'
import { createAppContainer,  createSwitchNavigator } from 'react-navigation'

import {createStackNavigator } from 'react-navigation-stack'

import {createBottomTabNavigator } from 'react-navigation-tabs'
import {Provider as MovieProvider, Context as MovieContext} from './src/context/movieContext'

import {Provider as AuthProvider, Context as AuthContext} from './src/context/authContext'


import * as favoriteActions from './src/store/favoriteActions'


import {createStore, applyMiddleware, compose, combineReducers} from 'redux';

import {Provider, connect} from 'react-redux'

import {SafeAreaView} from 'react-navigation'

import {setNavigator} from './src/navigationRef'

import { FontAwesome } from '@expo/vector-icons'; 

import favoriteReducer from './src/store/favoriteReducer'

import AsyncStorage from '@react-native-community/async-storage';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web


import { PersistGate } from 'redux-persist/integration/react'
import HomeScreen from './src/screens/HomeScreen';





const persistConfig = {
  key: 'root',
  version: 0,
  storage: AsyncStorage
}



const persistedReducer = persistReducer(persistConfig, favoriteReducer);

const store = createStore(persistedReducer);


const persistor = persistStore(store);

const icon = <FontAwesome name="th-list" size={24} color="black" />

const stackNavigator = createStackNavigator({
  MovieList: MovieListScreen,
  MovieDetails: MovieDetailScreen
})
stackNavigator.navigationOptions = {
  title: 'Movies',
  tabBarIcon: icon
}

const switchNavigator = createSwitchNavigator({

  loginFlow:  createStackNavigator({
    Signin: SigninScreen
  }),
  mainFlow: createBottomTabNavigator({
    Home: HomeScreen,
    MovieListFlow: stackNavigator

  })
});







const App = createAppContainer(switchNavigator);




const mapStateToProps = (state) =>{
  return {
      currentLocation: state.movies
  }
}





const mapDispatchToProps = (dispatch) => {
  return {
      addMovie: (movie)=>dispatch(favoriteActions.addMovie(movie))
  }
}

const AppWrapper = connect(mapStateToProps, mapDispatchToProps)( (props)=>{
  return <App ref = { (navigator) => {setNavigator(navigator)}}/>
});



export default () => {

  
 
  return (
    <AuthProvider>
      <MovieProvider>
            <Provider store = {store}>
              <PersistGate loading={null} persistor={persistor}>
                <AppWrapper/>
              </PersistGate>
            </Provider>
          </MovieProvider>
    </AuthProvider>
   

  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
