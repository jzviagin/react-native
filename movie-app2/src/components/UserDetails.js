import React, {useContext} from 'react';
import {View, StyleSheet, Text, ActivityIndicator} from 'react-native';
import {Image, Button} from 'react-native-elements'
import { FontAwesome } from '@expo/vector-icons'; 
import { color } from 'react-native-reanimated';





const UserDetails = (props) => {


    
return <>

    <View style = {styles.container}>
        < Text style = {styles.welcome}>Welcome {props.userName}</Text>
        <View style={styles.imageContainer}>
        {props.photo?<Image
            style={styles.image}
        
            source={{ uri: props.photo  }}
            PlaceholderContent={<ActivityIndicator />}
            />:
            <View style = {{display: 'flex', alignItems:'center', justifyContent:'center', width: '100%' , height:'100%'  }}> 
                <FontAwesome name="user"    size = {250} color="grey" />
            </View>
            
        }
        </View>
        
        
          
       
    </View>
      
    </>
};





const styles = StyleSheet.create({

    container:{
      
        display: "flex",
        alignItems: "center",
        justifyContent : "center"
       
    },
    welcome: {
        fontSize:20, margin:20
    },
   
   
    imageContainer: {
        marginTop: 20,
        marginBottom: 20,
        borderRadius:150,
        width: 300, height:300 ,
  
        borderWidth: 2,
        borderColor: "grey",
        overflow: 'hidden',
        
    },
    image: {
        width: '100%' , height:'100%' 
     
    }
});

export default UserDetails;