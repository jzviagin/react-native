import React, {useContext} from 'react';
import {View, StyleSheet, Text, ActivityIndicator} from 'react-native';
import {Context as MovieContext} from '../context/movieContext'
import {Image} from 'react-native-elements'
import { Directions, ScrollView } from 'react-native-gesture-handler';
import favoriteReducer from '../store/favoriteReducer';
import * as favoriteActions from '../store/favoriteActions'
import { FontAwesome } from '@expo/vector-icons'; 

import {connect} from 'react-redux'


const MovieDetailScreen = (props) => {

    const {state} = useContext(MovieContext);


    const id = props.navigation.getParam("id");
    const movie = state.movies.find( (elem) => {return elem.id === id});



     

  
    

return <>
    <ScrollView>
        <Image
            source={{ uri: 'https://image.tmdb.org/t/p/w500' + movie.poster_path }}
            style={{  width: '100%', height:300 }}
            PlaceholderContent={<ActivityIndicator />}
            />
        <FontAwesome style = {{margin:20}} onPress = { ()=> {
            if(props.favMovies.includes(movie.id)){
                props.removeMovie(movie.id)
            }else{
                 props.addMovie(movie.id)

            }
            
            } 
        }
            name={props.favMovies.includes(movie.id)?"heart":"heart-o"} size={40}  />
        <  Text style = {{fontSize:48, margin:20}}>{movie.title}</Text>

        



        <View style = {styles.rows}>
                <Text style = {styles.titleText}>Overview: </Text>
                <Text style = {styles.dataText}>{movie.overview}</Text>
                <Text style = {styles.titleText}>Rating: </Text>
                <Text style = {styles.dataText}>{movie.vote_average}</Text>
        </View>
    </ScrollView>
      
    </>
};



const styles = StyleSheet.create({

    rows: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: "flex-start",
        alignItems: "flex-start",
        flex:1,
        margin: 20
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 22
    },
    dataText: {
        fontSize: 20
    }
});

MovieDetailScreen.navigationOptions = {
    title: 'Movie Details'
  }


const mapStateToProps = (state) =>{
    return {
        favMovies:state.movies
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        //onFetchOrders: (token, userId) => dispatch(locationActions.fetchOrdersStart (token, userId)),
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})

        addMovie: (movie)=>dispatch(favoriteActions.addMovie(movie)),
        removeMovie: (movie)=>dispatch(favoriteActions.removeMovie(movie))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MovieDetailScreen);

