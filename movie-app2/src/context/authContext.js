import createDataContext from './createDataContext'



const authReducer = (state, action) => {
    switch(action.type){
        case 'set_user':
            return {...state, user: action.user, isLoading:false}
        case 'set_loading':
            return {...state, isLoading: true}
        default:
            return state;
    }
}









const setUser = dispatch => async (user)=>{


    dispatch ({
        type: 'set_user',
        user:user
     });
        
  
    
}

const setLoading = dispatch => async ()=>{


    dispatch ({
        type: 'set_loading'
     });
        
  
    
}




export const {Provider, Context} = createDataContext(authReducer, { setUser,setLoading}, {user:null, isLoading:false})