import { Transports, Reflow, Flow } from "@mcesystems/reflow";
import { ViewInterfacesType, viewInterfaces } from "usb_view_interfaces";
import devicesFlow from './Flows/devicesFlow'
import devicesFlowClass from './Flows/devicesFlowClass'


const reflow = new Reflow<ViewInterfacesType>({
	transport:new Transports.WebSocketsTransport({ port: 3002 }),
	views: viewInterfaces,
});
reflow.start(devicesFlow).then(() => {
	console.log("flow1 is finished")
})