import { Flow } from "@mcesystems/reflow";
import { ViewInterfacesType, deviceTypeClass, devicesByClass } from "usb_view_interfaces";
import devicesFlow from './devicesFlow'



var usb = require('usb')



function buildDeviceTree():devicesByClass { 



    const devices = usb.getDeviceList() 


    const deviceTree:devicesByClass= {}




    devices.forEach((device, index) => {
        if (device.deviceDescriptor.idVendor ==0 || device.deviceDescriptor.idProduct ==0){
     
            return;
        }
        if (deviceTree[device.deviceDescriptor.bDeviceClass] == undefined){
            deviceTree[device.deviceDescriptor.bDeviceClass] = []
        }
        deviceTree[device.deviceDescriptor.bDeviceClass].push({
            vendor: device.deviceDescriptor.idVendor,
            product: device.deviceDescriptor.idProduct
        })
        
      
    });

   
    

    console.log('deviceTree', deviceTree)

    return deviceTree;
}



const devicesFlowClass = <Flow<ViewInterfacesType>>(async ({ view, views, flow }) => {

    

    let devicesView;


    const reloadDevices = ()=>{

        const devices = buildDeviceTree()
        devicesView.update({devices:devices})

    }



    usb.on('attach', reloadDevices);

    usb.on('detach', reloadDevices);

   

    const devices = buildDeviceTree()

    console.log('views.DevicesViewClass',views.DevicesViewClass)

    devicesView = view(0, views.DevicesViewClass, {
        devices: devices,
    });

    await devicesView;

    devicesView.remove()
    await flow(devicesFlow)


    


    console.log('flow done')

});


export default devicesFlowClass;