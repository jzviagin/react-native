import { Flow } from "@mcesystems/reflow";
import { ViewInterfacesType, deviceType } from "usb_view_interfaces";
import devicesFlowClass from './devicesFlowClass'



var usb = require('usb')



function buildDeviceTree():deviceType[] { 



    const devices = usb.getDeviceList() 


    const deviceTree:deviceType[] = []



  
    const roots = []
    const children = {}

    const deviceMap = {}

    devices.forEach((device, index) => {
        if (device.deviceDescriptor.idVendor ==0 || device.deviceDescriptor.idProduct ==0){
     
            return;
        }
        deviceMap[device.busNumber + "_" + device.deviceAddress] = device;
        if (device.parent){

            const key = device.parent.busNumber + "_" + device.parent.deviceAddress
            if (!children[key]){
                children[key] = []
            }
            children[key].push(device.busNumber + "_" + device.deviceAddress)
  
        }else{
            roots.push(device.busNumber + "_" + device.deviceAddress)
        }
      
    });

   
    
    const processDevice = (devicesParent, device, level)=>{
       

        const newDevice = {
            vendor: device.deviceDescriptor.idVendor,
            product: device.deviceDescriptor.idProduct,
            class: device.deviceDescriptor.bDeviceClass,
            children:[]
        }
        devicesParent.push(newDevice);
        if (children[device.busNumber + "_" + device.deviceAddress]){
            children[device.busNumber + "_" + device.deviceAddress].forEach(child=>{
              
                processDevice(newDevice.children, deviceMap[child], level+ 1)
             
                
            })
            
        }

     


    }


    roots.forEach(root=>{

        processDevice(deviceTree, deviceMap[root], 0)
    })







    return deviceTree;
}



const devicesFlow = <Flow<ViewInterfacesType>>(async ({ view, views, flow }) => {

    

    let devicesView;


    const reloadDevices = ()=>{

        const devices = buildDeviceTree()
        devicesView.update({devices:devices})

    }



    usb.on('attach', reloadDevices);

    usb.on('detach', reloadDevices);

   

    const devices = buildDeviceTree()

    console.log('views.DevicesView',views.DevicesView)

    devicesView = view(0, views.DevicesView, {
        devices: devices,
    });

    await devicesView;

    devicesView.remove()
    await flow(devicesFlowClass)
   


    console.log('flow done')

});


export default devicesFlow;