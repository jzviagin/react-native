"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const reflow_1 = require("@mcesystems/reflow");
const usb_view_interfaces_1 = require("usb_view_interfaces");
const devicesFlow_1 = __importDefault(require("./Flows/devicesFlow"));
const reflow = new reflow_1.Reflow({
    transport: new reflow_1.Transports.WebSocketsTransport({ port: 3002 }),
    views: usb_view_interfaces_1.viewInterfaces,
});
reflow.start(devicesFlow_1.default).then(() => {
    console.log("flow1 is finished");
});
//# sourceMappingURL=index.js.map