"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const devicesFlow_1 = __importDefault(require("./devicesFlow"));
var usb = require('usb');
function buildDeviceTree() {
    const devices = usb.getDeviceList();
    const deviceTree = {};
    devices.forEach((device, index) => {
        if (device.deviceDescriptor.idVendor == 0 || device.deviceDescriptor.idProduct == 0) {
            return;
        }
        if (deviceTree[device.deviceDescriptor.bDeviceClass] == undefined) {
            deviceTree[device.deviceDescriptor.bDeviceClass] = [];
        }
        deviceTree[device.deviceDescriptor.bDeviceClass].push({
            vendor: device.deviceDescriptor.idVendor,
            product: device.deviceDescriptor.idProduct
        });
    });
    console.log('deviceTree', deviceTree);
    return deviceTree;
}
const devicesFlowClass = (({ view, views, flow }) => __awaiter(void 0, void 0, void 0, function* () {
    let devicesView;
    const reloadDevices = () => {
        const devices = buildDeviceTree();
        devicesView.update({ devices: devices });
    };
    usb.on('attach', reloadDevices);
    usb.on('detach', reloadDevices);
    const devices = buildDeviceTree();
    console.log('views.DevicesViewClass', views.DevicesViewClass);
    devicesView = view(0, views.DevicesViewClass, {
        devices: devices,
    });
    yield devicesView;
    devicesView.remove();
    yield flow(devicesFlow_1.default);
    console.log('flow done');
}));
exports.default = devicesFlowClass;
//# sourceMappingURL=devicesFlowClass.js.map