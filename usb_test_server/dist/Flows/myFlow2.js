"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (({ view, views }) => __awaiter(void 0, void 0, void 0, function* () {
    // Using the view() function to display the MyView component, at layer 0 of this flow
    const myView = view(0, views.MyView, {
        myInProp: "Hello Prop second flow",
        mySecondInProp: "Some text 2"
    });
    myView.on("myTriggeredEvent", ({ myEventData }) => {
        // do something with the event's data
        const myView = view(1, views.MyView, {
            myInProp: "Hello Prop second flow 2",
            mySecondInProp: "Some text 2"
        });
    });
    myView.on("event2", ({ event2param }) => {
        // do something with the event's data
    });
    const { myOutProp } = yield myView;
    // ...
}));
//# sourceMappingURL=myFlow2.js.map