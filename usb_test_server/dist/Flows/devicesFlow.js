"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const devicesFlowClass_1 = __importDefault(require("./devicesFlowClass"));
var usb = require('usb');
function buildDeviceTree() {
    const devices = usb.getDeviceList();
    const deviceTree = [];
    const roots = [];
    const children = {};
    const deviceMap = {};
    devices.forEach((device, index) => {
        if (device.deviceDescriptor.idVendor == 0 || device.deviceDescriptor.idProduct == 0) {
            return;
        }
        deviceMap[device.busNumber + "_" + device.deviceAddress] = device;
        if (device.parent) {
            const key = device.parent.busNumber + "_" + device.parent.deviceAddress;
            if (!children[key]) {
                children[key] = [];
            }
            children[key].push(device.busNumber + "_" + device.deviceAddress);
        }
        else {
            roots.push(device.busNumber + "_" + device.deviceAddress);
        }
    });
    const processDevice = (devicesParent, device, level) => {
        const newDevice = {
            vendor: device.deviceDescriptor.idVendor,
            product: device.deviceDescriptor.idProduct,
            class: device.deviceDescriptor.bDeviceClass,
            children: []
        };
        devicesParent.push(newDevice);
        if (children[device.busNumber + "_" + device.deviceAddress]) {
            children[device.busNumber + "_" + device.deviceAddress].forEach(child => {
                processDevice(newDevice.children, deviceMap[child], level + 1);
            });
        }
    };
    roots.forEach(root => {
        processDevice(deviceTree, deviceMap[root], 0);
    });
    return deviceTree;
}
const devicesFlow = (({ view, views, flow }) => __awaiter(void 0, void 0, void 0, function* () {
    let devicesView;
    const reloadDevices = () => {
        const devices = buildDeviceTree();
        devicesView.update({ devices: devices });
    };
    usb.on('attach', reloadDevices);
    usb.on('detach', reloadDevices);
    const devices = buildDeviceTree();
    console.log('views.DevicesView', views.DevicesView);
    devicesView = view(0, views.DevicesView, {
        devices: devices,
    });
    yield devicesView;
    devicesView.remove();
    yield flow(devicesFlowClass_1.default);
    console.log('flow done');
}));
exports.default = devicesFlow;
//# sourceMappingURL=devicesFlow.js.map