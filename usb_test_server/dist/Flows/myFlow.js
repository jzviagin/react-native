"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const myFlow2_1 = __importDefault(require("./myFlow2"));
exports.default = (({ view, views, flow }) => __awaiter(void 0, void 0, void 0, function* () {
    // Using the view() function to display the MyView component, at layer 0 of this flow
    const myView = view(0, views.MyView, {
        myInProp: "Hello Prop",
        mySecondInProp: "Some text"
    });
    myView.on("myTriggeredEvent", ({ myEventData }) => {
        // do something with the event's data
        const myView2 = view(1, views.MyView, {
            myInProp: myEventData + "",
            mySecondInProp: "Some text"
        });
    });
    myView.on("event2", ({ event2param }) => {
        // do something with the event's data
        flow(myFlow2_1.default);
    });
    const { myOutProp } = yield myView;
    myView.remove();
    yield flow(myFlow2_1.default);
    // ...
}));
//# sourceMappingURL=myFlow.js.map