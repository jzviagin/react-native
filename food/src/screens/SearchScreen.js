import React , {useState, useEffect} from "react";
import { Text, StyleSheet, View, ScrollView } from "react-native";
import SearchBar from '../components/SearchBar'
import Yelp from '../api/yelp'
import useResults from '../hooks/useResults'
import ResultList from '../components/ResultList'

const SearchScreen = (props) => {
  const [searchTerm, setSearchTerm] = useState("");
  /*const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");
  

  results.forEach(element => {
    console.log(element.name);
    });

  const searchApi = async  (term)=>{
      console.log("search called");
    try{
        res = await Yelp.get('/search', {
            params: {
                term: term,
                radius: 9000,
                longitude:-122.410924,
                latitude:37.753457

            }
        });
        setResults(res.data.businesses) ;
        setErrorMessage("");
    }catch (err){
        setErrorMessage("Something went wrong");
    } */
   
    

 
  //}

  /*useEffect( ()=>{
    searchApi("pasta");
    }, []) */

   const  [results, errorMessage, searchApi] = useResults();

  console.log("search: " + searchTerm)

  const resultsCostEffective = results.filter (item => {
      if (!item.price || item.price.length === 1 ){
          return true;
      }
      console.log(item.price)
      return false;
  });
  const resultsPricier = results.filter (item => {
    if (item.price && item.price.length === 2 ){
        return true;
    }
    console.log(item.price)
    return false;
});
    const resultsBigSpender = results.filter (item => {
        if (item.price && item.price.length > 2 ){
            return true;
        }
        console.log(item.price)
        return false;
    });
  return <>
      <SearchBar 
      onSubmit = {()=> {
          console.log("submit: " + searchTerm);
          searchApi(searchTerm);

      }}
      onChange = {(term) => {
          setSearchTerm( (currentTerm)=> {
                return term;
          })
      }}/>
      {errorMessage? <Text style={styles.text}>{errorMessage}</Text>: null}
   

    <ScrollView > 
        
        <ResultList style={styles.resultList} results = {resultsCostEffective} title = "Cost Effective"/>
        <ResultList style={styles.resultList} results = {resultsPricier} title = "Bit Pricier"/>
        <ResultList style={styles.resultList} results = {resultsBigSpender} title = "Big Spender!"/>
    </ScrollView>
        
 
    </>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    marginHorizontal: 15
  },
  resultList:{
      flex: 1
  }
});

export default SearchScreen;
