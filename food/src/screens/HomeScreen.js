import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";

const HomeScreen = (props) => {
  return <View>
      <Text style={styles.text}>Hi There</Text>
      <Button title = 'Go to search'
      onPress = { ()=> {
        props.navigation.navigate('Search')
        }}/>
       
    </View>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;
