import React, {useState, useEffect} from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, Image, TextInput, FlatList } from "react-native";
import Yelp from '../api/yelp'

const ResultsShowScreen = (props) => {

    const businessId = props.navigation.getParam('id');

    const [result, setResult] = useState({});
    

    const getBusiness = async  (id)=>{
       
      try{
          res = await Yelp.get('/' + id, {
              params: {
                
  
              }
          });
          setResult(res.data) ;
      }catch (err){
        console.log(err);
    } 
     
     
   
    }
    useEffect(()=>{
        getBusiness(businessId);
    }, [])

    return (
    
    <View style = {styles.container}>
        <Text>{result.name}</Text>
        <FlatList data = {result.photos} 
                    renderItem = {
                        (elem) => {
                            return <Image  style= {styles.image}  source = {{ uri:elem.item}}/>
                        }
                    }
                    keyExtractor={item => item}
                    
                    
                    showsHorizontalScrollIndicator= {false}
                    />
 
    </View>
        
        
 
  
    );

};

const styles = StyleSheet.create({
  text: {

    fontSize: 18
  },
  container: {
      flexDirection:'column',
      marginLeft: 15,
      marginBottom: 10
  },
  image: {
      width: 300,
      height: 200

  },
  name: {
    fontWeight: 'bold'

    },
    rating:{
        
    }
 

});

export default ResultsShowScreen;
