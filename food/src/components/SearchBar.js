import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, Image, TextInput } from "react-native";
//import { TextInput } from "react-native-gesture-handler";
import { Feather } from '@expo/vector-icons'

const SearchBar = (props) => {
  return <View style = {styles.background}>
      <Feather  style={styles.iconStyle} name="search" size={30} color="black" />
      <TextInput 
        autoCapitalize= "none"
        autoCorrect = {false}
        placeholder= "Search" 
        style={styles.text}  
        onChangeText={text => props.onChange(text) }
        onEndEditing= {()=> {props.onSubmit()}}
        >

        </TextInput>
     
        
 
    </View>;
};

const styles = StyleSheet.create({
  text: {

    flex: 1,
    fontSize: 18
  },
  background: {
    backgroundColor: '#F0EEEE',
    flexDirection: 'row',
    height: 50,
    borderRadius:5,
    marginHorizontal: 15,
    marginTop: 10,
    marginBottom: 10

  },
  iconStyle : {
      fontSize: 35,
      alignSelf: 'center',
      marginHorizontal: 15
  }

});

export default SearchBar;
