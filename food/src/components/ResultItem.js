import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, Image, TextInput } from "react-native";

const ResultItem = (props) => {

    const business = props.business;
    return (
    <View style = {styles.container}>
        <Image style= {styles.image} source = {{ uri:business.image_url}}></Image>
        <Text style= {styles.name}>{business.name}</Text>
        <Text style= {styles.rating}>{business.rating} stars, {business.review_count} Reviews</Text>
        
        
 
    </View>
    );

};

const styles = StyleSheet.create({
  text: {

    fontSize: 18
  },
  container: {
      flexDirection:'column',
      marginLeft: 15,
      marginBottom: 10
  },
  image: {
      width: 250,
      height: 120,
      borderRadius: 4,
      marginBottom: 5
  },
  name: {
    fontWeight: 'bold'

    },
    rating:{
        
    }
 

});

export default ResultItem;
