import React from "react";
import {  StyleSheet, View, FlatList, Text,TouchableOpacity } from "react-native";
import ResultItem from './ResultItem'
import { withNavigation } from 'react-navigation';
//import { TextInput } from "react-native-gesture-handler";

const ResultList = (props) => {

    return (
    <View style = {styles.background}>
        <Text style = {styles.title}>{props.title}</Text>
        <FlatList data = {props.results} 
                    renderItem = {
                        (elem) => {
                            return (
                                <TouchableOpacity onPress = {
                                    ()=>{
                                        props.navigation.navigate('ResultsShow', {id: elem.item.id})
                                    }
                                }>
                                    <ResultItem business = {elem.item}/>
                                </TouchableOpacity>
                              

                            )
                        }
                    }
                    keyExtractor={item => item.id}
                    horizontal = {true}
                    
                    showsHorizontalScrollIndicator= {false}
                    />
 
    </View>
    );

};

const styles = StyleSheet.create({
  text: {

    fontSize: 18
  },
  background: {
    
  },
  image: {
      height: 500,
      width: 500
  },
  title: {
      fontSize : 18,
      fontWeight: 'bold',
      marginHorizontal: 15
  }
 

});

export default withNavigation(ResultList);
