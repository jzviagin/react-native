import React , {useState, useEffect} from "react";
import Yelp from '../api/yelp'

export default () =>{
    const [results, setResults] = useState([]);
    const [errorMessage, setErrorMessage] = useState("");
    const searchApi = async  (term)=>{
        console.log("search called");
      try{
          res = await Yelp.get('/search', {
              params: {
                  term: term,
                  radius: 9000,
                  longitude:-122.410924,
                  latitude:37.753457
  
              }
          });
          setResults(res.data.businesses) ;
          setErrorMessage("");
      }catch (err){
          setErrorMessage("Something went wrong");
      } 
     
      //console.log("got res:" + res.data);
      /*res.data.businesses.forEach(element => {
          console.log(element.name);
      });*/
  
   
    }
  
    useEffect( ()=>{
      searchApi("pasta");
      }, []) ;

    return [results, errorMessage, searchApi];
    
}