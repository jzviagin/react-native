import React, {useState} from "react";
import { Text, StyleSheet, View, Image, Button , FlatList} from "react-native";

const SquareScreen = (props) => {
  
  const [rgb, setRgb] = useState({
      red: 255,
      green: 255,
      blue: 255
  })
  console.log(rgb)
  return (
        <View>
            <Button title = 'Inc Red'
                onPress = { ()=> {
                    if (rgb.red === 255){
                        return;
                    }
                    setRgb({
                        ...rgb,
                        red: rgb.red + 1
                    })
                }}/>

            <Button title = 'Dec Red'
                onPress = { ()=> {
                    if (rgb.red === 0){
                        return;
                    }
                    setRgb({
                        ...rgb,
                        red: rgb.red - 1
                    })
                }}/>

            <Button title = 'Inc Green'
                onPress = { ()=> {
                    if (rgb.green === 255){
                        return;
                    }
                    setRgb({
                        ...rgb,
                        green: rgb.green + 1
                    })
                }}/>

            <Button title = 'Dec Green'
                onPress = { ()=> {
                    if (rgb.green === 0){
                        return;
                    }
                    setRgb({
                        ...rgb,
                        green: rgb.green - 1
                    })
                }}/>
            <Button title = 'Inc Blue'
                onPress = { ()=> {
                    if (rgb.blue === 255){
                        return;
                    }
                    setRgb({
                        ...rgb,
                        blue: rgb.blue + 1
                    })
                }}/>

            <Button title = 'Dec Blue'
                onPress = { ()=> {
                    if (rgb.blue === 0){
                        return;
                    }
                    setRgb({
                        ...rgb,
                        blue: rgb.blue - 1
                    })
                }}/>
                
            <View  style = {{height: 100, width: 100, backgroundColor: `rgb(${rgb.red}, ${rgb.green}, ${rgb.blue})`}}/>
        </View>
    );
};


const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default SquareScreen;
