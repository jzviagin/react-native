import React, {useState, useReducer} from "react";
import { Text, StyleSheet, View, Image, Button } from "react-native";



const ACTION_INC = "ACTION_ICN";
const ACTION_DEC = "ACTION_DEC";

const reducer = (state, action)=>{
  switch (action.type){
    case ACTION_INC:
      return {
        ...state,
        counter: state.counter + 1
      }
    case ACTION_DEC:
      return {
        ...state,
        counter: state.counter - 1
      }
    default:
      return state;

  }
}

const CounterScreen = (props) => {
  const [state, dispatch] = useReducer(reducer, {
      counter: 0
  });
  
  console.log(props.navigation)
  return (
    <View>
      <Button title = 'Increase'
       onPress = { ()=> {
        dispatch({
          type: ACTION_INC
        });
      }}/>
      <Button title = 'Decrease'
        onPress = { () => {
          dispatch({
            type: ACTION_DEC
          });
      }}/>
      <Text style={styles.text}>Current count: {state.counter}</Text>
    </View>
    );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default CounterScreen;
