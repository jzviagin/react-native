import React from 'react';
import {Text, StyleSheet, View, FlatList} from 'react-native';


const ListScreen = (props) => {

    const friends = [
        {name: 'friend1', age: 20},
        {name: 'friend2', age: 20},
        {name: 'friend3', age: 20},
        {name: 'friend4', age: 20},
        {name: 'friend5', age: 20},
        {name: 'friend6', age: 20},
        {name: 'friend7', age: 20},
        {name: 'friend8', age: 20},
        {name: 'friend9', age: 20},
        {name: 'friend10', age: 20},
        {name: 'friend11', age: 20},
        {name: 'friend12', age: 20},
        {name: 'friend13', age: 20},
        {name: 'friend14', age: 20},
        {name: 'friend15', age: 20},
        {name: 'friend16', age: 20},
        {name: 'friend17', age: 20},
        {name: 'friend18', age: 20},
        {name: 'friend19', age: 20},
        {name: 'friend20', age: 20}
    ];

    const items = friends.map ( (elem) => {
        return <Text>{elem.name}</Text>   ;
    })
    return (
        <View>
            <Text style = {styles.textStyle}>List Screen</Text>
            <FlatList data = {friends} 
                renderItem = {
                    (elem) => {
                    return <Text  style = {styles.textStyle}>{elem.item.name} - Age: {elem.item.age}</Text>;
                    }
                }
                keyExtractor={item => item.name}
                
                showsHorizontalScrollIndicator= {false}
                />
        </View>
    
    )
    ;
}


const styles = StyleSheet.create({
    textStyle: {
        fontSize: 45,
        marginVertical: 50
    }
})

export default ListScreen;