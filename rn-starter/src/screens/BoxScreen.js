import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";

const BoxScreen = (props) => {
  console.log(props.navigation)
  return <View style = {styles.viewStyle}> 
        <Text style = {styles.text1Style}>Box screen</Text>
        <Text style = {styles.text2Style}>Box screen</Text>     
        <Text style = {styles.text3Style}>Box screen</Text>            
    </View>;
};

const styles = StyleSheet.create({
  text1Style: {
    fontSize: 10,
    borderWidth: 1,
    borderColor: 'red',
    flex:1,
    height: 200

  },
  text2Style: {
    fontSize: 10,
    borderWidth: 1,
    borderColor: 'red',
    alignSelf: 'flex-end',
    marginHorizontal: 10,
    flex:1,
    height: 200

  },
  text3Style: {
    fontSize: 10,
    borderWidth: 1,
    borderColor: 'red',
    flex:1,
    height: 200
    

  },
  viewStyle: {
    borderWidth: 1,
    borderColor: 'black',
    margin: 20,
    alignItems: 'flex-start',
    height: 400,
    flexDirection: 'row',

    justifyContent: 'center'
    
  }
});

export default BoxScreen;
