import React, {useState, useReducer} from "react";
import { Text, StyleSheet, View, Image, Button , FlatList} from "react-native";
import ColorCounter from "../components/ColorCounter"
import Reducer from "../store/reducers/colors"

const SquareScreen = (props) => {

  const [state, dispatch] = useReducer(Reducer, {

    red:255,
    green:255,
    blue:255
});
  
  /*const [rgb, setRgb] = useState({
      red: 255,
      green: 255,
      blue: 255
  })*/
  console.log("state"  + state)
  return (
        <View>
            
             <ColorCounter color="Red" dispatch = {dispatch}/>
             <ColorCounter color="Green" dispatch = {dispatch}/>
             <ColorCounter color="Blue"dispatch = {dispatch}/>
            <View  style = {{height: 100, width: 100, backgroundColor: `rgb(${state.red}, ${state.green}, ${state.blue})`}}/>
        </View>
    );
};




const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default SquareScreen;
