import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";
import ComponentsScreen from './ComponentsScreen'

const HomeScreen = (props) => {
  console.log(props.navigation)
  return <View>
      <Text style={styles.text}>Hi There</Text>
      <Button title = 'Go to components demo'
      onPress = { ()=> {
        console.log('Button pressed');
        props.navigation.navigate('Components')
        }}/>
        <Button title = 'Go to List demo'
      onPress = { ()=> {
        console.log('Button pressed');
        props.navigation.navigate('List')
        }}/>
          <Button title = 'Go to Image demo'
      onPress = { ()=> {
        console.log('Button pressed');
        props.navigation.navigate('Image')
        }}/>
        <Button title = 'Go to Counter demo'
      onPress = { ()=> {
        console.log('Button pressed');
        props.navigation.navigate('Counter')
        }}/>

        <Button title = 'Go to Color demo'
      onPress = { ()=> {
        console.log('Button pressed');
        props.navigation.navigate('Color')
        }}/>

        <Button title = 'Go to Square demo'
      onPress = { ()=> {
        console.log('Button pressed');
        props.navigation.navigate('Square')
        }}/>

        
      <Button title = 'Go to Box demo'
      onPress = { ()=> {
        console.log('Button pressed');
        props.navigation.navigate('Box')
        }}/>
        
      
    </View>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;
