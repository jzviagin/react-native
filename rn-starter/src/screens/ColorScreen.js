import React, {useState} from "react";
import { Text, StyleSheet, View, Image, Button , FlatList} from "react-native";

const ColorScreen = (props) => {
  
  const [colors, setColors] = useState([])
  console.log(colors)
  return (
    <View>
      <Button title = 'Add a color'
       onPress = { ()=> {
           const rgb = randomRgb();
           const newColors = [...colors, <View key = {'color'+colors.length} style = {{height: 100, width: 100, backgroundColor: rgb}}/>]
           setColors(newColors);
      }}/>
      <FlatList data = {colors} 
                renderItem = {
                    (elem) => {
                    return elem.item;
                    }
                }
                keyExtractor={item => item.key}
                
                showsHorizontalScrollIndicator= {false}
                />
    </View>
    );
};

const randomRgb = () => {
    const red = Math.floor(Math.random()*256);
    const green = Math.floor(Math.random()*256);
    const blue = Math.floor(Math.random()*256);
    return `rgb(${red}, ${green}, ${blue})`;
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default ColorScreen;
