import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity } from "react-native";
import ImageDetail from '../components/ImageDetail'

const ImageScreen = (props) => {
  console.log(props.navigation)
  return <View>
      <ImageDetail title = 'forest' source = {require ('../../assets/forest.jpg')} score = {1}/> 
      <ImageDetail title = 'Beach' source = {require ('../../assets/beach.jpg')} score = {2}/> 
      <ImageDetail title = 'Mountain' source = {require ('../../assets/mountain.jpg')} score = {3}/>       
    </View>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default ImageScreen;
