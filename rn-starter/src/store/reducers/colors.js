
import * as actionTypes from '../actions/actionTypes'



const initialState = {

    red:255,
    green:255,
    blue:255
}


const incColor = (state, action) => {
    let currentValue = 255;
    if (typeof state[action.color]  !== 'undefined'){
        currentValue = state[action.color]
    }
    if (currentValue == 255) {
    }else{
        currentValue = currentValue + 1;
    }
    return {
        ...state,
        [action.color]: currentValue
    }
}

const decColor = (state, action) => {
    let currentValue = 255;
    console.log("state color " + state[action.color])
    if (typeof state[action.color]  !== 'undefined'){
        currentValue = state[action.color]
    }
    if (currentValue == 0) {
    }else{
        currentValue = currentValue - 1;
    }
    return {
        ...state,
        [action.color]: currentValue
    }
}


const reducer = (state = initialState,action) => {




    console.log("action " + action.type + " " + action.color);
    switch (action.type){
        
        case actionTypes.INC_COLOR:
           return incColor(state, action);
        
        
        case actionTypes.DEC_COLOR:
           return decColor(state, action);

    
    

        default:
            return state;
    }
    
    
};

export default reducer;