import * as actionTypes from '../actions/actionTypes'

export const incColor = (color) => {
    return{
        type: actionTypes.INC_COLOR,
        color: color
    }
}

export const decColor = (color) => {
    return{
        type: actionTypes.DEC_COLOR,
        color: color
    }
}




