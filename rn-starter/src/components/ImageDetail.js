import React from "react";
import { Text, StyleSheet, View, Image } from "react-native";

const ImageDetail = (props) => {
  console.log(props.navigation)
  return <View>
    <Image source = {props.source} />
      <Text style={styles.text}>{props.title}</Text>
      <Text style={styles.text}>Image score - {props.score}</Text>
    
      
    </View>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default ImageDetail;
