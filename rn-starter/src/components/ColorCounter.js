import React, {useState} from "react";
import { Text, StyleSheet, View, Image, Button , FlatList} from "react-native";
import * as Actions from "../store/actions/"

const ColorCounter = (props) => {
  
  const [value, setValue] = useState(255)
  console.log(props.color + " : " + value)
  return (
        <View>
            <Text style = {styles.text}>{props.color}</Text>
            <Button title = {`Inc ${props.color}`}
                onPress = { ()=> {
                    /*if (value === 255){
                        return;
                    }
                    setValue(value + 1);*/
                    props.dispatch(Actions.incColor(props.color.toLowerCase()));
                }}/>

            <Button title = {`Dec ${props.color}`}
                onPress = { ()=> {
                   /* if (value === 0){
                        return;
                    }
                    setValue(value - 1);*/
                    props.dispatch(Actions.decColor(props.color.toLowerCase()));
                }}/>

          
        </View>
    );
};


const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default ColorCounter;
